﻿using System.Threading.Tasks;
using RASP.AbpTemplate.Security.Recaptcha;

namespace RASP.AbpTemplate.Test.Base.Web
{
    public class FakeRecaptchaValidator : IRecaptchaValidator
    {
        public Task ValidateAsync(string captchaResponse)
        {
            return Task.CompletedTask;
        }
    }
}
