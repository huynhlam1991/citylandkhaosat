﻿using Abp.Modules;
using RASP.AbpTemplate.Test.Base;

namespace RASP.AbpTemplate.Tests
{
    [DependsOn(typeof(AbpTemplateTestBaseModule))]
    public class AbpTemplateTestModule : AbpModule
    {
       
    }
}
