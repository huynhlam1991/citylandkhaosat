﻿using RASP.AbpTemplate.Auditing;
using RASP.AbpTemplate.Test.Base;
using Shouldly;
using Xunit;

namespace RASP.AbpTemplate.Tests.Auditing
{
    // ReSharper disable once InconsistentNaming
    public class NamespaceStripper_Tests: AppTestBase
    {
        private readonly INamespaceStripper _namespaceStripper;

        public NamespaceStripper_Tests()
        {
            _namespaceStripper = Resolve<INamespaceStripper>();
        }

        [Fact]
        public void Should_Stripe_Namespace()
        {
            var controllerName = _namespaceStripper.StripNameSpace("RASP.AbpTemplate.Web.Controllers.HomeController");
            controllerName.ShouldBe("HomeController");
        }

        [Theory]
        [InlineData("RASP.AbpTemplate.Auditing.GenericEntityService`1[[RASP.AbpTemplate.Storage.BinaryObject, RASP.AbpTemplate.Core, Version=1.10.1.0, Culture=neutral, PublicKeyToken=null]]", "GenericEntityService<BinaryObject>")]
        [InlineData("CompanyName.ProductName.Services.Base.EntityService`6[[CompanyName.ProductName.Entity.Book, CompanyName.ProductName.Core, Version=1.10.1.0, Culture=neutral, PublicKeyToken=null],[CompanyName.ProductName.Services.Dto.Book.CreateInput, N...", "EntityService<Book, CreateInput>")]
        [InlineData("RASP.AbpTemplate.Auditing.XEntityService`1[RASP.AbpTemplate.Auditing.AService`5[[RASP.AbpTemplate.Storage.BinaryObject, RASP.AbpTemplate.Core, Version=1.10.1.0, Culture=neutral, PublicKeyToken=null],[RASP.AbpTemplate.Storage.TestObject, RASP.AbpTemplate.Core, Version=1.10.1.0, Culture=neutral, PublicKeyToken=null],]]", "XEntityService<AService<BinaryObject, TestObject>>")]
        public void Should_Stripe_Generic_Namespace(string serviceName, string result)
        {
            var genericServiceName = _namespaceStripper.StripNameSpace(serviceName);
            genericServiceName.ShouldBe(result);
        }
    }
}
