﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Castle.Windsor.MsDependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using RASP.AbpTemplate.Configure;
using RASP.AbpTemplate.Startup;
using RASP.AbpTemplate.Test.Base;

namespace RASP.AbpTemplate.GraphQL.Tests
{
    [DependsOn(
        typeof(AbpTemplateGraphQLModule),
        typeof(AbpTemplateTestBaseModule))]
    public class AbpTemplateGraphQLTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            IServiceCollection services = new ServiceCollection();
            
            services.AddAndConfigureGraphQL();

            WindsorRegistrationHelper.CreateServiceProvider(IocManager.IocContainer, services);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(AbpTemplateGraphQLTestModule).GetAssembly());
        }
    }
}