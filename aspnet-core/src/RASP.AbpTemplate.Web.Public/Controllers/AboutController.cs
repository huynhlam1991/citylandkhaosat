﻿using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.Web.Controllers;

namespace RASP.AbpTemplate.Web.Public.Controllers
{
    public class AboutController : AbpTemplateControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}