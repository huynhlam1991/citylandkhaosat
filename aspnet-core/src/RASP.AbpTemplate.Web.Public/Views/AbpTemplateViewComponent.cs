﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace RASP.AbpTemplate.Web.Public.Views
{
    public abstract class AbpTemplateViewComponent : AbpViewComponent
    {
        protected AbpTemplateViewComponent()
        {
            LocalizationSourceName = AbpTemplateConsts.LocalizationSourceName;
        }
    }
}