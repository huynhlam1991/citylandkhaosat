﻿using Abp.Dependency;
using RASP.AbpTemplate.Configuration;
using RASP.AbpTemplate.Url;
using RASP.AbpTemplate.Web.Url;

namespace RASP.AbpTemplate.Web.Public.Url
{
    public class WebUrlService : WebUrlServiceBase, IWebUrlService, ITransientDependency
    {
        public WebUrlService(
            IAppConfigurationAccessor appConfigurationAccessor) :
            base(appConfigurationAccessor)
        {
        }

        public override string WebSiteRootAddressFormatKey => "App:WebSiteRootAddress";

        public override string ServerRootAddressFormatKey => "App:AdminWebSiteRootAddress";
    }
}