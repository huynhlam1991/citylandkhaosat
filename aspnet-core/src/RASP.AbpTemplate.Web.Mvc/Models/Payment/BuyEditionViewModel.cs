﻿using System.Collections.Generic;
using RASP.AbpTemplate.Editions;
using RASP.AbpTemplate.Editions.Dto;
using RASP.AbpTemplate.MultiTenancy.Payments;
using RASP.AbpTemplate.MultiTenancy.Payments.Dto;

namespace RASP.AbpTemplate.Web.Models.Payment
{
    public class BuyEditionViewModel
    {
        public SubscriptionStartType? SubscriptionStartType { get; set; }

        public EditionSelectDto Edition { get; set; }

        public decimal? AdditionalPrice { get; set; }

        public EditionPaymentType EditionPaymentType { get; set; }

        public List<PaymentGatewayModel> PaymentGateways { get; set; }
    }
}
