﻿using RASP.AbpTemplate.MultiTenancy.Payments;

namespace RASP.AbpTemplate.Web.Models.Payment
{
    public class CancelPaymentModel
    {
        public string PaymentId { get; set; }

        public SubscriptionPaymentGatewayType Gateway { get; set; }
    }
}