﻿using System.Collections.Generic;
using RASP.AbpTemplate.Editions.Dto;
using RASP.AbpTemplate.MultiTenancy.Payments;

namespace RASP.AbpTemplate.Web.Models.Payment
{
    public class UpgradeEditionViewModel
    {
        public EditionSelectDto Edition { get; set; }

        public PaymentPeriodType PaymentPeriodType { get; set; }

        public SubscriptionPaymentType SubscriptionPaymentType { get; set; }

        public decimal? AdditionalPrice { get; set; }

        public List<PaymentGatewayModel> PaymentGateways { get; set; }
    }
}