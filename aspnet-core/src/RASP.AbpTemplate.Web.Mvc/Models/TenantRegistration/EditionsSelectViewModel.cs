﻿using Abp.AutoMapper;
using RASP.AbpTemplate.MultiTenancy.Dto;

namespace RASP.AbpTemplate.Web.Models.TenantRegistration
{
    [AutoMapFrom(typeof(EditionsSelectOutput))]
    public class EditionsSelectViewModel : EditionsSelectOutput
    {
    }
}
