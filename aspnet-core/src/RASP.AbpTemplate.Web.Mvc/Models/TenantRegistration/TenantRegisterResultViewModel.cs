﻿using Abp.AutoMapper;
using RASP.AbpTemplate.MultiTenancy.Dto;

namespace RASP.AbpTemplate.Web.Models.TenantRegistration
{
    [AutoMapFrom(typeof(RegisterTenantOutput))]
    public class TenantRegisterResultViewModel : RegisterTenantOutput
    {
        public string TenantLoginAddress { get; set; }
    }
}