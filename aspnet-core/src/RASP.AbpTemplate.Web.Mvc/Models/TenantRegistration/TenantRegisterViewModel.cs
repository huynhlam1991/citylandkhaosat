﻿using RASP.AbpTemplate.Editions;
using RASP.AbpTemplate.Editions.Dto;
using RASP.AbpTemplate.MultiTenancy.Payments;
using RASP.AbpTemplate.Security;
using RASP.AbpTemplate.MultiTenancy.Payments.Dto;

namespace RASP.AbpTemplate.Web.Models.TenantRegistration
{
    public class TenantRegisterViewModel
    {
        public PasswordComplexitySetting PasswordComplexitySetting { get; set; }

        public int? EditionId { get; set; }

        public SubscriptionStartType? SubscriptionStartType { get; set; }

        public EditionSelectDto Edition { get; set; }

        public EditionPaymentType EditionPaymentType { get; set; }
    }
}
