﻿using System.Collections.Generic;
using Abp.Localization;
using RASP.AbpTemplate.Install.Dto;

namespace RASP.AbpTemplate.Web.Models.Install
{
    public class InstallViewModel
    {
        public List<ApplicationLanguage> Languages { get; set; }

        public AppSettingsJsonDto AppSettingsJson { get; set; }
    }
}
