﻿using Abp.AutoMapper;
using RASP.AbpTemplate.Sessions.Dto;

namespace RASP.AbpTemplate.Web.Views.Shared.Components.TenantChange
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}