﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace RASP.AbpTemplate.Web.Views
{
    public abstract class AbpTemplateViewComponent : AbpViewComponent
    {
        protected AbpTemplateViewComponent()
        {
            LocalizationSourceName = AbpTemplateConsts.LocalizationSourceName;
        }
    }
}