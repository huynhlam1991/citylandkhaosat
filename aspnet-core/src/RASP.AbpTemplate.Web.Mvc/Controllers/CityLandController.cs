﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.RMarketing;
using RASP.AbpTemplate.RSurvey;

namespace RASP.AbpTemplate.Web.Controllers
{
    public class CityLandController : AbpTemplateControllerBase
    {
        private readonly IRepository<RMailCustomer, Guid> _rMailCustomerRepository;
        private readonly IRepository<SurveyResult, long> _surveyResultRepository;
        private readonly IRepository<Survey, long> _surveyRepository;
        public CityLandController(IRepository<RMailCustomer, Guid> rMailCustomerRepository,
            IRepository<SurveyResult, long> surveyResultRepository,
            IRepository<Survey, long> surveyRepository)
        {
            _rMailCustomerRepository = rMailCustomerRepository;
            _surveyResultRepository = surveyResultRepository;
            _surveyRepository = surveyRepository;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Survey(string id, string name, string cpo)
        {
            using(var unitOfWork = UnitOfWorkManager.Begin())
            {
                if (!string.IsNullOrEmpty(cpo))
                {
                    cpo = cpo.ToUpper();
                }
                var person = _rMailCustomerRepository.FirstOrDefault(x => x.Id.ToString() == cpo);
                if(person == null)
                {
                    unitOfWork.Complete();
                    return View("NotFound");
                }
                else
                {
                    var surveyId = Convert.ToInt64(id);

                    var oldSurveyResult = _surveyResultRepository.FirstOrDefault(x => x.SurveyId == surveyId && x.Text == person.Email);

                    var survey = _surveyRepository.FirstOrDefault(x => x.Id == surveyId);
                    if (oldSurveyResult != null && survey!=null && survey.StartDate <= DateTime.Now && survey.EndDate >= DateTime.Now)
                    {
                        unitOfWork.Complete();
                        return View("NotFound");
                    }
                }
                unitOfWork.Complete();
                return View();
            }
        }
        public ActionResult NotFound()
        {
            return View();
        }
    }
}