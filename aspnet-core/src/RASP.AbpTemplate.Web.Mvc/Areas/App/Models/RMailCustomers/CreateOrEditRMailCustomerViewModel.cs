using RASP.AbpTemplate.RMarketing.Dtos;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Models.RMailCustomers
{
    public class CreateOrEditRMailCustomerModalViewModel
    {
       public CreateOrEditRMailCustomerDto RMailCustomer { get; set; }

	   
	   public bool IsEditMode => RMailCustomer.Id.HasValue;
    }
}