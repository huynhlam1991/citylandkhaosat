namespace RASP.AbpTemplate.Web.Areas.App.Models.Products
{
    public class ProductRProductCategoryLookupTableViewModel
    {
        public long? Id { get; set; }

        public string DisplayName { get; set; }

        public string FilterText { get; set; }
    }
}