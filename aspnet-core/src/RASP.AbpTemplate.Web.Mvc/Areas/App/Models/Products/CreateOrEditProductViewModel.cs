using RASP.AbpTemplate.RModel.Dtos;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Products
{
    public class CreateOrEditProductModalViewModel
    {
       public CreateOrEditProductDto Product { get; set; }

	   		public string RProductCategoryTitle { get; set;}


	   public bool IsEditMode => Product.Id.HasValue;
    }
}