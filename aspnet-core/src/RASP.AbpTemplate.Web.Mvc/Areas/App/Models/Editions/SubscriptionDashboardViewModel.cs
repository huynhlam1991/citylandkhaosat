﻿using RASP.AbpTemplate.Sessions.Dto;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Editions
{
    public class SubscriptionDashboardViewModel
    {
        public GetCurrentLoginInformationsOutput LoginInformations { get; set; }
    }
}
