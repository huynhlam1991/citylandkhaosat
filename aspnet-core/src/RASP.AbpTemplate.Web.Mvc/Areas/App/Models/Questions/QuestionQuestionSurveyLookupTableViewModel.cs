namespace RASP.AbpTemplate.Web.Areas.App.Models.Questions
{
    public class QuestionSurveyLookupTableViewModel
    {
        public long? Id { get; set; }

        public string DisplayName { get; set; }

        public string FilterText { get; set; }
    }
}