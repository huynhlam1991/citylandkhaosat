using RASP.AbpTemplate.RSurvey.Dtos;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Questions
{
    public class CreateOrEditQuestionModalViewModel
    {
       public CreateOrEditQuestionDto Question { get; set; }

	   		public string SurveyTitle { get; set;}


	   public bool IsEditMode => Question.Id.HasValue;
    }
}