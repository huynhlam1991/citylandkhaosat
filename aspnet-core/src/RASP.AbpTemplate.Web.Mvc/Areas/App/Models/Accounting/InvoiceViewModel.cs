﻿using RASP.AbpTemplate.MultiTenancy.Accounting.Dto;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Accounting
{
    public class InvoiceViewModel
    {
        public InvoiceDto Invoice { get; set; }
    }
}
