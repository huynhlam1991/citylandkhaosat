using RASP.AbpTemplate.RSurvey.Dtos;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Surveies
{
    public class CreateOrEditSurveyModalViewModel
    {
       public CreateOrEditSurveyDto Survey { get; set; }

	   
	   public bool IsEditMode => Survey.Id.HasValue;
    }
}