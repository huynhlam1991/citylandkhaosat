using RASP.AbpTemplate.RModel.Dtos;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Models.RCategories
{
    public class CreateOrEditRCategoryModalViewModel
    {
       public CreateOrEditRCategoryDto RCategory { get; set; }

	   
	   public bool IsEditMode => RCategory.Id.HasValue;
    }
}