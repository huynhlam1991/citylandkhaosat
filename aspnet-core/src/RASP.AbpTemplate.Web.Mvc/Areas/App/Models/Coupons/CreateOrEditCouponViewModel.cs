using RASP.AbpTemplate.RModel.Dtos;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Coupons
{
    public class CreateOrEditCouponModalViewModel
    {
       public CreateOrEditCouponDto Coupon { get; set; }

	   
	   public bool IsEditMode => Coupon.Id.HasValue;
    }
}