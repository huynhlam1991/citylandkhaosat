﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Configuration.Host.Dto;
using RASP.AbpTemplate.Editions.Dto;

namespace RASP.AbpTemplate.Web.Areas.App.Models.HostSettings
{
    public class HostSettingsViewModel
    {
        public HostSettingsEditDto Settings { get; set; }

        public List<SubscribableEditionComboboxItemDto> EditionItems { get; set; }

        public List<ComboboxItemDto> TimezoneItems { get; set; }
    }
}