using RASP.AbpTemplate.RSurvey.Dtos;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Models.PersonAnswers
{
    public class CreateOrEditPersonAnswerModalViewModel
    {
       public CreateOrEditPersonAnswerDto PersonAnswer { get; set; }

	   		public string SurveyTitle { get; set;}

		public string QuestionQuestionText { get; set;}

		public string AnswerAnswerText { get; set;}


	   public bool IsEditMode => PersonAnswer.Id.HasValue;
    }
}