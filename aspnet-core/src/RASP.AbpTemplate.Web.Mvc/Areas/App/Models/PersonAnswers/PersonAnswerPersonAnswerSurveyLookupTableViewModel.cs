namespace RASP.AbpTemplate.Web.Areas.App.Models.PersonAnswers
{
    public class PersonAnswerSurveyLookupTableViewModel
    {
        public long? Id { get; set; }

        public string DisplayName { get; set; }

        public string FilterText { get; set; }
    }
}