﻿using System.Collections.Generic;
using RASP.AbpTemplate.Editions.Dto;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Tenants
{
    public class TenantIndexViewModel
    {
        public List<SubscribableEditionComboboxItemDto> EditionItems { get; set; }
    }
}