﻿using Abp.AutoMapper;
using RASP.AbpTemplate.MultiTenancy;
using RASP.AbpTemplate.MultiTenancy.Dto;
using RASP.AbpTemplate.Web.Areas.App.Models.Common;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Tenants
{
    [AutoMapFrom(typeof (GetTenantFeaturesEditOutput))]
    public class TenantFeaturesEditViewModel : GetTenantFeaturesEditOutput, IFeatureEditViewModel
    {
        public Tenant Tenant { get; set; }
    }
}