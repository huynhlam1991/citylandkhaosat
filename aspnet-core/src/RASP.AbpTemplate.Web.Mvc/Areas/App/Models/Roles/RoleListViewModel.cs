﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Authorization.Permissions.Dto;
using RASP.AbpTemplate.Web.Areas.App.Models.Common;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Roles
{
    public class RoleListViewModel : IPermissionsEditViewModel
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}