﻿using Abp.AutoMapper;
using RASP.AbpTemplate.Authorization.Roles.Dto;
using RASP.AbpTemplate.Web.Areas.App.Models.Common;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Roles
{
    [AutoMapFrom(typeof(GetRoleForEditOutput))]
    public class CreateOrEditRoleModalViewModel : GetRoleForEditOutput, IPermissionsEditViewModel
    {
        public bool IsEditMode => Role.Id.HasValue;
    }
}