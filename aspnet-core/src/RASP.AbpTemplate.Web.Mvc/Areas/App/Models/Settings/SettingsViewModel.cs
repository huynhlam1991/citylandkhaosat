﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Configuration.Tenants.Dto;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Settings
{
    public class SettingsViewModel
    {
        public TenantSettingsEditDto Settings { get; set; }
        
        public List<ComboboxItemDto> TimezoneItems { get; set; }
    }
}