﻿using Abp.AutoMapper;
using RASP.AbpTemplate.Authorization.Users;
using RASP.AbpTemplate.Authorization.Users.Dto;
using RASP.AbpTemplate.Web.Areas.App.Models.Common;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Users
{
    [AutoMapFrom(typeof(GetUserPermissionsForEditOutput))]
    public class UserPermissionsEditViewModel : GetUserPermissionsForEditOutput, IPermissionsEditViewModel
    {
        public User User { get; set; }
    }
}