﻿using System.Collections.Generic;
using RASP.AbpTemplate.Authorization.Users.Dto;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Users
{
    public class UserLoginAttemptModalViewModel
    {
        public List<UserLoginAttemptDto> LoginAttempts { get; set; }
    }
}