namespace RASP.AbpTemplate.Web.Areas.App.Models.SurveyResults
{
    public class SurveyResultUserLookupTableViewModel
    {
        public long? Id { get; set; }

        public string DisplayName { get; set; }

        public string FilterText { get; set; }
    }
}