using RASP.AbpTemplate.RSurvey.Dtos;
using Abp.Extensions;
using System.Collections.Generic;

namespace RASP.AbpTemplate.Web.Areas.App.Models.SurveyResults
{
    public class CreateOrEditSurveyResultModalViewModel
    {
       public CreateOrEditSurveyResultDto SurveyResult { get; set; }

	   		public string SurveyTitle { get; set;}

		public string UserName { get; set;}


	   public bool IsEditMode => SurveyResult.Id.HasValue;
    }
}