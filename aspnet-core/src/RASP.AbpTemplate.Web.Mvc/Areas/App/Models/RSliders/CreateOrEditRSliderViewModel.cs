using RASP.AbpTemplate.RModel.Dtos;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Models.RSliders
{
    public class CreateOrEditRSliderModalViewModel
    {
       public CreateOrEditRSliderDto RSlider { get; set; }

	   
	   public bool IsEditMode => RSlider.Id.HasValue;
    }
}