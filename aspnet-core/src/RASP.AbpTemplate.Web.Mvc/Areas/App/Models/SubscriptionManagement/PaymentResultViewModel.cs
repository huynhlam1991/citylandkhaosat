﻿using Abp.AutoMapper;
using RASP.AbpTemplate.Editions;
using RASP.AbpTemplate.MultiTenancy.Payments.Dto;

namespace RASP.AbpTemplate.Web.Areas.App.Models.SubscriptionManagement
{
    [AutoMapTo(typeof(ExecutePaymentDto))]
    public class PaymentResultViewModel : SubscriptionPaymentDto
    {
        public EditionPaymentType EditionPaymentType { get; set; }
    }
}