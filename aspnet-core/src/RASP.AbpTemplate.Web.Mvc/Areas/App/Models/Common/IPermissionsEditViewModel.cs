﻿using System.Collections.Generic;
using RASP.AbpTemplate.Authorization.Permissions.Dto;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }

        List<string> GrantedPermissionNames { get; set; }
    }
}