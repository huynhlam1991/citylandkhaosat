﻿using System.Collections.Generic;
using RASP.AbpTemplate.Organizations.Dto;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Common
{
    public interface IOrganizationUnitsEditViewModel
    {
        List<OrganizationUnitDto> AllOrganizationUnits { get; set; }

        List<string> MemberedOrganizationUnits { get; set; }
    }
}