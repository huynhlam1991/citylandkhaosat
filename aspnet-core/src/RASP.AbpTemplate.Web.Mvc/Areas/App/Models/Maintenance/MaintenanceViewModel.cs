﻿using System.Collections.Generic;
using RASP.AbpTemplate.Caching.Dto;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Maintenance
{
    public class MaintenanceViewModel
    {
        public IReadOnlyList<CacheDto> Caches { get; set; }
    }
}