﻿using Abp.AutoMapper;
using RASP.AbpTemplate.Localization.Dto;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Languages
{
    [AutoMapFrom(typeof(GetLanguageForEditOutput))]
    public class CreateOrEditLanguageModalViewModel : GetLanguageForEditOutput
    {
        public bool IsEditMode => Language.Id.HasValue;
    }
}