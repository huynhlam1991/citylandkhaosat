using RASP.AbpTemplate.RSurvey.Dtos;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Answers
{
    public class CreateOrEditAnswerModalViewModel
    {
       public CreateOrEditAnswerDto Answer { get; set; }

	   		public string QuestionQuestionText { get; set;}


	   public bool IsEditMode => Answer.Id.HasValue;
    }
}