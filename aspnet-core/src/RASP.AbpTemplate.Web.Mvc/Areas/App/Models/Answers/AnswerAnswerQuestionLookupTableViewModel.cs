namespace RASP.AbpTemplate.Web.Areas.App.Models.Answers
{
    public class AnswerQuestionLookupTableViewModel
    {
        public long? Id { get; set; }

        public string DisplayName { get; set; }

        public string FilterText { get; set; }
    }
}