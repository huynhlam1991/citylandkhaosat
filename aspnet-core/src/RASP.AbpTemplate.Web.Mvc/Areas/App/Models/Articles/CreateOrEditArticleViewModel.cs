using RASP.AbpTemplate.RModel.Dtos;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Models.Articles
{
    public class CreateOrEditArticleModalViewModel
    {
       public CreateOrEditArticleDto Article { get; set; }

	   		public string RCategoryTitle { get; set;}


	   public bool IsEditMode => Article.Id.HasValue;
    }
}