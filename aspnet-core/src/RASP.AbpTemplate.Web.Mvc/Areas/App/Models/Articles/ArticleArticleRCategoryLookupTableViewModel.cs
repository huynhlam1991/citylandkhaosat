namespace RASP.AbpTemplate.Web.Areas.App.Models.Articles
{
    public class ArticleRCategoryLookupTableViewModel
    {
        public long? Id { get; set; }

        public string DisplayName { get; set; }

        public string FilterText { get; set; }
    }
}