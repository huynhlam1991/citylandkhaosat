using RASP.AbpTemplate.RModel.Dtos;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Models.RProductCategories
{
    public class CreateOrEditRProductCategoryModalViewModel
    {
       public CreateOrEditRProductCategoryDto RProductCategory { get; set; }

	   
	   public bool IsEditMode => RProductCategory.Id.HasValue;
    }
}