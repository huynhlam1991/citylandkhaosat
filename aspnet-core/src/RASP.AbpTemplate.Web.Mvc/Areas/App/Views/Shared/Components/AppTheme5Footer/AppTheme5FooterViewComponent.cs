﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.Web.Areas.App.Models.Layout;
using RASP.AbpTemplate.Web.Session;
using RASP.AbpTemplate.Web.Views;

namespace RASP.AbpTemplate.Web.Areas.App.Views.Shared.Components.AppTheme5Footer
{
    public class AppTheme5FooterViewComponent : AbpTemplateViewComponent
    {
        private readonly IPerRequestSessionCache _sessionCache;

        public AppTheme5FooterViewComponent(IPerRequestSessionCache sessionCache)
        {
            _sessionCache = sessionCache;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var footerModel = new FooterViewModel
            {
                LoginInformations = await _sessionCache.GetCurrentLoginInformationsAsync()
            };

            return View(footerModel);
        }
    }
}
