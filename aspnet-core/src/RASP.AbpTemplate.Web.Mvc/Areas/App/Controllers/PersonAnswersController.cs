using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.Web.Areas.App.Models.PersonAnswers;
using RASP.AbpTemplate.Web.Controllers;
using RASP.AbpTemplate.Authorization;
using RASP.AbpTemplate.RSurvey;
using RASP.AbpTemplate.RSurvey.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_PersonAnswers)]
    public class PersonAnswersController : AbpTemplateControllerBase
    {
        private readonly IPersonAnswersAppService _personAnswersAppService;

        public PersonAnswersController(IPersonAnswersAppService personAnswersAppService)
        {
            _personAnswersAppService = personAnswersAppService;
        }

        public ActionResult Index()
        {
            var model = new PersonAnswersViewModel
			{
				FilterText = ""
			};

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_PersonAnswers_Create, AppPermissions.Pages_Administration_PersonAnswers_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(long? id)
        {
			GetPersonAnswerForEditOutput getPersonAnswerForEditOutput;

			if (id.HasValue){
				getPersonAnswerForEditOutput = await _personAnswersAppService.GetPersonAnswerForEdit(new EntityDto<long> { Id = (long) id });
			}
			else{
				getPersonAnswerForEditOutput = new GetPersonAnswerForEditOutput{
					PersonAnswer = new CreateOrEditPersonAnswerDto()
				};
			}

            var viewModel = new CreateOrEditPersonAnswerModalViewModel()
            {
				PersonAnswer = getPersonAnswerForEditOutput.PersonAnswer,
					SurveyTitle = getPersonAnswerForEditOutput.SurveyTitle,
					QuestionQuestionText = getPersonAnswerForEditOutput.QuestionQuestionText,
					AnswerAnswerText = getPersonAnswerForEditOutput.AnswerAnswerText
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewPersonAnswerModal(long id)
        {
			var getPersonAnswerForViewDto = await _personAnswersAppService.GetPersonAnswerForView(id);

            var model = new PersonAnswerViewModel()
            {
				PersonAnswer = getPersonAnswerForViewDto.PersonAnswer
, SurveyTitle = getPersonAnswerForViewDto.SurveyTitle 
, QuestionQuestionText = getPersonAnswerForViewDto.QuestionQuestionText 
, AnswerAnswerText = getPersonAnswerForViewDto.AnswerAnswerText 

            };

            return PartialView("_ViewPersonAnswerModal", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_PersonAnswers_Create, AppPermissions.Pages_Administration_PersonAnswers_Edit)]
        public PartialViewResult SurveyLookupTableModal(long? id, string displayName)
        {
            var viewModel = new PersonAnswerSurveyLookupTableViewModel()
            {
                Id = id,
                DisplayName = displayName,
                FilterText = ""
            };

            return PartialView("_PersonAnswerSurveyLookupTableModal", viewModel);
        }
        [AbpMvcAuthorize(AppPermissions.Pages_Administration_PersonAnswers_Create, AppPermissions.Pages_Administration_PersonAnswers_Edit)]
        public PartialViewResult QuestionLookupTableModal(long? id, string displayName)
        {
            var viewModel = new PersonAnswerQuestionLookupTableViewModel()
            {
                Id = id,
                DisplayName = displayName,
                FilterText = ""
            };

            return PartialView("_PersonAnswerQuestionLookupTableModal", viewModel);
        }
        [AbpMvcAuthorize(AppPermissions.Pages_Administration_PersonAnswers_Create, AppPermissions.Pages_Administration_PersonAnswers_Edit)]
        public PartialViewResult AnswerLookupTableModal(long? id, string displayName)
        {
            var viewModel = new PersonAnswerAnswerLookupTableViewModel()
            {
                Id = id,
                DisplayName = displayName,
                FilterText = ""
            };

            return PartialView("_PersonAnswerAnswerLookupTableModal", viewModel);
        }

    }
}