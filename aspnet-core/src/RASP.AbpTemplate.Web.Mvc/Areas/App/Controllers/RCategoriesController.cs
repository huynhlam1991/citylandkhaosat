using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.Web.Areas.App.Models.RCategories;
using RASP.AbpTemplate.Web.Controllers;
using RASP.AbpTemplate.Authorization;
using RASP.AbpTemplate.RModel;
using RASP.AbpTemplate.RModel.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_RCategories)]
    public class RCategoriesController : AbpTemplateControllerBase
    {
        private readonly IRCategoriesAppService _rCategoriesAppService;

        public RCategoriesController(IRCategoriesAppService rCategoriesAppService)
        {
            _rCategoriesAppService = rCategoriesAppService;
        }

        public ActionResult Index()
        {
            var model = new RCategoriesViewModel
			{
				FilterText = ""
			};

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_RCategories_Create, AppPermissions.Pages_Administration_RCategories_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(long? id)
        {
			GetRCategoryForEditOutput getRCategoryForEditOutput;

			if (id.HasValue){
				getRCategoryForEditOutput = await _rCategoriesAppService.GetRCategoryForEdit(new EntityDto<long> { Id = (long) id });
			}
			else{
				getRCategoryForEditOutput = new GetRCategoryForEditOutput{
					RCategory = new CreateOrEditRCategoryDto()
				};
			}

            var viewModel = new CreateOrEditRCategoryModalViewModel()
            {
				RCategory = getRCategoryForEditOutput.RCategory
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewRCategoryModal(long id)
        {
			var getRCategoryForViewDto = await _rCategoriesAppService.GetRCategoryForView(id);

            var model = new RCategoryViewModel()
            {
				RCategory = getRCategoryForViewDto.RCategory

            };

            return PartialView("_ViewRCategoryModal", model);
        }


    }
}