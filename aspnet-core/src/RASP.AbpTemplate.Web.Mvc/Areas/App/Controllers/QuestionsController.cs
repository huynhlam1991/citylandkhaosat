using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.Web.Areas.App.Models.Questions;
using RASP.AbpTemplate.Web.Controllers;
using RASP.AbpTemplate.Authorization;
using RASP.AbpTemplate.RSurvey;
using RASP.AbpTemplate.RSurvey.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_Questions)]
    public class QuestionsController : AbpTemplateControllerBase
    {
        private readonly IQuestionsAppService _questionsAppService;

        public QuestionsController(IQuestionsAppService questionsAppService)
        {
            _questionsAppService = questionsAppService;
        }

        public ActionResult Index()
        {
            var model = new QuestionsViewModel
			{
				FilterText = ""
			};

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_Questions_Create, AppPermissions.Pages_Administration_Questions_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(long? id)
        {
			GetQuestionForEditOutput getQuestionForEditOutput;

			if (id.HasValue){
				getQuestionForEditOutput = await _questionsAppService.GetQuestionForEdit(new EntityDto<long> { Id = (long) id });
			}
			else{
				getQuestionForEditOutput = new GetQuestionForEditOutput{
					Question = new CreateOrEditQuestionDto()
				};
			}

            var viewModel = new CreateOrEditQuestionModalViewModel()
            {
				Question = getQuestionForEditOutput.Question,
					SurveyTitle = getQuestionForEditOutput.SurveyTitle
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewQuestionModal(long id)
        {
			var getQuestionForViewDto = await _questionsAppService.GetQuestionForView(id);

            var model = new QuestionViewModel()
            {
				Question = getQuestionForViewDto.Question
, SurveyTitle = getQuestionForViewDto.SurveyTitle 

            };

            return PartialView("_ViewQuestionModal", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_Questions_Create, AppPermissions.Pages_Administration_Questions_Edit)]
        public PartialViewResult SurveyLookupTableModal(long? id, string displayName)
        {
            var viewModel = new QuestionSurveyLookupTableViewModel()
            {
                Id = id,
                DisplayName = displayName,
                FilterText = ""
            };

            return PartialView("_QuestionSurveyLookupTableModal", viewModel);
        }

    }
}