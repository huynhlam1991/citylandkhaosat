using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.Web.Areas.App.Models.RProductCategories;
using RASP.AbpTemplate.Web.Controllers;
using RASP.AbpTemplate.Authorization;
using RASP.AbpTemplate.RModel;
using RASP.AbpTemplate.RModel.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_RProductCategories)]
    public class RProductCategoriesController : AbpTemplateControllerBase
    {
        private readonly IRProductCategoriesAppService _rProductCategoriesAppService;

        public RProductCategoriesController(IRProductCategoriesAppService rProductCategoriesAppService)
        {
            _rProductCategoriesAppService = rProductCategoriesAppService;
        }

        public ActionResult Index()
        {
            var model = new RProductCategoriesViewModel
			{
				FilterText = ""
			};

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_RProductCategories_Create, AppPermissions.Pages_Administration_RProductCategories_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(long? id)
        {
			GetRProductCategoryForEditOutput getRProductCategoryForEditOutput;

			if (id.HasValue){
				getRProductCategoryForEditOutput = await _rProductCategoriesAppService.GetRProductCategoryForEdit(new EntityDto<long> { Id = (long) id });
			}
			else{
				getRProductCategoryForEditOutput = new GetRProductCategoryForEditOutput{
					RProductCategory = new CreateOrEditRProductCategoryDto()
				};
			}

            var viewModel = new CreateOrEditRProductCategoryModalViewModel()
            {
				RProductCategory = getRProductCategoryForEditOutput.RProductCategory
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewRProductCategoryModal(long id)
        {
			var getRProductCategoryForViewDto = await _rProductCategoriesAppService.GetRProductCategoryForView(id);

            var model = new RProductCategoryViewModel()
            {
				RProductCategory = getRProductCategoryForViewDto.RProductCategory

            };

            return PartialView("_ViewRProductCategoryModal", model);
        }


    }
}