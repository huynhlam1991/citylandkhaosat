﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.Web.Controllers;
using RASP.AbpTemplate.Web.Startup;

namespace RASP.AbpTemplate.Web.Areas.App.Controllers
{
    
    [Area("App")]
    public class ServiceSurveyController : AbpTemplateControllerBase
    {
        public ActionResult Index()
        {
            //HttpContext.Session.Clear();
            return View();
        }
        public ActionResult Result(string id, string name, string email)
        {
            ViewBag.SurveyName = name;
            ViewBag.Email = email;
            return View();
        }
        public ActionResult Survey(string id, string name)
        {
            ViewBag.SurveyName = name;
            return View();
        }
        public ActionResult SurveyCreator(string id, string name)
        {
            ViewBag.SurveyName = name;
            return View();
        }
        public ActionResult ResultAnalytics(string id, string name)
        {
            ViewBag.SurveyName = name;
            return View();
        }
        //[HttpGet("getActive")]
        //public JsonResult getActive()
        //{
        //    var db = new SessionStorage(HttpContext.Session);
        //    return Json(db.GetSurveys());
        //}

        //[HttpGet("getSurvey")]
        //public string getSurvey(string surveyId)
        //{
        //    var db = new SessionStorage(HttpContext.Session);
        //    return db.GetSurvey(surveyId);
        //}

        //[HttpGet("create")]
        //public JsonResult create(string name)
        //{
        //    var db = new SessionStorage(HttpContext.Session);
        //    db.StoreSurvey(name, "{}");
        //    return Json("Ok");
        //}

        //[HttpGet("changeName")]
        //public JsonResult changeName(string id, string name)
        //{
        //    var db = new SessionStorage(HttpContext.Session);
        //    db.ChangeName(id, name);
        //    return Json("Ok");
        //}

        //[HttpPost("changeJson")]
        //public string changeJson([FromBody]ChangeSurveyModel model)
        //{
        //    var db = new SessionStorage(HttpContext.Session);
        //    db.StoreSurvey(model.Id, model.Json);
        //    return db.GetSurvey(model.Id);
        //}

        //[HttpGet("delete")]
        //public JsonResult delete(string id)
        //{
        //    var db = new SessionStorage(HttpContext.Session);
        //    db.DeleteSurvey(id);
        //    return Json("Ok");
        //}

        //[HttpPost("post")]
        //public JsonResult postResult([FromBody]PostSurveyResultModel model)
        //{
        //    var db = new SessionStorage(HttpContext.Session);
        //    db.PostResults(model.postId, model.surveyResult);
        //    return Json("Ok");
        //}

        //[HttpGet("results")]
        //public JsonResult getResults(string postId)
        //{
        //    var db = new SessionStorage(HttpContext.Session);
        //    return Json(db.GetResults(postId));
        //}

        // // GET api/values/5
        // [HttpGet("{id}")]
        // public string Get(int id)
        // {
        //     return "value";
        // }

        // // POST api/values
        // [HttpPost]
        // public void Post([FromBody]string value)
        // {
        // }

        // // PUT api/values/5
        // [HttpPut("{id}")]
        // public void Put(int id, [FromBody]string value)
        // {
        // }

        // // DELETE api/values/5
        // [HttpDelete("{id}")]
        // public void Delete(int id)
        // {
        // }
    }
}