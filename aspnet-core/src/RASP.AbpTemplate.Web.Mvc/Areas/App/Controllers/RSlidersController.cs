using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.Web.Areas.App.Models.RSliders;
using RASP.AbpTemplate.Web.Controllers;
using RASP.AbpTemplate.Authorization;
using RASP.AbpTemplate.RModel;
using RASP.AbpTemplate.RModel.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_RSliders)]
    public class RSlidersController : AbpTemplateControllerBase
    {
        private readonly IRSlidersAppService _rSlidersAppService;

        public RSlidersController(IRSlidersAppService rSlidersAppService)
        {
            _rSlidersAppService = rSlidersAppService;
        }

        public ActionResult Index()
        {
            var model = new RSlidersViewModel
			{
				FilterText = ""
			};

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_RSliders_Create, AppPermissions.Pages_Administration_RSliders_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
			GetRSliderForEditOutput getRSliderForEditOutput;

			if (id.HasValue){
				getRSliderForEditOutput = await _rSlidersAppService.GetRSliderForEdit(new EntityDto { Id = (int) id });
			}
			else{
				getRSliderForEditOutput = new GetRSliderForEditOutput{
					RSlider = new CreateOrEditRSliderDto()
				};
			}

            var viewModel = new CreateOrEditRSliderModalViewModel()
            {
				RSlider = getRSliderForEditOutput.RSlider
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewRSliderModal(int id)
        {
			var getRSliderForViewDto = await _rSlidersAppService.GetRSliderForView(id);

            var model = new RSliderViewModel()
            {
				RSlider = getRSliderForViewDto.RSlider

            };

            return PartialView("_ViewRSliderModal", model);
        }


    }
}