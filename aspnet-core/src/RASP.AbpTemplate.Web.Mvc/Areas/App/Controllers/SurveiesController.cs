using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.Web.Areas.App.Models.Surveies;
using RASP.AbpTemplate.Web.Controllers;
using RASP.AbpTemplate.Authorization;
using RASP.AbpTemplate.RSurvey;
using RASP.AbpTemplate.RSurvey.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace RASP.AbpTemplate.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_Surveies)]
    public class SurveiesController : AbpTemplateControllerBase
    {
        private readonly ISurveiesAppService _surveiesAppService;
        private readonly IHostingEnvironment _hostingEnvironment;
        public SurveiesController(ISurveiesAppService surveiesAppService,
            IHostingEnvironment hostingEnvironment)
        {
            _surveiesAppService = surveiesAppService;
            _hostingEnvironment = hostingEnvironment;
        }

        public ActionResult Index()
        {
            var model = new SurveiesViewModel
			{
				FilterText = ""
			};

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_Surveies_Create, AppPermissions.Pages_Administration_Surveies_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(long? id)
        {
			GetSurveyForEditOutput getSurveyForEditOutput;

			if (id.HasValue){
				getSurveyForEditOutput = await _surveiesAppService.GetSurveyForEdit(new EntityDto<long> { Id = (long) id });
			}
			else{
				getSurveyForEditOutput = new GetSurveyForEditOutput{
					Survey = new CreateOrEditSurveyDto()
				};
				getSurveyForEditOutput.Survey.StartDate = DateTime.Now;
				getSurveyForEditOutput.Survey.EndDate = DateTime.Now;
			}

            var viewModel = new CreateOrEditSurveyModalViewModel()
            {
				Survey = getSurveyForEditOutput.Survey
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewSurveyModal(long id)
        {
			var getSurveyForViewDto = await _surveiesAppService.GetSurveyForView(id);

            var model = new SurveyViewModel()
            {
				Survey = getSurveyForViewDto.Survey

            };

            return PartialView("_ViewSurveyModal", model);
        }
        public ActionResult DownLoadFile(long surveyId)
        {
            var webRoot = _hostingEnvironment.WebRootPath;
            var strFullPath = webRoot + string.Format("\\data\\survey\\{0}", "excelexport.xlsx");
            
            byte[] fileBytes = System.IO.File.ReadAllBytes(strFullPath);
            var fileName = "SurveyExport_"+ surveyId + "_" + DateTime.Now.ToString("ddMMyyyyHHmm") + ".xlsx";
            return File(fileBytes, "application/force-download", fileName);
        }

    }
}