using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.Web.Areas.App.Models.RMailCustomers;
using RASP.AbpTemplate.Web.Controllers;
using RASP.AbpTemplate.Authorization;
using RASP.AbpTemplate.RMarketing;
using RASP.AbpTemplate.RMarketing.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_RMailCustomers)]
    public class RMailCustomersController : AbpTemplateControllerBase
    {
        private readonly IRMailCustomersAppService _rMailCustomersAppService;

        public RMailCustomersController(IRMailCustomersAppService rMailCustomersAppService)
        {
            _rMailCustomersAppService = rMailCustomersAppService;
        }

        public ActionResult Index()
        {
            var model = new RMailCustomersViewModel
			{
				FilterText = ""
			};

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_RMailCustomers_Create, AppPermissions.Pages_Administration_RMailCustomers_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(Guid? id)
        {
			GetRMailCustomerForEditOutput getRMailCustomerForEditOutput;

			if (id.HasValue){
				getRMailCustomerForEditOutput = await _rMailCustomersAppService.GetRMailCustomerForEdit(new EntityDto<Guid> { Id = (Guid) id });
			}
			else{
				getRMailCustomerForEditOutput = new GetRMailCustomerForEditOutput{
					RMailCustomer = new CreateOrEditRMailCustomerDto()
				};
			}

            var viewModel = new CreateOrEditRMailCustomerModalViewModel()
            {
				RMailCustomer = getRMailCustomerForEditOutput.RMailCustomer
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewRMailCustomerModal(Guid id)
        {
			var getRMailCustomerForViewDto = await _rMailCustomersAppService.GetRMailCustomerForView(id);

            var model = new RMailCustomerViewModel()
            {
				RMailCustomer = getRMailCustomerForViewDto.RMailCustomer

            };

            return PartialView("_ViewRMailCustomerModal", model);
        }


    }
}