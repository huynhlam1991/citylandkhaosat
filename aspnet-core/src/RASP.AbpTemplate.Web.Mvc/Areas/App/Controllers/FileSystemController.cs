﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Abp.Web.Models;
using elFinder.NetCore;
using elFinder.NetCore.Drivers.FileSystem;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.Web.Controllers;
using RASP.AbpTemplate.Web.Startup;

namespace RASP.AbpTemplate.Web.Areas.App.Controllers
{
    [DontWrapResult]
    [Area("App")]
    [IgnoreAntiforgeryToken]
    public class FileSystemController : AbpTemplateControllerBase
    {
        public FileSystemController()
        {
        }
        [IgnoreAntiforgeryToken]
        public async Task<ActionResult> Connector()
        {
            var connector = GetConnector();
            return await connector.ProcessAsync(Request);
        }
        [IgnoreAntiforgeryToken]
        
        public async Task<ActionResult> Thumb(string id)
        {
            var connector = GetConnector();
            return await connector.GetThumbnailAsync(HttpContext.Request, HttpContext.Response, id);
        }

        private Connector GetConnector()
        {
            var driver = new FileSystemDriver();

            string absoluteUrl = UriHelper.BuildAbsolute(Request.Scheme, Request.Host);
            var uri = new Uri(absoluteUrl);

            var root = new RootVolume(
                RASP.AbpTemplate.Web.Startup.Startup.MapPath("~/Files"),
                $"http://{uri.Authority}/Files/",
                $"http://{uri.Authority}/App/FileSystem/Thumb/")
            {
                //IsReadOnly = !User.IsInRole("Administrators")
                IsReadOnly = false, // Can be readonly according to user's membership permission
                IsLocked = false, // If locked, files and directories cannot be deleted, renamed or moved
                Alias = "Files", // Beautiful name given to the root/home folder
                //MaxUploadSizeInKb = 2048, // Limit imposed to user uploaded file <= 2048 KB
                //LockedFolders = new List<string>(new string[] { "Folder1" })
            };

            driver.AddRoot(root);

            return new Connector(driver);
        }
        //public string MapPath(string path, string basePath = null)
        //{
        //    if (string.IsNullOrEmpty(basePath))
        //    {
        //        basePath = _env.WebRootPath;
        //    }

        //    path = path.Replace("~/", "").TrimStart('/').Replace('/', '\\');
        //    return Path.Combine(basePath, path);
        //}
    }
}