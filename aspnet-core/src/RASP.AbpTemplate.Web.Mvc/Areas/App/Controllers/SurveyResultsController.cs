using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.Web.Areas.App.Models.SurveyResults;
using RASP.AbpTemplate.Web.Controllers;
using RASP.AbpTemplate.Authorization;
using RASP.AbpTemplate.RSurvey;
using RASP.AbpTemplate.RSurvey.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;
using Abp.Domain.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace RASP.AbpTemplate.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_SurveyResults)]
    public class SurveyResultsController : AbpTemplateControllerBase
    {
        private readonly ISurveyResultsAppService _surveyResultsAppService;

        public SurveyResultsController(ISurveyResultsAppService surveyResultsAppService)
        {
            _surveyResultsAppService = surveyResultsAppService;
        }

        public ActionResult Index()
        {
            var model = new SurveyResultsViewModel
			{
				FilterText = ""
			};

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_SurveyResults_Create, AppPermissions.Pages_Administration_SurveyResults_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(long? id)
        {
			GetSurveyResultForEditOutput getSurveyResultForEditOutput;

			if (id.HasValue){
				getSurveyResultForEditOutput = await _surveyResultsAppService.GetSurveyResultForEdit(new EntityDto<long> { Id = (long) id });
			}
			else{
				getSurveyResultForEditOutput = new GetSurveyResultForEditOutput{
					SurveyResult = new CreateOrEditSurveyResultDto()
				};
			}

            var viewModel = new CreateOrEditSurveyResultModalViewModel()
            {
				SurveyResult = getSurveyResultForEditOutput.SurveyResult,
					SurveyTitle = getSurveyResultForEditOutput.SurveyTitle,
					UserName = getSurveyResultForEditOutput.UserName
            };
            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewSurveyResultModal(long id)
        {
			var getSurveyResultForViewDto = await _surveyResultsAppService.GetSurveyResultForView(id);

            var model = new SurveyResultViewModel()
            {
				SurveyResult = getSurveyResultForViewDto.SurveyResult
, SurveyTitle = getSurveyResultForViewDto.SurveyTitle 
, UserName = getSurveyResultForViewDto.UserName 

            };

            return PartialView("_ViewSurveyResultModal", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_SurveyResults_Create, AppPermissions.Pages_Administration_SurveyResults_Edit)]
        public PartialViewResult SurveyLookupTableModal(long? id, string displayName)
        {
            var viewModel = new SurveyResultSurveyLookupTableViewModel()
            {
                Id = id,
                DisplayName = displayName,
                FilterText = ""
            };

            return PartialView("_SurveyResultSurveyLookupTableModal", viewModel);
        }
        [AbpMvcAuthorize(AppPermissions.Pages_Administration_SurveyResults_Create, AppPermissions.Pages_Administration_SurveyResults_Edit)]
        public PartialViewResult UserLookupTableModal(long? id, string displayName)
        {
            var viewModel = new SurveyResultUserLookupTableViewModel()
            {
                Id = id,
                DisplayName = displayName,
                FilterText = ""
            };

            return PartialView("_SurveyResultUserLookupTableModal", viewModel);
        }

    }
}