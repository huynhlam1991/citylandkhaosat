﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.Web.Controllers;

namespace RASP.AbpTemplate.Web.Areas.App.Controllers
{
    [Area("App")]
    public class TikiDrawDataController : AbpTemplateControllerBase
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}