using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.Web.Areas.App.Models.Answers;
using RASP.AbpTemplate.Web.Controllers;
using RASP.AbpTemplate.Authorization;
using RASP.AbpTemplate.RSurvey;
using RASP.AbpTemplate.RSurvey.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_Answers)]
    public class AnswersController : AbpTemplateControllerBase
    {
        private readonly IAnswersAppService _answersAppService;

        public AnswersController(IAnswersAppService answersAppService)
        {
            _answersAppService = answersAppService;
        }

        public ActionResult Index()
        {
            var model = new AnswersViewModel
			{
				FilterText = ""
			};

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_Answers_Create, AppPermissions.Pages_Administration_Answers_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(long? id)
        {
			GetAnswerForEditOutput getAnswerForEditOutput;

			if (id.HasValue){
				getAnswerForEditOutput = await _answersAppService.GetAnswerForEdit(new EntityDto<long> { Id = (long) id });
			}
			else{
				getAnswerForEditOutput = new GetAnswerForEditOutput{
					Answer = new CreateOrEditAnswerDto()
				};
			}

            var viewModel = new CreateOrEditAnswerModalViewModel()
            {
				Answer = getAnswerForEditOutput.Answer,
					QuestionQuestionText = getAnswerForEditOutput.QuestionQuestionText
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewAnswerModal(long id)
        {
			var getAnswerForViewDto = await _answersAppService.GetAnswerForView(id);

            var model = new AnswerViewModel()
            {
				Answer = getAnswerForViewDto.Answer
, QuestionQuestionText = getAnswerForViewDto.QuestionQuestionText 

            };

            return PartialView("_ViewAnswerModal", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_Answers_Create, AppPermissions.Pages_Administration_Answers_Edit)]
        public PartialViewResult QuestionLookupTableModal(long? id, string displayName)
        {
            var viewModel = new AnswerQuestionLookupTableViewModel()
            {
                Id = id,
                DisplayName = displayName,
                FilterText = ""
            };

            return PartialView("_AnswerQuestionLookupTableModal", viewModel);
        }

    }
}