using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.Web.Areas.App.Models.Articles;
using RASP.AbpTemplate.Web.Controllers;
using RASP.AbpTemplate.Authorization;
using RASP.AbpTemplate.RModel;
using RASP.AbpTemplate.RModel.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_Articles)]
    public class ArticlesController : AbpTemplateControllerBase
    {
        private readonly IArticlesAppService _articlesAppService;

        public ArticlesController(IArticlesAppService articlesAppService)
        {
            _articlesAppService = articlesAppService;
        }

        public ActionResult Index()
        {
            var model = new ArticlesViewModel
			{
				FilterText = ""
			};

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_Articles_Create, AppPermissions.Pages_Administration_Articles_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(long? id)
        {
			GetArticleForEditOutput getArticleForEditOutput;

			if (id.HasValue){
				getArticleForEditOutput = await _articlesAppService.GetArticleForEdit(new EntityDto<long> { Id = (long) id });
			}
			else{
				getArticleForEditOutput = new GetArticleForEditOutput{
					Article = new CreateOrEditArticleDto()
				};
			}

            var viewModel = new CreateOrEditArticleModalViewModel()
            {
				Article = getArticleForEditOutput.Article,
					RCategoryTitle = getArticleForEditOutput.RCategoryTitle
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewArticleModal(long id)
        {
			var getArticleForViewDto = await _articlesAppService.GetArticleForView(id);

            var model = new ArticleViewModel()
            {
				Article = getArticleForViewDto.Article
, RCategoryTitle = getArticleForViewDto.RCategoryTitle 

            };

            return PartialView("_ViewArticleModal", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_Articles_Create, AppPermissions.Pages_Administration_Articles_Edit)]
        public PartialViewResult RCategoryLookupTableModal(long? id, string displayName)
        {
            var viewModel = new ArticleRCategoryLookupTableViewModel()
            {
                Id = id,
                DisplayName = displayName,
                FilterText = ""
            };

            return PartialView("_ArticleRCategoryLookupTableModal", viewModel);
        }

    }
}