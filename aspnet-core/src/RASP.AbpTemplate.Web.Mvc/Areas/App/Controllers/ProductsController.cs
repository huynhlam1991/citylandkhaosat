using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using RASP.AbpTemplate.Web.Areas.App.Models.Products;
using RASP.AbpTemplate.Web.Controllers;
using RASP.AbpTemplate.Authorization;
using RASP.AbpTemplate.RModel;
using RASP.AbpTemplate.RModel.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace RASP.AbpTemplate.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_Products)]
    public class ProductsController : AbpTemplateControllerBase
    {
        private readonly IProductsAppService _productsAppService;

        public ProductsController(IProductsAppService productsAppService)
        {
            _productsAppService = productsAppService;
        }

        public ActionResult Index()
        {
            var model = new ProductsViewModel
			{
				FilterText = ""
			};

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_Products_Create, AppPermissions.Pages_Administration_Products_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(long? id)
        {
			GetProductForEditOutput getProductForEditOutput;

			if (id.HasValue){
				getProductForEditOutput = await _productsAppService.GetProductForEdit(new EntityDto<long> { Id = (long) id });
			}
			else{
				getProductForEditOutput = new GetProductForEditOutput{
					Product = new CreateOrEditProductDto()
				};
			}

            var viewModel = new CreateOrEditProductModalViewModel()
            {
				Product = getProductForEditOutput.Product,
					RProductCategoryTitle = getProductForEditOutput.RProductCategoryTitle
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewProductModal(long id)
        {
			var getProductForViewDto = await _productsAppService.GetProductForView(id);

            var model = new ProductViewModel()
            {
				Product = getProductForViewDto.Product
, RProductCategoryTitle = getProductForViewDto.RProductCategoryTitle 

            };

            return PartialView("_ViewProductModal", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_Products_Create, AppPermissions.Pages_Administration_Products_Edit)]
        public PartialViewResult RProductCategoryLookupTableModal(long? id, string displayName)
        {
            var viewModel = new ProductRProductCategoryLookupTableViewModel()
            {
                Id = id,
                DisplayName = displayName,
                FilterText = ""
            };

            return PartialView("_ProductRProductCategoryLookupTableModal", viewModel);
        }

    }
}