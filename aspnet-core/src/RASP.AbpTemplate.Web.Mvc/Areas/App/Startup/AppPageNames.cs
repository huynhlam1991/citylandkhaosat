namespace RASP.AbpTemplate.Web.Areas.App.Startup
{
    public class AppPageNames
    {
        public static class Common
        {
            public const string RMailCustomers = "Administration.RMarketing.RMailCustomers";
            public const string SurveyResults = "Administration.RSurvey.SurveyResults";
            public const string PersonAnswers = "Administration.RSurvey.PersonAnswers";
            public const string Answers = "Administration.RSurvey.Answers";
            public const string Questions = "Administration.RSurvey.Questions";
            public const string Surveies = "Administration.RSurvey.Surveies";
            public const string RProductCategories = "Administration.RModel.RProductCategories";
            public const string Products = "Administration.RModel.Products";
            public const string Coupons = "Administration.RModel.Coupons";
            public const string RSliders = "Administration.RModel.RSliders";
            public const string Articles = "Administration.RModel.Articles";
            public const string RCategories = "Administration.RModel.RCategories";
            public const string Administration = "Administration";
            public const string Roles = "Administration.Roles";
            public const string Users = "Administration.Users";
            public const string AuditLogs = "Administration.AuditLogs";
            public const string OrganizationUnits = "Administration.OrganizationUnits";
            public const string Languages = "Administration.Languages";
            public const string DemoUiComponents = "Administration.DemoUiComponents";
            public const string UiCustomization = "Administration.UiCustomization";
        }

        public static class Host
        {
            public const string Tenants = "Tenants";
            public const string Editions = "Editions";
            public const string Maintenance = "Administration.Maintenance";
            public const string Settings = "Administration.Settings.Host";
            public const string Dashboard = "Dashboard";
        }

        public static class Tenant
        {
            public const string Dashboard = "Dashboard.Tenant";
            public const string Settings = "Administration.Settings.Tenant";
            public const string SubscriptionManagement = "Administration.SubscriptionManagement.Tenant";
        }
    }
}
