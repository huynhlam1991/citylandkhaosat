using Abp.Application.Navigation;
using Abp.Authorization;
using Abp.Localization;
using RASP.AbpTemplate.Authorization;

namespace RASP.AbpTemplate.Web.Areas.App.Startup
{
    public class AppNavigationProvider : NavigationProvider
    {
        public const string MenuName = "App";

        public override void SetNavigation(INavigationProviderContext context)
        {
            var menu = context.Manager.Menus[MenuName] = new MenuDefinition(MenuName, new FixedLocalizableString("Main Menu"));

            menu
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Host.Dashboard,
                        L("Dashboard"),
                        url: "App/HostDashboard",
                        icon: "flaticon-line-graph",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Host_Dashboard)
                    )
                ).AddItem(new MenuItemDefinition(
                    AppPageNames.Host.Tenants,
                    L("Tenants"),
                    url: "App/Tenants",
                    icon: "flaticon-list-3",
                    permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Tenants)
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Host.Editions,
                        L("Editions"),
                        url: "App/Editions",
                        icon: "flaticon-app",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Editions)
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Tenant.Dashboard,
                        L("Dashboard"),
                        url: "App/Dashboard",
                        icon: "flaticon-line-graph",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Tenant_Dashboard)
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Administration,
                        L("Administration"),
                        icon: "flaticon-interface-8"
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.OrganizationUnits,
                            L("OrganizationUnits"),
                            url: "App/OrganizationUnits",
                            icon: "flaticon-map",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_OrganizationUnits)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.Roles,
                            L("Roles"),
                            url: "App/Roles",
                            icon: "flaticon-suitcase",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Roles)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.Users,
                            L("Users"),
                            url: "App/Users",
                            icon: "flaticon-users",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Users)
                        )
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.RMailCustomers,
                        L("RMailCustomers"),
                        url: "App/RMailCustomers",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_RMailCustomers
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.SurveyResults,
                        L("SurveyResults"),
                        url: "App/SurveyResults",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_SurveyResults
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.PersonAnswers,
                        L("PersonAnswers"),
                        url: "App/PersonAnswers",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_PersonAnswers
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Answers,
                        L("Answers"),
                        url: "App/Answers",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_Answers
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Questions,
                        L("Questions"),
                        url: "App/Questions",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_Questions
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Surveies,
                        L("Surveies"),
                        url: "App/Surveies",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_Surveies
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.RProductCategories,
                        L("RProductCategories"),
                        url: "App/RProductCategories",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_RProductCategories
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Products,
                        L("Products"),
                        url: "App/Products",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_Products
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Coupons,
                        L("Coupons"),
                        url: "App/Coupons",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_Coupons
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.RSliders,
                        L("RSliders"),
                        url: "App/RSliders",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_RSliders
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Articles,
                        L("Articles"),
                        url: "App/Articles",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_Articles
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.RCategories,
                        L("RCategories"),
                        url: "App/RCategories",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_RCategories
                    )
                ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.Languages,
                            L("Languages"),
                            url: "App/Languages",
                            icon: "flaticon-tabs",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Languages)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.AuditLogs,
                            L("AuditLogs"),
                            url: "App/AuditLogs",
                            icon: "flaticon-folder-1",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_AuditLogs)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Host.Maintenance,
                            L("Maintenance"),
                            url: "App/Maintenance",
                            icon: "flaticon-lock",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Host_Maintenance)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.SubscriptionManagement,
                            L("Subscription"),
                            url: "App/SubscriptionManagement",
                            icon: "flaticon-refresh",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.UiCustomization,
                            L("VisualSettings"),
                            url: "App/UiCustomization",
                            icon: "flaticon-medical",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_UiCustomization)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Host.Settings,
                            L("Settings"),
                            url: "App/HostSettings",
                            icon: "flaticon-settings",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Host_Settings)
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.Settings,
                            L("Settings"),
                            url: "App/Settings",
                            icon: "flaticon-settings",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Tenant_Settings)
                        )
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.DemoUiComponents,
                        L("DemoUiComponents"),
                        url: "App/DemoUiComponents",
                        icon: "flaticon-shapes",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_DemoUiComponents)
                    )
                );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, AbpTemplateConsts.LocalizationSourceName);
        }
    }
}