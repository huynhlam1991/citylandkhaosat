﻿var app = app || {};
(function () {
    debugger;
    //Check if SignalR is defined
    if (!signalR) {
        return;
    }

    //Create namespaces
    app.signalr = app.signalr || {};
    app.signalr.hubs = app.signalr.hubs || {};

    var tikiHub = null;

    // Configure the connection
    function configureConnection(connection) {
        // Set the common hub
        abp.signalr.hubs.tiki = connection;
        tikiHub = connection;

        // Reconnect if hub disconnects
        connection.onclose(function (e) {
            if (e) {
                abp.log.debug('Tiki connection closed with error: ' + e);
            }
            else {
                abp.log.debug('Tiki disconnected');
            }

            if (!abp.signalr.autoConnect) {
                return;
            }

            setTimeout(function () {
                connection.start();
            }, 5000);
        });

        // Register to get notifications
        registerTikiEvents(connection);
    }

    // Connect to the server
    abp.signalr.connect = function () {
        // Start the connection.
        startConnection(abp.appPath + 'signalr-tiki', configureConnection)
            .then(function (connection) {
                abp.log.debug('Connected to SignalR Tiki server!');
                abp.event.trigger('app.tiki.connected');

                // Call the Register method on the hub.
                connection.invoke('register').then(function () {
                    abp.log.debug('Registered to the SignalR Tiki server!');
                });
            })
            .catch(function (error) {
                abp.log.debug(error.message);
            });
    };

    // Starts a connection with transport fallback - if the connection cannot be started using
    // the webSockets transport the function will fallback to the serverSentEvents transport and
    // if this does not work it will try longPolling. If the connection cannot be started using
    // any of the available transports the function will return a rejected Promise.
    function startConnection(url, configureConnection) {
        if (abp.signalr.remoteServiceBaseUrl) {
            url = abp.signalr.remoteServiceBaseUrl + url;
        }

        // Add query string: https://github.com/aspnet/SignalR/issues/680
        if (abp.signalr.qs) {
            url += '?' + abp.signalr.qs;
        }

        return function start(transport) {
            abp.log.debug('Starting connection using ' + signalR.HttpTransportType[transport] + ' transport');

            var connection = new signalR.HubConnectionBuilder()
                .withUrl(url, transport)
                .build();

            if (configureConnection && typeof configureConnection === 'function') {
                configureConnection(connection);
            }

            return connection.start()
                .then(function () {
                    return connection;
                })
                .catch(function (error) {
                    abp.log.debug('Cannot start the connection using ' + signalR.HttpTransportType[transport] + ' transport. ' + error.message);
                    if (transport !== signalR.HttpTransportType.LongPolling) {
                        return start(transport + 1);
                    }

                    return Promise.reject(error);
                });
        }(signalR.HttpTransportType.WebSockets);
    }

    abp.signalr.startConnection = startConnection;

    if (abp.signalr.autoConnect === undefined) {
        abp.signalr.autoConnect = true;
    }

    if (abp.signalr.autoConnect) {
        abp.signalr.connect();
    }

    function registerTikiEvents(connection) {
        connection.on('getTikiMessage', function (message) {
            if (message < 1000) {
                BatDauDem(message);
                $('#count').text(message);
            }
            
            //abp.event.trigger('app.tiki.messageReceived', message);
        });

        //connection.on('getAllFriends', function (friends) {
        //    abp.event.trigger('abp.tiki.friendListChanged', friends);
        //});

        //connection.on('getFriendshipRequest', function (friendData, isOwnRequest) {
        //    abp.event.trigger('app.tiki.friendshipRequestReceived', friendData, isOwnRequest);
        //});

        //connection.on('getUserConnectNotification', function (friend, isConnected) {
        //    abp.event.trigger('app.tiki.userConnectionStateChanged',
        //        {
        //            friend: friend,
        //            isConnected: isConnected
        //        });
        //});

        //connection.on('getUserStateChange', function (friend, state) {
        //    abp.event.trigger('app.tiki.userStateChanged',
        //        {
        //            friend: friend,
        //            state: state
        //        });
        //});

        //connection.on('getallUnreadMessagesOfUserRead', function (friend) {
        //    abp.event.trigger('app.tiki.allUnreadMessagesOfUserRead',
        //        {
        //            friend: friend
        //        });
        //});

        //connection.on('getReadStateChange', function (friend) {
        //    abp.event.trigger('app.tiki.readStateChange',
        //        {
        //            friend: friend
        //        });
        //});
    }
    
    app.tiki.sendMessage = function (messageData, callback) {

        if (tikiHub.connection.connectionState !== 1) {
            callback && callback();
            abp.notify.warn(app.localize('TikiIsNotConnectedWarning'));
            return;
        }

        tikiHub.invoke('sendMessage', messageData).then(function (result) {
            if (result) {
                abp.notify.warn(result);
            }

            callback && callback();
        });
    };

})();