(function ($) {
    app.modals.CreateOrEditAnswerModal = function () {

        var _answersService = abp.services.app.answers;

        var _modalManager;
        var _$answerInformationForm = null;

		        var _AnswerquestionLookupTableModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Answers/QuestionLookupTableModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Answers/_AnswerQuestionLookupTableModal.js',
            modalClass: 'QuestionLookupTableModal'
        });

        this.init = function (modalManager) {
            _modalManager = modalManager;

			var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });

            _$answerInformationForm = _modalManager.getModal().find('form[name=AnswerInformationsForm]');
            _$answerInformationForm.validate();
        };

		          $('#OpenQuestionLookupTableButton').click(function () {

            var answer = _$answerInformationForm.serializeFormToObject();

            _AnswerquestionLookupTableModal.open({ id: answer.questionId, displayName: answer.questionQuestionText }, function (data) {
                _$answerInformationForm.find('input[name=questionQuestionText]').val(data.displayName); 
                _$answerInformationForm.find('input[name=questionId]').val(data.id); 
            });
        });
		
		$('#ClearQuestionQuestionTextButton').click(function () {
                _$answerInformationForm.find('input[name=questionQuestionText]').val(''); 
                _$answerInformationForm.find('input[name=questionId]').val(''); 
        });
		


        this.save = function () {
            if (!_$answerInformationForm.valid()) {
                return;
            }
            if ($('#Answer_QuestionId').prop('required') && $('#Answer_QuestionId').val() == '') {
                abp.message.error(app.localize('{0}IsRequired', app.localize('Question')));
                return;
            }

            var answer = _$answerInformationForm.serializeFormToObject();
			
			 _modalManager.setBusy(true);
			 _answersService.createOrEdit(
				answer
			 ).done(function () {
               abp.notify.info(app.localize('SavedSuccessfully'));
               _modalManager.close();
               abp.event.trigger('app.createOrEditAnswerModalSaved');
			 }).always(function () {
               _modalManager.setBusy(false);
			});
        };
    };
})(jQuery);