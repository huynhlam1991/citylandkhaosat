(function ($) {
    app.modals.CreateOrEditQuestionModal = function () {

        var _questionsService = abp.services.app.questions;

        var _modalManager;
        var _$questionInformationForm = null;

		        var _QuestionsurveyLookupTableModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Questions/SurveyLookupTableModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Questions/_QuestionSurveyLookupTableModal.js',
            modalClass: 'SurveyLookupTableModal'
        });

        this.init = function (modalManager) {
            _modalManager = modalManager;

			var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });

            _$questionInformationForm = _modalManager.getModal().find('form[name=QuestionInformationsForm]');
            _$questionInformationForm.validate();
        };

		          $('#OpenSurveyLookupTableButton').click(function () {

            var question = _$questionInformationForm.serializeFormToObject();

            _QuestionsurveyLookupTableModal.open({ id: question.surveyId, displayName: question.surveyTitle }, function (data) {
                _$questionInformationForm.find('input[name=surveyTitle]').val(data.displayName); 
                _$questionInformationForm.find('input[name=surveyId]').val(data.id); 
            });
        });
		
		$('#ClearSurveyTitleButton').click(function () {
                _$questionInformationForm.find('input[name=surveyTitle]').val(''); 
                _$questionInformationForm.find('input[name=surveyId]').val(''); 
        });
		


        this.save = function () {
            if (!_$questionInformationForm.valid()) {
                return;
            }
            if ($('#Question_SurveyId').prop('required') && $('#Question_SurveyId').val() == '') {
                abp.message.error(app.localize('{0}IsRequired', app.localize('Survey')));
                return;
            }

            var question = _$questionInformationForm.serializeFormToObject();
			
			 _modalManager.setBusy(true);
			 _questionsService.createOrEdit(
				question
			 ).done(function () {
               abp.notify.info(app.localize('SavedSuccessfully'));
               _modalManager.close();
               abp.event.trigger('app.createOrEditQuestionModalSaved');
			 }).always(function () {
               _modalManager.setBusy(false);
			});
        };
    };
})(jQuery);