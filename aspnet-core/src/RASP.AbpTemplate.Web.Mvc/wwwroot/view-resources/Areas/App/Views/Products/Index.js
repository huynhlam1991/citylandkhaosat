(function () {
    $(function () {

        var _$productsTable = $('#ProductsTable');
        var _productsService = abp.services.app.products;
		var _entityTypeFullName = 'RASP.AbpTemplate.RModel.Product';
        
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Administration.Products.Create'),
            edit: abp.auth.hasPermission('Pages.Administration.Products.Edit'),
            'delete': abp.auth.hasPermission('Pages.Administration.Products.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Products/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Products/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditProductModal'
        });

		 var _viewProductModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Products/ViewproductModal',
            modalClass: 'ViewProductModal'
        });

		        var _entityTypeHistoryModal = app.modals.EntityTypeHistoryModal.create();
		        function entityHistoryIsEnabled() {
            return abp.custom.EntityHistory &&
                abp.custom.EntityHistory.IsEnabled &&
                _.filter(abp.custom.EntityHistory.EnabledEntities, entityType => entityType === _entityTypeFullName).length === 1;
        }

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }

        var dataTable = _$productsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _productsService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#ProductsTableFilter').val(),
					metaTitleFilter: $('#MetaTitleFilterId').val(),
					metaDescriptionFilter: $('#MetaDescriptionFilterId').val(),
					titleFilter: $('#TitleFilterId').val(),
					descriptionFilter: $('#DescriptionFilterId').val(),
					contentFilter: $('#ContentFilterId').val(),
					minPriorityFilter: $('#MinPriorityFilterId').val(),
					maxPriorityFilter: $('#MaxPriorityFilterId').val(),
					isAvailableFilter: $('#IsAvailableFilterId').val(),
					isHotFilter: $('#IsHotFilterId').val(),
					isNewFilter: $('#IsNewFilterId').val(),
					minPriceOriginFilter: $('#MinPriceOriginFilterId').val(),
					maxPriceOriginFilter: $('#MaxPriceOriginFilterId').val(),
					minPricePromotionFilter: $('#MinPricePromotionFilterId').val(),
					maxPricePromotionFilter: $('#MaxPricePromotionFilterId').val(),
					saleManFilter: $('#SaleManFilterId').val(),
					linkReferenceFilter: $('#LinkReferenceFilterId').val(),
					isContactFilter: $('#IsContactFilterId').val(),
					rProductCategoryTitleFilter: $('#RProductCategoryTitleFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    width: 120,
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                action: function (data) {
                                    _viewProductModal.open({ id: data.record.product.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                                _createOrEditModal.open({ id: data.record.product.id });
                            }
                        },
                        {
                            text: app.localize('History'),
                            visible: function () {
                                return entityHistoryIsEnabled();
                            },
                            action: function (data) {
                                _entityTypeHistoryModal.open({
                                    entityTypeFullName: _entityTypeFullName,
                                    entityId: data.record.product.id
                                });
                            }
						}, 
						{
                            text: app.localize('Delete'),
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteProduct(data.record.product);
                            }
                        }]
                    }
                },
					{
						targets: 1,
						 data: "product.title",
						 name: "title"   
					},
					{
						targets: 2,
						 data: "product.priority",
						 name: "priority"   
					},
					{
						targets: 3,
						 data: "product.isAvailable",
						 name: "isAvailable"  ,
						render: function (isAvailable) {
							if (isAvailable) {
								return '<div class="text-center"><i class="fa fa-check-circle kt--font-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 4,
						 data: "product.isHot",
						 name: "isHot"  ,
						render: function (isHot) {
							if (isHot) {
								return '<div class="text-center"><i class="fa fa-check-circle kt--font-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 5,
						 data: "product.isNew",
						 name: "isNew"  ,
						render: function (isNew) {
							if (isNew) {
								return '<div class="text-center"><i class="fa fa-check-circle kt--font-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 6,
						 data: "product.imageName",
						 name: "imageName"   
					},
					{
						targets: 7,
						 data: "product.priceOrigin",
						 name: "priceOrigin"   
					},
					{
						targets: 8,
						 data: "product.pricePromotion",
						 name: "pricePromotion"   
					},
					{
						targets: 9,
						 data: "product.hit",
						 name: "hit"   
					},
					{
						targets: 10,
						 data: "rProductCategoryTitle" ,
						 name: "rProductCategoryFk.title" 
					}
            ]
        });


        function getProducts() {
            dataTable.ajax.reload();
        }

        function deleteProduct(product) {
            abp.message.confirm(
                '',
                function (isConfirmed) {
                    if (isConfirmed) {
                        _productsService.delete({
                            id: product.id
                        }).done(function () {
                            getProducts(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewProductButton').click(function () {
            _createOrEditModal.open();
        });

		$('#ExportToExcelButton').click(function () {
            _productsService
                .getProductsToExcel({
				filter : $('#ProductsTableFilter').val(),
					metaTitleFilter: $('#MetaTitleFilterId').val(),
					metaDescriptionFilter: $('#MetaDescriptionFilterId').val(),
					titleFilter: $('#TitleFilterId').val(),
					descriptionFilter: $('#DescriptionFilterId').val(),
					contentFilter: $('#ContentFilterId').val(),
					minPriorityFilter: $('#MinPriorityFilterId').val(),
					maxPriorityFilter: $('#MaxPriorityFilterId').val(),
					isAvailableFilter: $('#IsAvailableFilterId').val(),
					isHotFilter: $('#IsHotFilterId').val(),
					isNewFilter: $('#IsNewFilterId').val(),
					minPriceOriginFilter: $('#MinPriceOriginFilterId').val(),
					maxPriceOriginFilter: $('#MaxPriceOriginFilterId').val(),
					minPricePromotionFilter: $('#MinPricePromotionFilterId').val(),
					maxPricePromotionFilter: $('#MaxPricePromotionFilterId').val(),
					saleManFilter: $('#SaleManFilterId').val(),
					linkReferenceFilter: $('#LinkReferenceFilterId').val(),
					isContactFilter: $('#IsContactFilterId').val(),
					rProductCategoryTitleFilter: $('#RProductCategoryTitleFilterId').val()
				})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        abp.event.on('app.createOrEditProductModalSaved', function () {
            getProducts();
        });

		$('#GetProductsButton').click(function (e) {
            e.preventDefault();
            getProducts();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getProducts();
		  }
		});

    });
})();