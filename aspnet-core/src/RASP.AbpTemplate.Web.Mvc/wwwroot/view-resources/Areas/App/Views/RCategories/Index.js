(function () {
    $(function () {

        var _$rCategoriesTable = $('#RCategoriesTable');
        var _rCategoriesService = abp.services.app.rCategories;
		var _entityTypeFullName = 'RASP.AbpTemplate.RModel.RCategory';
        
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Administration.RCategories.Create'),
            edit: abp.auth.hasPermission('Pages.Administration.RCategories.Edit'),
            'delete': abp.auth.hasPermission('Pages.Administration.RCategories.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RCategories/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/RCategories/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditRCategoryModal'
        });

		 var _viewRCategoryModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RCategories/ViewrCategoryModal',
            modalClass: 'ViewRCategoryModal'
        });

		        var _entityTypeHistoryModal = app.modals.EntityTypeHistoryModal.create();
		        function entityHistoryIsEnabled() {
            return abp.custom.EntityHistory &&
                abp.custom.EntityHistory.IsEnabled &&
                _.filter(abp.custom.EntityHistory.EnabledEntities, entityType => entityType === _entityTypeFullName).length === 1;
        }

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }

        var dataTable = _$rCategoriesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _rCategoriesService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#RCategoriesTableFilter').val(),
					metaTitleFilter: $('#MetaTitleFilterId').val(),
					metaDescriptionFilter: $('#MetaDescriptionFilterId').val(),
					titleFilter: $('#TitleFilterId').val(),
					descriptionFilter: $('#DescriptionFilterId').val(),
					contentFilter: $('#ContentFilterId').val(),
					tagFilter: $('#TagFilterId').val(),
					authorFilter: $('#AuthorFilterId').val(),
					minParentIdFilter: $('#MinParentIdFilterId').val(),
					maxParentIdFilter: $('#MaxParentIdFilterId').val(),
					isAvailableFilter: $('#IsAvailableFilterId').val(),
					isShowMenuTopFilter: $('#IsShowMenuTopFilterId').val(),
					isShowMenuBottomFilter: $('#IsShowMenuBottomFilterId').val(),
					minPriorityFilter: $('#MinPriorityFilterId').val(),
					maxPriorityFilter: $('#MaxPriorityFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    width: 120,
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                action: function (data) {
                                    _viewRCategoryModal.open({ id: data.record.rCategory.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                                _createOrEditModal.open({ id: data.record.rCategory.id });
                            }
                        },
                        {
                            text: app.localize('History'),
                            visible: function () {
                                return entityHistoryIsEnabled();
                            },
                            action: function (data) {
                                _entityTypeHistoryModal.open({
                                    entityTypeFullName: _entityTypeFullName,
                                    entityId: data.record.rCategory.id
                                });
                            }
						}, 
						{
                            text: app.localize('Delete'),
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteRCategory(data.record.rCategory);
                            }
                        }]
                    }
                },
					{
						targets: 1,
						 data: "rCategory.metaTitle",
						 name: "metaTitle"   
					},
					{
						targets: 2,
						 data: "rCategory.title",
						 name: "title"   
					},
					{
						targets: 3,
						 data: "rCategory.description",
						 name: "description"   
					},
					{
						targets: 4,
						 data: "rCategory.tag",
						 name: "tag"   
					},
					{
						targets: 5,
						 data: "rCategory.author",
						 name: "author"   
					},
					{
						targets: 6,
						 data: "rCategory.parentId",
						 name: "parentId"   
					},
					{
						targets: 7,
						 data: "rCategory.isAvailable",
						 name: "isAvailable"  ,
						render: function (isAvailable) {
							if (isAvailable) {
								return '<div class="text-center"><i class="fa fa-check-circle kt--font-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 8,
						 data: "rCategory.isShowMenuTop",
						 name: "isShowMenuTop"  ,
						render: function (isShowMenuTop) {
							if (isShowMenuTop) {
								return '<div class="text-center"><i class="fa fa-check-circle kt--font-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 9,
						 data: "rCategory.isShowMenuBottom",
						 name: "isShowMenuBottom"  ,
						render: function (isShowMenuBottom) {
							if (isShowMenuBottom) {
								return '<div class="text-center"><i class="fa fa-check-circle kt--font-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 10,
						 data: "rCategory.priority",
						 name: "priority"   
					}
            ]
        });


        function getRCategories() {
            dataTable.ajax.reload();
        }

        function deleteRCategory(rCategory) {
            abp.message.confirm(
                '',
                function (isConfirmed) {
                    if (isConfirmed) {
                        _rCategoriesService.delete({
                            id: rCategory.id
                        }).done(function () {
                            getRCategories(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewRCategoryButton').click(function () {
            _createOrEditModal.open();
        });

		$('#ExportToExcelButton').click(function () {
            _rCategoriesService
                .getRCategoriesToExcel({
				filter : $('#RCategoriesTableFilter').val(),
					metaTitleFilter: $('#MetaTitleFilterId').val(),
					metaDescriptionFilter: $('#MetaDescriptionFilterId').val(),
					titleFilter: $('#TitleFilterId').val(),
					descriptionFilter: $('#DescriptionFilterId').val(),
					contentFilter: $('#ContentFilterId').val(),
					tagFilter: $('#TagFilterId').val(),
					authorFilter: $('#AuthorFilterId').val(),
					minParentIdFilter: $('#MinParentIdFilterId').val(),
					maxParentIdFilter: $('#MaxParentIdFilterId').val(),
					isAvailableFilter: $('#IsAvailableFilterId').val(),
					isShowMenuTopFilter: $('#IsShowMenuTopFilterId').val(),
					isShowMenuBottomFilter: $('#IsShowMenuBottomFilterId').val(),
					minPriorityFilter: $('#MinPriorityFilterId').val(),
					maxPriorityFilter: $('#MaxPriorityFilterId').val()
				})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        abp.event.on('app.createOrEditRCategoryModalSaved', function () {
            getRCategories();
        });

		$('#GetRCategoriesButton').click(function (e) {
            e.preventDefault();
            getRCategories();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getRCategories();
		  }
		});

    });
})();