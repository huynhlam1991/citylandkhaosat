(function () {
    $(function () {

        var _$personAnswersTable = $('#PersonAnswersTable');
        var _personAnswersService = abp.services.app.personAnswers;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Administration.PersonAnswers.Create'),
            edit: abp.auth.hasPermission('Pages.Administration.PersonAnswers.Edit'),
            'delete': abp.auth.hasPermission('Pages.Administration.PersonAnswers.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/PersonAnswers/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/PersonAnswers/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditPersonAnswerModal'
        });

		 var _viewPersonAnswerModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/PersonAnswers/ViewpersonAnswerModal',
            modalClass: 'ViewPersonAnswerModal'
        });

		
		

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }

        var dataTable = _$personAnswersTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _personAnswersService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#PersonAnswersTableFilter').val(),
					otherTextFilter: $('#OtherTextFilterId').val(),
					surveyTitleFilter: $('#SurveyTitleFilterId').val(),
					questionQuestionTextFilter: $('#QuestionQuestionTextFilterId').val(),
					answerAnswerTextFilter: $('#AnswerAnswerTextFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    width: 120,
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                action: function (data) {
                                    _viewPersonAnswerModal.open({ id: data.record.personAnswer.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                                _createOrEditModal.open({ id: data.record.personAnswer.id });
                            }
                        }, 
						{
                            text: app.localize('Delete'),
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deletePersonAnswer(data.record.personAnswer);
                            }
                        }]
                    }
                },
					{
						targets: 1,
						 data: "personAnswer.otherText",
						 name: "otherText"   
					},
					{
						targets: 2,
						 data: "surveyTitle" ,
						 name: "surveyFk.title" 
					},
					{
						targets: 3,
						 data: "questionQuestionText" ,
						 name: "questionFk.questionText" 
					},
					{
						targets: 4,
						 data: "answerAnswerText" ,
						 name: "answerFk.answerText" 
					}
            ]
        });


        function getPersonAnswers() {
            dataTable.ajax.reload();
        }

        function deletePersonAnswer(personAnswer) {
            abp.message.confirm(
                '',
                function (isConfirmed) {
                    if (isConfirmed) {
                        _personAnswersService.delete({
                            id: personAnswer.id
                        }).done(function () {
                            getPersonAnswers(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewPersonAnswerButton').click(function () {
            _createOrEditModal.open();
        });

		$('#ExportToExcelButton').click(function () {
            _personAnswersService
                .getPersonAnswersToExcel({
				filter : $('#PersonAnswersTableFilter').val(),
					otherTextFilter: $('#OtherTextFilterId').val(),
					surveyTitleFilter: $('#SurveyTitleFilterId').val(),
					questionQuestionTextFilter: $('#QuestionQuestionTextFilterId').val(),
					answerAnswerTextFilter: $('#AnswerAnswerTextFilterId').val()
				})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        abp.event.on('app.createOrEditPersonAnswerModalSaved', function () {
            getPersonAnswers();
        });

		$('#GetPersonAnswersButton').click(function (e) {
            e.preventDefault();
            getPersonAnswers();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getPersonAnswers();
		  }
		});

    });
})();