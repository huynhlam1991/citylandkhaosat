(function () {
    $(function () {

        var _$rProductCategoriesTable = $('#RProductCategoriesTable');
        var _rProductCategoriesService = abp.services.app.rProductCategories;
		var _entityTypeFullName = 'RASP.AbpTemplate.RModel.RProductCategory';
        
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Administration.RProductCategories.Create'),
            edit: abp.auth.hasPermission('Pages.Administration.RProductCategories.Edit'),
            'delete': abp.auth.hasPermission('Pages.Administration.RProductCategories.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RProductCategories/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/RProductCategories/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditRProductCategoryModal'
        });

		 var _viewRProductCategoryModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RProductCategories/ViewrProductCategoryModal',
            modalClass: 'ViewRProductCategoryModal'
        });

		        var _entityTypeHistoryModal = app.modals.EntityTypeHistoryModal.create();
		        function entityHistoryIsEnabled() {
            return abp.custom.EntityHistory &&
                abp.custom.EntityHistory.IsEnabled &&
                _.filter(abp.custom.EntityHistory.EnabledEntities, entityType => entityType === _entityTypeFullName).length === 1;
        }

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }

        var dataTable = _$rProductCategoriesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _rProductCategoriesService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#RProductCategoriesTableFilter').val(),
					metaTitleFilter: $('#MetaTitleFilterId').val(),
					metaDescriptionFilter: $('#MetaDescriptionFilterId').val(),
					titleFilter: $('#TitleFilterId').val(),
					descriptionFilter: $('#DescriptionFilterId').val(),
					contentFilter: $('#ContentFilterId').val(),
					tagFilter: $('#TagFilterId').val(),
					minParentIdFilter: $('#MinParentIdFilterId').val(),
					maxParentIdFilter: $('#MaxParentIdFilterId').val(),
					isAvailableFilter: $('#IsAvailableFilterId').val(),
					isShowMenuTopFilter: $('#IsShowMenuTopFilterId').val(),
					isShowMenuBottomFilter: $('#IsShowMenuBottomFilterId').val(),
					minPriorityFilter: $('#MinPriorityFilterId').val(),
					maxPriorityFilter: $('#MaxPriorityFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    width: 120,
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                action: function (data) {
                                    _viewRProductCategoryModal.open({ id: data.record.rProductCategory.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                                _createOrEditModal.open({ id: data.record.rProductCategory.id });
                            }
                        },
                        {
                            text: app.localize('History'),
                            visible: function () {
                                return entityHistoryIsEnabled();
                            },
                            action: function (data) {
                                _entityTypeHistoryModal.open({
                                    entityTypeFullName: _entityTypeFullName,
                                    entityId: data.record.rProductCategory.id
                                });
                            }
						}, 
						{
                            text: app.localize('Delete'),
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteRProductCategory(data.record.rProductCategory);
                            }
                        }]
                    }
                },
					{
						targets: 1,
						 data: "rProductCategory.metaTitle",
						 name: "metaTitle"   
					},
					{
						targets: 2,
						 data: "rProductCategory.metaDescription",
						 name: "metaDescription"   
					},
					{
						targets: 3,
						 data: "rProductCategory.title",
						 name: "title"   
					},
					{
						targets: 4,
						 data: "rProductCategory.description",
						 name: "description"   
					},
					{
						targets: 5,
						 data: "rProductCategory.isAvailable",
						 name: "isAvailable"  ,
						render: function (isAvailable) {
							if (isAvailable) {
								return '<div class="text-center"><i class="fa fa-check-circle kt--font-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 6,
						 data: "rProductCategory.isShowMenuTop",
						 name: "isShowMenuTop"  ,
						render: function (isShowMenuTop) {
							if (isShowMenuTop) {
								return '<div class="text-center"><i class="fa fa-check-circle kt--font-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 7,
						 data: "rProductCategory.isShowMenuBottom",
						 name: "isShowMenuBottom"  ,
						render: function (isShowMenuBottom) {
							if (isShowMenuBottom) {
								return '<div class="text-center"><i class="fa fa-check-circle kt--font-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 8,
						 data: "rProductCategory.priority",
						 name: "priority"   
					}
            ]
        });


        function getRProductCategories() {
            dataTable.ajax.reload();
        }

        function deleteRProductCategory(rProductCategory) {
            abp.message.confirm(
                '',
                function (isConfirmed) {
                    if (isConfirmed) {
                        _rProductCategoriesService.delete({
                            id: rProductCategory.id
                        }).done(function () {
                            getRProductCategories(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewRProductCategoryButton').click(function () {
            _createOrEditModal.open();
        });

		$('#ExportToExcelButton').click(function () {
            _rProductCategoriesService
                .getRProductCategoriesToExcel({
				filter : $('#RProductCategoriesTableFilter').val(),
					metaTitleFilter: $('#MetaTitleFilterId').val(),
					metaDescriptionFilter: $('#MetaDescriptionFilterId').val(),
					titleFilter: $('#TitleFilterId').val(),
					descriptionFilter: $('#DescriptionFilterId').val(),
					contentFilter: $('#ContentFilterId').val(),
					tagFilter: $('#TagFilterId').val(),
					minParentIdFilter: $('#MinParentIdFilterId').val(),
					maxParentIdFilter: $('#MaxParentIdFilterId').val(),
					isAvailableFilter: $('#IsAvailableFilterId').val(),
					isShowMenuTopFilter: $('#IsShowMenuTopFilterId').val(),
					isShowMenuBottomFilter: $('#IsShowMenuBottomFilterId').val(),
					minPriorityFilter: $('#MinPriorityFilterId').val(),
					maxPriorityFilter: $('#MaxPriorityFilterId').val()
				})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        abp.event.on('app.createOrEditRProductCategoryModalSaved', function () {
            getRProductCategories();
        });

		$('#GetRProductCategoriesButton').click(function (e) {
            e.preventDefault();
            getRProductCategories();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getRProductCategories();
		  }
		});

    });
})();