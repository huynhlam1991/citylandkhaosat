(function ($) {
    app.modals.AnswerLookupTableModal = function () {

        var _modalManager;

        var _personAnswersService = abp.services.app.personAnswers;
        var _$answerTable = $('#AnswerTable');

        this.init = function (modalManager) {
            _modalManager = modalManager;
        };


        var dataTable = _$answerTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _personAnswersService.getAllAnswerForLookupTable,
                inputFilter: function () {
                    return {
                        filter: $('#AnswerTableFilter').val()
                    };
                }
            },
            columnDefs: [
                {
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: "<div class=\"text-center\"><input id='selectbtn' class='btn btn-success' type='button' width='25px' value='" + app.localize('Select') + "' /></div>"
                },
                {
                    autoWidth: false,
                    orderable: false,
                    targets: 1,
                    data: "displayName"
                }
            ]
        });

        $('#AnswerTable tbody').on('click', '[id*=selectbtn]', function () {
            var data = dataTable.row($(this).parents('tr')).data();
            _modalManager.setResult(data);
            _modalManager.close();
        });

        function getAnswer() {
            dataTable.ajax.reload();
        }

        $('#GetAnswerButton').click(function (e) {
            e.preventDefault();
            getAnswer();
        });

        $('#SelectButton').click(function (e) {
            e.preventDefault();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getAnswer();
            }
        });

    };
})(jQuery);

