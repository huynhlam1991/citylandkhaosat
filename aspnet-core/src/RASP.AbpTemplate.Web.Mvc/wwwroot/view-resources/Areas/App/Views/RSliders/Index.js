(function () {
    $(function () {

        var _$rSlidersTable = $('#RSlidersTable');
        var _rSlidersService = abp.services.app.rSliders;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Administration.RSliders.Create'),
            edit: abp.auth.hasPermission('Pages.Administration.RSliders.Edit'),
            'delete': abp.auth.hasPermission('Pages.Administration.RSliders.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RSliders/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/RSliders/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditRSliderModal'
        });

		 var _viewRSliderModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RSliders/ViewrSliderModal',
            modalClass: 'ViewRSliderModal'
        });

		
		

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }

        var dataTable = _$rSlidersTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _rSlidersService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#RSlidersTableFilter').val(),
					titleFilter: $('#TitleFilterId').val(),
					fileNameFilter: $('#FileNameFilterId').val(),
					linkFilter: $('#LinkFilterId').val(),
					descriptionFilter: $('#DescriptionFilterId').val(),
					isAvailableFilter: $('#IsAvailableFilterId').val(),
					positionCodeFilter: $('#PositionCodeFilterId').val(),
					minPriorityFilter: $('#MinPriorityFilterId').val(),
					maxPriorityFilter: $('#MaxPriorityFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    width: 120,
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                action: function (data) {
                                    _viewRSliderModal.open({ id: data.record.rSlider.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                                _createOrEditModal.open({ id: data.record.rSlider.id });
                            }
                        }, 
						{
                            text: app.localize('Delete'),
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteRSlider(data.record.rSlider);
                            }
                        }]
                    }
                },
					{
						targets: 1,
						 data: "rSlider.title",
						 name: "title"   
					},
					{
						targets: 2,
						 data: "rSlider.fileName",
						 name: "fileName"   
					},
					{
						targets: 3,
						 data: "rSlider.description",
						 name: "description"   
					},
					{
						targets: 4,
						 data: "rSlider.isAvailable",
						 name: "isAvailable"  ,
						render: function (isAvailable) {
							if (isAvailable) {
								return '<div class="text-center"><i class="fa fa-check-circle kt--font-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 5,
						 data: "rSlider.positionCode",
						 name: "positionCode"   
					},
					{
						targets: 6,
						 data: "rSlider.priority",
						 name: "priority"   
					}
            ]
        });


        function getRSliders() {
            dataTable.ajax.reload();
        }

        function deleteRSlider(rSlider) {
            abp.message.confirm(
                '',
                function (isConfirmed) {
                    if (isConfirmed) {
                        _rSlidersService.delete({
                            id: rSlider.id
                        }).done(function () {
                            getRSliders(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewRSliderButton').click(function () {
            _createOrEditModal.open();
        });

		$('#ExportToExcelButton').click(function () {
            _rSlidersService
                .getRSlidersToExcel({
				filter : $('#RSlidersTableFilter').val(),
					titleFilter: $('#TitleFilterId').val(),
					fileNameFilter: $('#FileNameFilterId').val(),
					linkFilter: $('#LinkFilterId').val(),
					descriptionFilter: $('#DescriptionFilterId').val(),
					isAvailableFilter: $('#IsAvailableFilterId').val(),
					positionCodeFilter: $('#PositionCodeFilterId').val(),
					minPriorityFilter: $('#MinPriorityFilterId').val(),
					maxPriorityFilter: $('#MaxPriorityFilterId').val()
				})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        abp.event.on('app.createOrEditRSliderModalSaved', function () {
            getRSliders();
        });

		$('#GetRSlidersButton').click(function (e) {
            e.preventDefault();
            getRSliders();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getRSliders();
		  }
		});

    });
})();