(function ($) {
    app.modals.RCategoryLookupTableModal = function () {

        var _modalManager;

        var _articlesService = abp.services.app.articles;
        var _$rCategoryTable = $('#RCategoryTable');

        this.init = function (modalManager) {
            _modalManager = modalManager;
        };


        var dataTable = _$rCategoryTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _articlesService.getAllRCategoryForLookupTable,
                inputFilter: function () {
                    return {
                        filter: $('#RCategoryTableFilter').val()
                    };
                }
            },
            columnDefs: [
                {
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: "<div class=\"text-center\"><input id='selectbtn' class='btn btn-success' type='button' width='25px' value='" + app.localize('Select') + "' /></div>"
                },
                {
                    autoWidth: false,
                    orderable: false,
                    targets: 1,
                    data: "displayName"
                }
            ]
        });

        $('#RCategoryTable tbody').on('click', '[id*=selectbtn]', function () {
            var data = dataTable.row($(this).parents('tr')).data();
            _modalManager.setResult(data);
            _modalManager.close();
        });

        function getRCategory() {
            dataTable.ajax.reload();
        }

        $('#GetRCategoryButton').click(function (e) {
            e.preventDefault();
            getRCategory();
        });

        $('#SelectButton').click(function (e) {
            e.preventDefault();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getRCategory();
            }
        });

    };
})(jQuery);

