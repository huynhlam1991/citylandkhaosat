﻿(function () {
    $(function () {

        var _$surveyResultsTable = $('#SurveyResultsTable');
        var _surveyResultsService = abp.services.app.surveyResults;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Administration.SurveyResults.Create'),
            edit: abp.auth.hasPermission('Pages.Administration.SurveyResults.Edit'),
            'delete': abp.auth.hasPermission('Pages.Administration.SurveyResults.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/SurveyResults/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/SurveyResults/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditSurveyResultModal'
        });

		 var _viewSurveyResultModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/SurveyResults/ViewsurveyResultModal',
            modalClass: 'ViewSurveyResultModal'
        });

		
		

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }

        var dataTable = _$surveyResultsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _surveyResultsService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#SurveyResultsTableFilter').val(),
					textFilter: $('#TextFilterId').val(),
					jsonFilter: $('#JsonFilterId').val(),
					surveyTitleFilter: $('#SurveyTitleFilterId').val(),
					userNameFilter: $('#UserNameFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    width: 120,
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                action: function (data) {
                                    _viewSurveyResultModal.open({ id: data.record.surveyResult.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                                _createOrEditModal.open({ id: data.record.surveyResult.id });
                            }
                        }, 
						{
                            text: app.localize('Delete'),
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteSurveyResult(data.record.surveyResult);
                            }
                            },
                            {
                                text: 'Xem kết quả',
                                visible: function () {
                                    return true; //_permissions.delete;
                                },
                                action: function (data) {
                                    debugger;
                                    window.open('/App/ServiceSurvey/Result?id=' + data.record.surveyResult.surveyId + '&name=' + data.record.surveyTitle + '&email=' + data.record.surveyResult.text + '', '_blank');
                                }
                            }]
                    }
                },
					{
						targets: 1,
						 data: "surveyResult.text",
						 name: "text"   
					},
					{
                        targets: 2,
                        visible: false,
						 data: "surveyResult.json",
						 name: "json"   
					},
					{
						targets: 3,
						 data: "surveyTitle" ,
                        name: "surveyFk.title"
					},
					{
                        targets: 4,
                        visible: false,
						 data: "userName" ,
						 name: "userFk.name" 
					}
            ]
        });


        function getSurveyResults() {
            dataTable.ajax.reload();
        }

        function deleteSurveyResult(surveyResult) {
            abp.message.confirm(
                '',
                function (isConfirmed) {
                    if (isConfirmed) {
                        _surveyResultsService.delete({
                            id: surveyResult.id
                        }).done(function () {
                            getSurveyResults(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewSurveyResultButton').click(function () {
            _createOrEditModal.open();
        });

		$('#ExportToExcelButton').click(function () {
            _surveyResultsService
                .getSurveyResultsToExcel({
				filter : $('#SurveyResultsTableFilter').val(),
					textFilter: $('#TextFilterId').val(),
					jsonFilter: $('#JsonFilterId').val(),
					surveyTitleFilter: $('#SurveyTitleFilterId').val(),
					userNameFilter: $('#UserNameFilterId').val()
				})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        abp.event.on('app.createOrEditSurveyResultModalSaved', function () {
            getSurveyResults();
        });

		$('#GetSurveyResultsButton').click(function (e) {
            e.preventDefault();
            getSurveyResults();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getSurveyResults();
		  }
		});

    });
})();