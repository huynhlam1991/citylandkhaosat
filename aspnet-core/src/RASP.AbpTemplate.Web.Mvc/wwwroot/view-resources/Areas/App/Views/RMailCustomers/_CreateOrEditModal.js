(function ($) {
    app.modals.CreateOrEditRMailCustomerModal = function () {

        var _rMailCustomersService = abp.services.app.rMailCustomers;

        var _modalManager;
        var _$rMailCustomerInformationForm = null;

		

        this.init = function (modalManager) {
            _modalManager = modalManager;

			var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });

            _$rMailCustomerInformationForm = _modalManager.getModal().find('form[name=RMailCustomerInformationsForm]');
            _$rMailCustomerInformationForm.validate();
        };

		  

        this.save = function () {
            if (!_$rMailCustomerInformationForm.valid()) {
                return;
            }

            var rMailCustomer = _$rMailCustomerInformationForm.serializeFormToObject();
			
			 _modalManager.setBusy(true);
			 _rMailCustomersService.createOrEdit(
				rMailCustomer
			 ).done(function () {
               abp.notify.info(app.localize('SavedSuccessfully'));
               _modalManager.close();
               abp.event.trigger('app.createOrEditRMailCustomerModalSaved');
			 }).always(function () {
               _modalManager.setBusy(false);
			});
        };
    };
})(jQuery);