(function ($) {
    app.modals.CreateOrEditSurveyResultModal = function () {

        var _surveyResultsService = abp.services.app.surveyResults;

        var _modalManager;
        var _$surveyResultInformationForm = null;

		        var _SurveyResultsurveyLookupTableModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/SurveyResults/SurveyLookupTableModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/SurveyResults/_SurveyResultSurveyLookupTableModal.js',
            modalClass: 'SurveyLookupTableModal'
        });        var _SurveyResultuserLookupTableModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/SurveyResults/UserLookupTableModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/SurveyResults/_SurveyResultUserLookupTableModal.js',
            modalClass: 'UserLookupTableModal'
        });

        this.init = function (modalManager) {
            _modalManager = modalManager;

			var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });

            _$surveyResultInformationForm = _modalManager.getModal().find('form[name=SurveyResultInformationsForm]');
            _$surveyResultInformationForm.validate();
        };

		          $('#OpenSurveyLookupTableButton').click(function () {

            var surveyResult = _$surveyResultInformationForm.serializeFormToObject();

            _SurveyResultsurveyLookupTableModal.open({ id: surveyResult.surveyId, displayName: surveyResult.surveyTitle }, function (data) {
                _$surveyResultInformationForm.find('input[name=surveyTitle]').val(data.displayName); 
                _$surveyResultInformationForm.find('input[name=surveyId]').val(data.id); 
            });
        });
		
		$('#ClearSurveyTitleButton').click(function () {
                _$surveyResultInformationForm.find('input[name=surveyTitle]').val(''); 
                _$surveyResultInformationForm.find('input[name=surveyId]').val(''); 
        });
		
        $('#OpenUserLookupTableButton').click(function () {

            var surveyResult = _$surveyResultInformationForm.serializeFormToObject();

            _SurveyResultuserLookupTableModal.open({ id: surveyResult.userId, displayName: surveyResult.userName }, function (data) {
                _$surveyResultInformationForm.find('input[name=userName]').val(data.displayName); 
                _$surveyResultInformationForm.find('input[name=userId]').val(data.id); 
            });
        });
		
		$('#ClearUserNameButton').click(function () {
                _$surveyResultInformationForm.find('input[name=userName]').val(''); 
                _$surveyResultInformationForm.find('input[name=userId]').val(''); 
        });
		


        this.save = function () {
            if (!_$surveyResultInformationForm.valid()) {
                return;
            }
            if ($('#SurveyResult_SurveyId').prop('required') && $('#SurveyResult_SurveyId').val() == '') {
                abp.message.error(app.localize('{0}IsRequired', app.localize('Survey')));
                return;
            }
            if ($('#SurveyResult_UserId').prop('required') && $('#SurveyResult_UserId').val() == '') {
                abp.message.error(app.localize('{0}IsRequired', app.localize('User')));
                return;
            }

            var surveyResult = _$surveyResultInformationForm.serializeFormToObject();
			
			 _modalManager.setBusy(true);
			 _surveyResultsService.createOrEdit(
				surveyResult
			 ).done(function () {
               abp.notify.info(app.localize('SavedSuccessfully'));
               _modalManager.close();
               abp.event.trigger('app.createOrEditSurveyResultModalSaved');
			 }).always(function () {
               _modalManager.setBusy(false);
			});
        };
    };
})(jQuery);