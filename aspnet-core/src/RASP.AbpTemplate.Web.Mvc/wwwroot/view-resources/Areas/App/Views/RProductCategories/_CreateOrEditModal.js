(function ($) {
    app.modals.CreateOrEditRProductCategoryModal = function () {

        var _rProductCategoriesService = abp.services.app.rProductCategories;

        var _modalManager;
        var _$rProductCategoryInformationForm = null;

		

        this.init = function (modalManager) {
            _modalManager = modalManager;

			var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });

            _$rProductCategoryInformationForm = _modalManager.getModal().find('form[name=RProductCategoryInformationsForm]');
            _$rProductCategoryInformationForm.validate();
        };

		  

        this.save = function () {
            if (!_$rProductCategoryInformationForm.valid()) {
                return;
            }

            var rProductCategory = _$rProductCategoryInformationForm.serializeFormToObject();
			
			 _modalManager.setBusy(true);
			 _rProductCategoriesService.createOrEdit(
				rProductCategory
			 ).done(function () {
               abp.notify.info(app.localize('SavedSuccessfully'));
               _modalManager.close();
               abp.event.trigger('app.createOrEditRProductCategoryModalSaved');
			 }).always(function () {
               _modalManager.setBusy(false);
			});
        };
    };
})(jQuery);