(function ($) {
    app.modals.CreateOrEditSurveyModal = function () {

        var _surveiesService = abp.services.app.surveies;

        var _modalManager;
        var _$surveyInformationForm = null;

		

        this.init = function (modalManager) {
            _modalManager = modalManager;

			var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });

            _$surveyInformationForm = _modalManager.getModal().find('form[name=SurveyInformationsForm]');
            _$surveyInformationForm.validate();
        };

		  

        this.save = function () {
            if (!_$surveyInformationForm.valid()) {
                return;
            }

            var survey = _$surveyInformationForm.serializeFormToObject();
			
			 _modalManager.setBusy(true);
			 _surveiesService.createOrEdit(
				survey
			 ).done(function () {
               abp.notify.info(app.localize('SavedSuccessfully'));
               _modalManager.close();
               abp.event.trigger('app.createOrEditSurveyModalSaved');
			 }).always(function () {
               _modalManager.setBusy(false);
			});
        };
    };
})(jQuery);