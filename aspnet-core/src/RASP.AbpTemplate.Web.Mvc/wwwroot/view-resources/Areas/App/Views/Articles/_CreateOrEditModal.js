(function ($) {
    app.modals.CreateOrEditArticleModal = function () {

        var _articlesService = abp.services.app.articles;

        var _modalManager;
        var _$articleInformationForm = null;

		        var _ArticlerCategoryLookupTableModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Articles/RCategoryLookupTableModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Articles/_ArticleRCategoryLookupTableModal.js',
            modalClass: 'RCategoryLookupTableModal'
        });

        this.init = function (modalManager) {
            _modalManager = modalManager;

			var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });

            _$articleInformationForm = _modalManager.getModal().find('form[name=ArticleInformationsForm]');
            _$articleInformationForm.validate();
        };

		          $('#OpenRCategoryLookupTableButton').click(function () {

            var article = _$articleInformationForm.serializeFormToObject();

            _ArticlerCategoryLookupTableModal.open({ id: article.rCategoryId, displayName: article.rCategoryTitle }, function (data) {
                _$articleInformationForm.find('input[name=rCategoryTitle]').val(data.displayName); 
                _$articleInformationForm.find('input[name=rCategoryId]').val(data.id); 
            });
        });
		
		$('#ClearRCategoryTitleButton').click(function () {
                _$articleInformationForm.find('input[name=rCategoryTitle]').val(''); 
                _$articleInformationForm.find('input[name=rCategoryId]').val(''); 
        });
		


        this.save = function () {
            if (!_$articleInformationForm.valid()) {
                return;
            }
            if ($('#Article_RCategoryId').prop('required') && $('#Article_RCategoryId').val() == '') {
                abp.message.error(app.localize('{0}IsRequired', app.localize('RCategory')));
                return;
            }

            var article = _$articleInformationForm.serializeFormToObject();
			
			 _modalManager.setBusy(true);
			 _articlesService.createOrEdit(
				article
			 ).done(function () {
               abp.notify.info(app.localize('SavedSuccessfully'));
               _modalManager.close();
               abp.event.trigger('app.createOrEditArticleModalSaved');
			 }).always(function () {
               _modalManager.setBusy(false);
			});
        };
    };
})(jQuery);