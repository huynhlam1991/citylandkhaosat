(function ($) {
    app.modals.CreateOrEditPersonAnswerModal = function () {

        var _personAnswersService = abp.services.app.personAnswers;

        var _modalManager;
        var _$personAnswerInformationForm = null;

		        var _PersonAnswersurveyLookupTableModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/PersonAnswers/SurveyLookupTableModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/PersonAnswers/_PersonAnswerSurveyLookupTableModal.js',
            modalClass: 'SurveyLookupTableModal'
        });        var _PersonAnswerquestionLookupTableModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/PersonAnswers/QuestionLookupTableModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/PersonAnswers/_PersonAnswerQuestionLookupTableModal.js',
            modalClass: 'QuestionLookupTableModal'
        });        var _PersonAnsweranswerLookupTableModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/PersonAnswers/AnswerLookupTableModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/PersonAnswers/_PersonAnswerAnswerLookupTableModal.js',
            modalClass: 'AnswerLookupTableModal'
        });

        this.init = function (modalManager) {
            _modalManager = modalManager;

			var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });

            _$personAnswerInformationForm = _modalManager.getModal().find('form[name=PersonAnswerInformationsForm]');
            _$personAnswerInformationForm.validate();
        };

		          $('#OpenSurveyLookupTableButton').click(function () {

            var personAnswer = _$personAnswerInformationForm.serializeFormToObject();

            _PersonAnswersurveyLookupTableModal.open({ id: personAnswer.surveyId, displayName: personAnswer.surveyTitle }, function (data) {
                _$personAnswerInformationForm.find('input[name=surveyTitle]').val(data.displayName); 
                _$personAnswerInformationForm.find('input[name=surveyId]').val(data.id); 
            });
        });
		
		$('#ClearSurveyTitleButton').click(function () {
                _$personAnswerInformationForm.find('input[name=surveyTitle]').val(''); 
                _$personAnswerInformationForm.find('input[name=surveyId]').val(''); 
        });
		
        $('#OpenQuestionLookupTableButton').click(function () {

            var personAnswer = _$personAnswerInformationForm.serializeFormToObject();

            _PersonAnswerquestionLookupTableModal.open({ id: personAnswer.questionId, displayName: personAnswer.questionQuestionText }, function (data) {
                _$personAnswerInformationForm.find('input[name=questionQuestionText]').val(data.displayName); 
                _$personAnswerInformationForm.find('input[name=questionId]').val(data.id); 
            });
        });
		
		$('#ClearQuestionQuestionTextButton').click(function () {
                _$personAnswerInformationForm.find('input[name=questionQuestionText]').val(''); 
                _$personAnswerInformationForm.find('input[name=questionId]').val(''); 
        });
		
        $('#OpenAnswerLookupTableButton').click(function () {

            var personAnswer = _$personAnswerInformationForm.serializeFormToObject();

            _PersonAnsweranswerLookupTableModal.open({ id: personAnswer.answerId, displayName: personAnswer.answerAnswerText }, function (data) {
                _$personAnswerInformationForm.find('input[name=answerAnswerText]').val(data.displayName); 
                _$personAnswerInformationForm.find('input[name=answerId]').val(data.id); 
            });
        });
		
		$('#ClearAnswerAnswerTextButton').click(function () {
                _$personAnswerInformationForm.find('input[name=answerAnswerText]').val(''); 
                _$personAnswerInformationForm.find('input[name=answerId]').val(''); 
        });
		


        this.save = function () {
            if (!_$personAnswerInformationForm.valid()) {
                return;
            }
            if ($('#PersonAnswer_SurveyId').prop('required') && $('#PersonAnswer_SurveyId').val() == '') {
                abp.message.error(app.localize('{0}IsRequired', app.localize('Survey')));
                return;
            }
            if ($('#PersonAnswer_QuestionId').prop('required') && $('#PersonAnswer_QuestionId').val() == '') {
                abp.message.error(app.localize('{0}IsRequired', app.localize('Question')));
                return;
            }
            if ($('#PersonAnswer_AnswerId').prop('required') && $('#PersonAnswer_AnswerId').val() == '') {
                abp.message.error(app.localize('{0}IsRequired', app.localize('Answer')));
                return;
            }

            var personAnswer = _$personAnswerInformationForm.serializeFormToObject();
			
			 _modalManager.setBusy(true);
			 _personAnswersService.createOrEdit(
				personAnswer
			 ).done(function () {
               abp.notify.info(app.localize('SavedSuccessfully'));
               _modalManager.close();
               abp.event.trigger('app.createOrEditPersonAnswerModalSaved');
			 }).always(function () {
               _modalManager.setBusy(false);
			});
        };
    };
})(jQuery);