(function () {
    $(function () {

        var _$articlesTable = $('#ArticlesTable');
        var _articlesService = abp.services.app.articles;
		var _entityTypeFullName = 'RASP.AbpTemplate.RModel.Article';
        
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Administration.Articles.Create'),
            edit: abp.auth.hasPermission('Pages.Administration.Articles.Edit'),
            'delete': abp.auth.hasPermission('Pages.Administration.Articles.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Articles/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Articles/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditArticleModal'
        });

		 var _viewArticleModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Articles/ViewarticleModal',
            modalClass: 'ViewArticleModal'
        });

		        var _entityTypeHistoryModal = app.modals.EntityTypeHistoryModal.create();
		        function entityHistoryIsEnabled() {
            return abp.custom.EntityHistory &&
                abp.custom.EntityHistory.IsEnabled &&
                _.filter(abp.custom.EntityHistory.EnabledEntities, entityType => entityType === _entityTypeFullName).length === 1;
        }

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }

        var dataTable = _$articlesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _articlesService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#ArticlesTableFilter').val(),
					metaTitleFilter: $('#MetaTitleFilterId').val(),
					metaDescriptionFilter: $('#MetaDescriptionFilterId').val(),
					tagFilter: $('#TagFilterId').val(),
					titleFilter: $('#TitleFilterId').val(),
					descriptionFilter: $('#DescriptionFilterId').val(),
					contentFilter: $('#ContentFilterId').val(),
					authorFilter: $('#AuthorFilterId').val(),
					isAvailableFilter: $('#IsAvailableFilterId').val(),
					isHotFilter: $('#IsHotFilterId').val(),
					isNewFilter: $('#IsNewFilterId').val(),
					minPriorityFilter: $('#MinPriorityFilterId').val(),
					maxPriorityFilter: $('#MaxPriorityFilterId').val(),
					minHitFilter: $('#MinHitFilterId').val(),
					maxHitFilter: $('#MaxHitFilterId').val(),
					rCategoryTitleFilter: $('#RCategoryTitleFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    width: 120,
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                action: function (data) {
                                    _viewArticleModal.open({ id: data.record.article.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                                _createOrEditModal.open({ id: data.record.article.id });
                            }
                        },
                        {
                            text: app.localize('History'),
                            visible: function () {
                                return entityHistoryIsEnabled();
                            },
                            action: function (data) {
                                _entityTypeHistoryModal.open({
                                    entityTypeFullName: _entityTypeFullName,
                                    entityId: data.record.article.id
                                });
                            }
						}, 
						{
                            text: app.localize('Delete'),
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteArticle(data.record.article);
                            }
                        }]
                    }
                },
					{
						targets: 1,
						 data: "article.metaTitle",
						 name: "metaTitle"   
					},
					{
						targets: 2,
						 data: "article.metaDescription",
						 name: "metaDescription"   
					},
					{
						targets: 3,
						 data: "article.imageName",
						 name: "imageName"   
					},
					{
						targets: 4,
						 data: "article.title",
						 name: "title"   
					},
					{
						targets: 5,
						 data: "article.author",
						 name: "author"   
					},
					{
						targets: 6,
						 data: "article.isAvailable",
						 name: "isAvailable"  ,
						render: function (isAvailable) {
							if (isAvailable) {
								return '<div class="text-center"><i class="fa fa-check-circle kt--font-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 7,
						 data: "article.isHot",
						 name: "isHot"   
					},
					{
						targets: 8,
						 data: "article.isNew",
						 name: "isNew"   
					},
					{
						targets: 9,
						 data: "article.priority",
						 name: "priority"   
					},
					{
						targets: 10,
						 data: "article.hit",
						 name: "hit"   
					},
					{
						targets: 11,
						 data: "rCategoryTitle" ,
						 name: "rCategoryFk.title" 
					}
            ]
        });


        function getArticles() {
            dataTable.ajax.reload();
        }

        function deleteArticle(article) {
            abp.message.confirm(
                '',
                function (isConfirmed) {
                    if (isConfirmed) {
                        _articlesService.delete({
                            id: article.id
                        }).done(function () {
                            getArticles(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewArticleButton').click(function () {
            _createOrEditModal.open();
        });

		$('#ExportToExcelButton').click(function () {
            _articlesService
                .getArticlesToExcel({
				filter : $('#ArticlesTableFilter').val(),
					metaTitleFilter: $('#MetaTitleFilterId').val(),
					metaDescriptionFilter: $('#MetaDescriptionFilterId').val(),
					tagFilter: $('#TagFilterId').val(),
					titleFilter: $('#TitleFilterId').val(),
					descriptionFilter: $('#DescriptionFilterId').val(),
					contentFilter: $('#ContentFilterId').val(),
					authorFilter: $('#AuthorFilterId').val(),
					isAvailableFilter: $('#IsAvailableFilterId').val(),
					isHotFilter: $('#IsHotFilterId').val(),
					isNewFilter: $('#IsNewFilterId').val(),
					minPriorityFilter: $('#MinPriorityFilterId').val(),
					maxPriorityFilter: $('#MaxPriorityFilterId').val(),
					minHitFilter: $('#MinHitFilterId').val(),
					maxHitFilter: $('#MaxHitFilterId').val(),
					rCategoryTitleFilter: $('#RCategoryTitleFilterId').val()
				})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        abp.event.on('app.createOrEditArticleModalSaved', function () {
            getArticles();
        });

		$('#GetArticlesButton').click(function (e) {
            e.preventDefault();
            getArticles();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getArticles();
		  }
		});

    });
})();