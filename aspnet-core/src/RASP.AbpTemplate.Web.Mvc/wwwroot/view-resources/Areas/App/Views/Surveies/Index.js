﻿(function () {
    $(function () {

        var _$surveiesTable = $('#SurveiesTable');
        var _surveiesService = abp.services.app.surveies;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Administration.Surveies.Create'),
            edit: abp.auth.hasPermission('Pages.Administration.Surveies.Edit'),
            'delete': abp.auth.hasPermission('Pages.Administration.Surveies.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Surveies/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Surveies/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditSurveyModal'
        });

		 var _viewSurveyModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Surveies/ViewsurveyModal',
            modalClass: 'ViewSurveyModal'
        });

		
		

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }

        var dataTable = _$surveiesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _surveiesService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#SurveiesTableFilter').val(),
					titleFilter: $('#TitleFilterId').val(),
					descriptionFilter: $('#DescriptionFilterId').val(),
					minStartDateFilter:  getDateFilter($('#MinStartDateFilterId')),
					maxStartDateFilter:  getDateFilter($('#MaxStartDateFilterId')),
					minEndDateFilter:  getDateFilter($('#MinEndDateFilterId')),
					maxEndDateFilter:  getDateFilter($('#MaxEndDateFilterId')),
					isAvailableFilter: $('#IsAvailableFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    width: 120,
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						//{
      //                          text: app.localize('View'),
      //                          action: function (data) {
      //                              _viewSurveyModal.open({ id: data.record.survey.id });
      //                          }
      //                  },
						{
                            text: app.localize('Edit'),
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                                _createOrEditModal.open({ id: data.record.survey.id });
                            }
                        }, 
						{
                            text: app.localize('Delete'),
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteSurvey(data.record.survey);
                            }
                            }, 
						{
                            text: app.localize('Run'),
                            visible: function () {
                                return true; //_permissions.delete;
                            },
                            action: function (data) {
                                window.open('/App/ServiceSurvey/Survey?id=' + data.record.survey.id + '&name=' + data.record.survey.title+'', '_blank');
                            }
                            },
                            {
                                text: "Xem thống kê",
                                visible: function () {
                                    return true; //_permissions.delete;
                                },
                                action: function (data) {
                                    window.open('/App/ServiceSurvey/ResultAnalytics?id=' + data.record.survey.id + '&name=' + data.record.survey.title + '', '_blank');
                                }
                            },
                            {
                                text: "Xuất excel",
                                visible: function () {
                                    return true; //_permissions.delete;
                                },
                                action: function (data) {
                                    _surveiesService
                                        .getSurveiesToExcel({
                                            filter: $('#SurveiesTableFilter').val(),
                                            titleFilter: $('#TitleFilterId').val(),
                                            descriptionFilter: $('#DescriptionFilterId').val(),
                                            minStartDateFilter: getDateFilter($('#MinStartDateFilterId')),
                                            maxStartDateFilter: getDateFilter($('#MaxStartDateFilterId')),
                                            minEndDateFilter: getDateFilter($('#MinEndDateFilterId')),
                                            maxEndDateFilter: getDateFilter($('#MaxEndDateFilterId')),
                                            isAvailableFilter: $('#IsAvailableFilterId').val(),
                                            surveyId: data.record.survey.id
                                        })
                                        .done(function (result) {
                                            app.downloadTempFile(result);
                                        });
                                    //window.open('/App/Surveies/DownLoadFile?surveyId=' + data.record.survey.id + '', '_blank');
                                }
                            },
                            {
                                text: "Xuất excel chi tiết",
                                visible: function () {
                                    return true; //_permissions.delete;
                                },
                                action: function (data) {
                                    _surveiesService
                                        .getSurveiesToExcelDetail({
                                            filter: $('#SurveiesTableFilter').val(),
                                            titleFilter: $('#TitleFilterId').val(),
                                            descriptionFilter: $('#DescriptionFilterId').val(),
                                            minStartDateFilter: getDateFilter($('#MinStartDateFilterId')),
                                            maxStartDateFilter: getDateFilter($('#MaxStartDateFilterId')),
                                            minEndDateFilter: getDateFilter($('#MinEndDateFilterId')),
                                            maxEndDateFilter: getDateFilter($('#MaxEndDateFilterId')),
                                            isAvailableFilter: $('#IsAvailableFilterId').val(),
                                            surveyId: data.record.survey.id
                                        })
                                        .done(function (result) {
                                            app.downloadTempFile(result);
                                        });
                                    //window.open('/App/Surveies/DownLoadFile?surveyId=' + data.record.survey.id + '', '_blank');
                                }
                            },
                            {
                                text: "Gửi mail",
                                visible: function () {
                                    return true; //_permissions.delete;
                                },
                                action: function (data) {
                                    window.open('/App/RMailCustomers/Index?id=' + data.record.survey.id + '&name=' + data.record.survey.title + '', '_blank');
                                }
                            }]
                    }
                },
					{
						targets: 1,
						 data: "survey",
                        name: "title",
                        render: function (survey) {
                            return '<a href="/App/ServiceSurvey/SurveyCreator?id=' + survey.id + '&name=' + survey.title + '">' + survey.title + '</a>';   
                        }
                            
					},
					{
						targets: 2,
						 data: "survey.description",
						 name: "description"   
					},
					{
						targets: 3,
						 data: "survey.startDate",
						 name: "startDate" ,
					render: function (startDate) {
						if (startDate) {
							return moment(startDate).format('L');
						}
						return "";
					}
			  
					},
					{
						targets: 4,
						 data: "survey.endDate",
						 name: "endDate" ,
					render: function (endDate) {
						if (endDate) {
							return moment(endDate).format('L');
						}
						return "";
					}
			  
					},
					{
						targets: 5,
						 data: "survey.isAvailable",
						 name: "isAvailable"  ,
						render: function (isAvailable) {
							if (isAvailable) {
								return '<div class="text-center"><i class="fa fa-check-circle kt--font-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 6,
						 data: "survey.text",
						 name: "text"   
					}
            ]
        });


        function getSurveies() {
            dataTable.ajax.reload();
        }

        function deleteSurvey(survey) {
            abp.message.confirm(
                '',
                function (isConfirmed) {
                    if (isConfirmed) {
                        _surveiesService.delete({
                            id: survey.id
                        }).done(function () {
                            getSurveies(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewSurveyButton').click(function () {
            _createOrEditModal.open();
        });

		$('#ExportToExcelButton').click(function () {
            _surveiesService
                .getSurveiesToExcel({
				filter : $('#SurveiesTableFilter').val(),
					titleFilter: $('#TitleFilterId').val(),
					descriptionFilter: $('#DescriptionFilterId').val(),
					minStartDateFilter:  getDateFilter($('#MinStartDateFilterId')),
					maxStartDateFilter:  getDateFilter($('#MaxStartDateFilterId')),
					minEndDateFilter:  getDateFilter($('#MinEndDateFilterId')),
					maxEndDateFilter:  getDateFilter($('#MaxEndDateFilterId')),
					isAvailableFilter: $('#IsAvailableFilterId').val()
				})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        abp.event.on('app.createOrEditSurveyModalSaved', function () {
            getSurveies();
        });

		$('#GetSurveiesButton').click(function (e) {
            e.preventDefault();
            getSurveies();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getSurveies();
		  }
		});
        
    });
})();