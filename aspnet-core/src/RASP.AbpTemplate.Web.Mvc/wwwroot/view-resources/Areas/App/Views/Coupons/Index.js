(function () {
    $(function () {

        var _$couponsTable = $('#CouponsTable');
        var _couponsService = abp.services.app.coupons;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Administration.Coupons.Create'),
            edit: abp.auth.hasPermission('Pages.Administration.Coupons.Edit'),
            'delete': abp.auth.hasPermission('Pages.Administration.Coupons.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Coupons/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Coupons/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditCouponModal'
        });

		 var _viewCouponModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Coupons/ViewcouponModal',
            modalClass: 'ViewCouponModal'
        });

		
		

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }

        var dataTable = _$couponsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _couponsService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#CouponsTableFilter').val(),
					titleFilter: $('#TitleFilterId').val(),
					descriptionFilter: $('#DescriptionFilterId').val(),
					contentFilter: $('#ContentFilterId').val(),
					valueCouponFilter: $('#ValueCouponFilterId').val(),
					minTimeStartFilter:  getDateFilter($('#MinTimeStartFilterId')),
					maxTimeStartFilter:  getDateFilter($('#MaxTimeStartFilterId')),
					minTimeEndFilter:  getDateFilter($('#MinTimeEndFilterId')),
					maxTimeEndFilter:  getDateFilter($('#MaxTimeEndFilterId')),
					isCountDownToEndFilter: $('#IsCountDownToEndFilterId').val(),
					isCountDownToStartFilter: $('#IsCountDownToStartFilterId').val(),
					isAvailableFilter: $('#IsAvailableFilterId').val(),
					authorFilter: $('#AuthorFilterId').val(),
					isShowCarouselFilter: $('#IsShowCarouselFilterId').val(),
					imageNameFilter: $('#ImageNameFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    width: 120,
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                action: function (data) {
                                    _viewCouponModal.open({ id: data.record.coupon.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                                _createOrEditModal.open({ id: data.record.coupon.id });
                            }
                        }, 
						{
                            text: app.localize('Delete'),
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteCoupon(data.record.coupon);
                            }
                        }]
                    }
                },
					{
						targets: 1,
						 data: "coupon.title",
						 name: "title"   
					},
					{
						targets: 2,
						 data: "coupon.description",
						 name: "description"   
					},
					{
						targets: 3,
						 data: "coupon.content",
						 name: "content"   
					},
					{
						targets: 4,
						 data: "coupon.valueCoupon",
						 name: "valueCoupon"   
					},
					{
						targets: 5,
						 data: "coupon.timeStart",
						 name: "timeStart" ,
					render: function (timeStart) {
						if (timeStart) {
							return moment(timeStart).format('L');
						}
						return "";
					}
			  
					},
					{
						targets: 6,
						 data: "coupon.timeEnd",
						 name: "timeEnd" ,
					render: function (timeEnd) {
						if (timeEnd) {
							return moment(timeEnd).format('L');
						}
						return "";
					}
			  
					},
					{
						targets: 7,
						 data: "coupon.isCountDownToEnd",
						 name: "isCountDownToEnd"  ,
						render: function (isCountDownToEnd) {
							if (isCountDownToEnd) {
								return '<div class="text-center"><i class="fa fa-check-circle kt--font-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 8,
						 data: "coupon.isCountDownToStart",
						 name: "isCountDownToStart"  ,
						render: function (isCountDownToStart) {
							if (isCountDownToStart) {
								return '<div class="text-center"><i class="fa fa-check-circle kt--font-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 9,
						 data: "coupon.isAvailable",
						 name: "isAvailable"  ,
						render: function (isAvailable) {
							if (isAvailable) {
								return '<div class="text-center"><i class="fa fa-check-circle kt--font-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 10,
						 data: "coupon.author",
						 name: "author"   
					},
					{
						targets: 11,
						 data: "coupon.isShowCarousel",
						 name: "isShowCarousel"  ,
						render: function (isShowCarousel) {
							if (isShowCarousel) {
								return '<div class="text-center"><i class="fa fa-check-circle kt--font-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 12,
						 data: "coupon.imageName",
						 name: "imageName"   
					},
					{
						targets: 13,
						 data: "coupon.hit",
						 name: "hit"   
					}
            ]
        });


        function getCoupons() {
            dataTable.ajax.reload();
        }

        function deleteCoupon(coupon) {
            abp.message.confirm(
                '',
                function (isConfirmed) {
                    if (isConfirmed) {
                        _couponsService.delete({
                            id: coupon.id
                        }).done(function () {
                            getCoupons(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewCouponButton').click(function () {
            _createOrEditModal.open();
        });

		$('#ExportToExcelButton').click(function () {
            _couponsService
                .getCouponsToExcel({
				filter : $('#CouponsTableFilter').val(),
					titleFilter: $('#TitleFilterId').val(),
					descriptionFilter: $('#DescriptionFilterId').val(),
					contentFilter: $('#ContentFilterId').val(),
					valueCouponFilter: $('#ValueCouponFilterId').val(),
					minTimeStartFilter:  getDateFilter($('#MinTimeStartFilterId')),
					maxTimeStartFilter:  getDateFilter($('#MaxTimeStartFilterId')),
					minTimeEndFilter:  getDateFilter($('#MinTimeEndFilterId')),
					maxTimeEndFilter:  getDateFilter($('#MaxTimeEndFilterId')),
					isCountDownToEndFilter: $('#IsCountDownToEndFilterId').val(),
					isCountDownToStartFilter: $('#IsCountDownToStartFilterId').val(),
					isAvailableFilter: $('#IsAvailableFilterId').val(),
					authorFilter: $('#AuthorFilterId').val(),
					isShowCarouselFilter: $('#IsShowCarouselFilterId').val(),
					imageNameFilter: $('#ImageNameFilterId').val()
				})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        abp.event.on('app.createOrEditCouponModalSaved', function () {
            getCoupons();
        });

		$('#GetCouponsButton').click(function (e) {
            e.preventDefault();
            getCoupons();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getCoupons();
		  }
		});

    });
})();