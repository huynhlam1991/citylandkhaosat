(function () {
    $(function () {

        var _$rMailCustomersTable = $('#RMailCustomersTable');
        var _rMailCustomersService = abp.services.app.rMailCustomers;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Administration.RMailCustomers.Create'),
            edit: abp.auth.hasPermission('Pages.Administration.RMailCustomers.Edit'),
            'delete': abp.auth.hasPermission('Pages.Administration.RMailCustomers.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RMailCustomers/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/RMailCustomers/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditRMailCustomerModal'
        });

		 var _viewRMailCustomerModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RMailCustomers/ViewrMailCustomerModal',
            modalClass: 'ViewRMailCustomerModal'
        });

		
		

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }

        var dataTable = _$rMailCustomersTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _rMailCustomersService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#RMailCustomersTableFilter').val(),
					nameFilter: $('#NameFilterId').val(),
					emailFilter: $('#EmailFilterId').val(),
					privateCodeFilter: $('#PrivateCodeFilterId').val(),
					contentMailFilter: $('#ContentMailFilterId').val(),
					phoneNumberFilter: $('#PhoneNumberFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    width: 120,
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                action: function (data) {
                                    _viewRMailCustomerModal.open({ id: data.record.rMailCustomer.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                                _createOrEditModal.open({ id: data.record.rMailCustomer.id });
                            }
                        }, 
						{
                            text: app.localize('Delete'),
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteRMailCustomer(data.record.rMailCustomer);
                            }
                        }]
                    }
                },
					{
						targets: 1,
						 data: "rMailCustomer.name",
						 name: "name"   
					},
					{
						targets: 2,
						 data: "rMailCustomer.email",
						 name: "email"   
					},
					{
						targets: 3,
						 data: "rMailCustomer.privateCode",
						 name: "privateCode"   
					},
					{
						targets: 4,
						 data: "rMailCustomer.phoneNumber",
						 name: "phoneNumber"   
					}
            ]
        });


        function getRMailCustomers() {
            dataTable.ajax.reload();
        }

        function deleteRMailCustomer(rMailCustomer) {
            abp.message.confirm(
                '',
                function (isConfirmed) {
                    if (isConfirmed) {
                        _rMailCustomersService.delete({
                            id: rMailCustomer.id
                        }).done(function () {
                            getRMailCustomers(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewRMailCustomerButton').click(function () {
            _createOrEditModal.open();
        });

		$('#ExportToExcelButton').click(function () {
            _rMailCustomersService
                .getRMailCustomersToExcel({
				filter : $('#RMailCustomersTableFilter').val(),
					nameFilter: $('#NameFilterId').val(),
					emailFilter: $('#EmailFilterId').val(),
					privateCodeFilter: $('#PrivateCodeFilterId').val(),
					contentMailFilter: $('#ContentMailFilterId').val(),
					phoneNumberFilter: $('#PhoneNumberFilterId').val()
				})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });
        $('#SendMailButton').click(function () {
            abp.ui.setBusy($("body"));
            var contentMail = tinyMCE.editors[$('#ContentMail').attr('id')].getContent();
            _rMailCustomersService
                .sendMailMarketing({
                    filter: $('#RMailCustomersTableFilter').val(),
                    nameFilter: $('#NameFilterId').val(),
                    emailFilter: $('#EmailFilterId').val(),
                    privateCodeFilter: $('#PrivateCodeFilterId').val(),
                    contentMailFilter: $('#ContentMailFilterId').val(),
                    phoneNumberFilter: $('#PhoneNumberFilterId').val(),
                    contentMail: contentMail,
                    domain: $('#Domain').val(),
                    subject: $('#Subject').val()
                })
                .done(function (result) {
                    abp.notify.info(app.localize('Successfully'));
                    abp.ui.clearBusy($("body"));
                });
        });

        abp.event.on('app.createOrEditRMailCustomerModalSaved', function () {
            getRMailCustomers();
        });

		$('#GetRMailCustomersButton').click(function (e) {
            e.preventDefault();
            getRMailCustomers();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getRMailCustomers();
		  }
		});
        tinymce.init({
            selector: "#ContentMail", theme: "modern", height: 100,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor"
            ],
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
            toolbar2: "| link unlink anchor | forecolor backcolor  | print preview code ",
            image_advtab: true,
        });
    });
})();