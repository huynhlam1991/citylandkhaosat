(function ($) {
    app.modals.CreateOrEditRCategoryModal = function () {

        var _rCategoriesService = abp.services.app.rCategories;

        var _modalManager;
        var _$rCategoryInformationForm = null;

		

        this.init = function (modalManager) {
            _modalManager = modalManager;

			var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });

            _$rCategoryInformationForm = _modalManager.getModal().find('form[name=RCategoryInformationsForm]');
            _$rCategoryInformationForm.validate();
        };

		  

        this.save = function () {
            if (!_$rCategoryInformationForm.valid()) {
                return;
            }

            var rCategory = _$rCategoryInformationForm.serializeFormToObject();
			
			 _modalManager.setBusy(true);
			 _rCategoriesService.createOrEdit(
				rCategory
			 ).done(function () {
               abp.notify.info(app.localize('SavedSuccessfully'));
               _modalManager.close();
               abp.event.trigger('app.createOrEditRCategoryModalSaved');
			 }).always(function () {
               _modalManager.setBusy(false);
			});
        };
    };
})(jQuery);