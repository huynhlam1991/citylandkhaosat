(function ($) {
    app.modals.CreateOrEditRSliderModal = function () {

        var _rSlidersService = abp.services.app.rSliders;

        var _modalManager;
        var _$rSliderInformationForm = null;

		

        this.init = function (modalManager) {
            _modalManager = modalManager;

			var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });

            _$rSliderInformationForm = _modalManager.getModal().find('form[name=RSliderInformationsForm]');
            _$rSliderInformationForm.validate();
        };

		  

        this.save = function () {
            if (!_$rSliderInformationForm.valid()) {
                return;
            }

            var rSlider = _$rSliderInformationForm.serializeFormToObject();
			
			 _modalManager.setBusy(true);
			 _rSlidersService.createOrEdit(
				rSlider
			 ).done(function () {
               abp.notify.info(app.localize('SavedSuccessfully'));
               _modalManager.close();
               abp.event.trigger('app.createOrEditRSliderModalSaved');
			 }).always(function () {
               _modalManager.setBusy(false);
			});
        };
    };
})(jQuery);