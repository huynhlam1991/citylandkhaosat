(function () {
    $(function () {

        var _$questionsTable = $('#QuestionsTable');
        var _questionsService = abp.services.app.questions;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Administration.Questions.Create'),
            edit: abp.auth.hasPermission('Pages.Administration.Questions.Edit'),
            'delete': abp.auth.hasPermission('Pages.Administration.Questions.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Questions/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Questions/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditQuestionModal'
        });

		 var _viewQuestionModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Questions/ViewquestionModal',
            modalClass: 'ViewQuestionModal'
        });

		
		

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }

        var dataTable = _$questionsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _questionsService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#QuestionsTableFilter').val(),
					questionTextFilter: $('#QuestionTextFilterId').val(),
					questionTypeFilter: $('#QuestionTypeFilterId').val(),
					surveyTitleFilter: $('#SurveyTitleFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    width: 120,
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                action: function (data) {
                                    _viewQuestionModal.open({ id: data.record.question.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                                _createOrEditModal.open({ id: data.record.question.id });
                            }
                        }, 
						{
                            text: app.localize('Delete'),
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteQuestion(data.record.question);
                            }
                        }]
                    }
                },
					{
						targets: 1,
						 data: "question.questionText",
						 name: "questionText"   
					},
					{
						targets: 2,
						 data: "question.questionType",
						 name: "questionType"   ,
						render: function (questionType) {
							return app.localize('Enum_QuestionTypeEnum_' + questionType);
						}
			
					},
					{
						targets: 3,
						 data: "surveyTitle" ,
						 name: "surveyFk.title" 
					}
            ]
        });


        function getQuestions() {
            dataTable.ajax.reload();
        }

        function deleteQuestion(question) {
            abp.message.confirm(
                '',
                function (isConfirmed) {
                    if (isConfirmed) {
                        _questionsService.delete({
                            id: question.id
                        }).done(function () {
                            getQuestions(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewQuestionButton').click(function () {
            _createOrEditModal.open();
        });

		$('#ExportToExcelButton').click(function () {
            _questionsService
                .getQuestionsToExcel({
				filter : $('#QuestionsTableFilter').val(),
					questionTextFilter: $('#QuestionTextFilterId').val(),
					questionTypeFilter: $('#QuestionTypeFilterId').val(),
					surveyTitleFilter: $('#SurveyTitleFilterId').val()
				})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        abp.event.on('app.createOrEditQuestionModalSaved', function () {
            getQuestions();
        });

		$('#GetQuestionsButton').click(function (e) {
            e.preventDefault();
            getQuestions();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getQuestions();
		  }
		});

    });
})();