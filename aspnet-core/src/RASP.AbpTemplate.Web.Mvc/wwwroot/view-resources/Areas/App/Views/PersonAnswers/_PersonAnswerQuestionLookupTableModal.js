(function ($) {
    app.modals.QuestionLookupTableModal = function () {

        var _modalManager;

        var _personAnswersService = abp.services.app.personAnswers;
        var _$questionTable = $('#QuestionTable');

        this.init = function (modalManager) {
            _modalManager = modalManager;
        };


        var dataTable = _$questionTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _personAnswersService.getAllQuestionForLookupTable,
                inputFilter: function () {
                    return {
                        filter: $('#QuestionTableFilter').val()
                    };
                }
            },
            columnDefs: [
                {
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: "<div class=\"text-center\"><input id='selectbtn' class='btn btn-success' type='button' width='25px' value='" + app.localize('Select') + "' /></div>"
                },
                {
                    autoWidth: false,
                    orderable: false,
                    targets: 1,
                    data: "displayName"
                }
            ]
        });

        $('#QuestionTable tbody').on('click', '[id*=selectbtn]', function () {
            var data = dataTable.row($(this).parents('tr')).data();
            _modalManager.setResult(data);
            _modalManager.close();
        });

        function getQuestion() {
            dataTable.ajax.reload();
        }

        $('#GetQuestionButton').click(function (e) {
            e.preventDefault();
            getQuestion();
        });

        $('#SelectButton').click(function (e) {
            e.preventDefault();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getQuestion();
            }
        });

    };
})(jQuery);

