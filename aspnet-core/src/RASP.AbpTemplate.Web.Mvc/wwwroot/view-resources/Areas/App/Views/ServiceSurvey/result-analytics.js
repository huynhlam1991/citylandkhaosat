﻿
function getParams() {
    var url = window.location.href
        .slice(window.location.href.indexOf("?") + 1)
        .split("&");
    var result = {};
    url.forEach(function (item) {
        var param = item.split("=");
        result[param[0]] = param[1];
    });
    return result;
}
function init() {
    var json = {};
    var surveyId = decodeURI(getParams()["id"]);
    $.get("/api/SurveyService/getSurvey/?surveyId=" + surveyId, function (data) {
        
        json = JSON.parse(data);
        window.survey = new Survey.Model(json);

        //$("#surveyElement").Survey({ model: survey });
        var surveyResultNode = document.getElementById("surveyResult");
        surveyResultNode.innerHTML = "";
        $.get("/api/SurveyService/getSurveyResults/?surveyId=" + surveyId, function (result) {
            var dataJsonResult = {
                ResultCount: result.resultCount,
                Data: []
            };
            $.each(result.data, function (index, value) {
                var newObj = JSON.parse(value);
                dataJsonResult.Data.push(newObj);
            });
            var normalizedData = dataJsonResult
                .Data
            .map(function (item) {

                survey
                    .getAllQuestions()
                    .forEach(function (q) {
                        if (item[q.name] === undefined) {
                            item[q.name] = "";
                        }
                    });
                return item;
            });

        var visPanel = new SurveyAnalytics.VisualizationPanel(surveyResultNode, survey.getAllQuestions(), normalizedData);
        visPanel.render();
    });
    });
    

    
    
}

init();