(function ($) {
    app.modals.CreateOrEditProductModal = function () {

        var _productsService = abp.services.app.products;

        var _modalManager;
        var _$productInformationForm = null;

		        var _ProductrProductCategoryLookupTableModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Products/RProductCategoryLookupTableModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Products/_ProductRProductCategoryLookupTableModal.js',
            modalClass: 'RProductCategoryLookupTableModal'
        });

        this.init = function (modalManager) {
            _modalManager = modalManager;

			var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });

            _$productInformationForm = _modalManager.getModal().find('form[name=ProductInformationsForm]');
            _$productInformationForm.validate();
            
            tinymce.init({
                selector: "#Product_Content", theme: "modern", height: 300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                    "table contextmenu directionality emoticons paste textcolor"
                ],
                toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
                toolbar2: "| link unlink anchor | image media | forecolor backcolor  | print preview code ",
                image_advtab: true
            });
        };

		          $('#OpenRProductCategoryLookupTableButton').click(function () {

            var product = _$productInformationForm.serializeFormToObject();

            _ProductrProductCategoryLookupTableModal.open({ id: product.rProductCategoryId, displayName: product.rProductCategoryTitle }, function (data) {
                _$productInformationForm.find('input[name=rProductCategoryTitle]').val(data.displayName); 
                _$productInformationForm.find('input[name=rProductCategoryId]').val(data.id); 
            });
        });
		
		$('#ClearRProductCategoryTitleButton').click(function () {
                _$productInformationForm.find('input[name=rProductCategoryTitle]').val(''); 
                _$productInformationForm.find('input[name=rProductCategoryId]').val(''); 
        });
		


        this.save = function () {
            if (!_$productInformationForm.valid()) {
                return;
            }
            if ($('#Product_RProductCategoryId').prop('required') && $('#Product_RProductCategoryId').val() == '') {
                abp.message.error(app.localize('{0}IsRequired', app.localize('RProductCategory')));
                return;
            }

            var product = _$productInformationForm.serializeFormToObject();
			
			 _modalManager.setBusy(true);
			 _productsService.createOrEdit(
				product
			 ).done(function () {
               abp.notify.info(app.localize('SavedSuccessfully'));
               _modalManager.close();
               abp.event.trigger('app.createOrEditProductModalSaved');
			 }).always(function () {
               _modalManager.setBusy(false);
			});
        };
    };
})(jQuery);