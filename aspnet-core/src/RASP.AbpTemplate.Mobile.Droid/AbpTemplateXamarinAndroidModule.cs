﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace RASP.AbpTemplate
{
    [DependsOn(typeof(AbpTemplateXamarinSharedModule))]
    public class AbpTemplateXamarinAndroidModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(AbpTemplateXamarinAndroidModule).GetAssembly());
        }
    }
}