﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Abp.Web.Models;
using Abp.Domain.Repositories;
using RASP.AbpTemplate.RSurvey;
using Abp.Domain.Entities;
using RASP.AbpTemplate.RMarketing;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace RASP.AbpTemplate.Web.Controllers
{
    public class ChangeSurveyModel
    {
        public string Id { get; set; }
        public string Json { get; set; }
        public string Text { get; set; }
    }

    public class PostSurveyResultModel
    {
        public string postId { get; set; }
        public string surveyResult { get; set; }
        public string privateCode { get; set; }
    }
    public class ResultAnalyticsModel
    {
        public int ResultCount { get; set; }
        public List<string> Data { get; set; }
    }
    [DontWrapResult]
    [IgnoreAntiforgeryToken]
    [Route("api/[controller]/[action]")]
    public class SurveyServiceController : AbpTemplateControllerBase
    {

        private readonly IRepository<Survey, long> _surveyRepository;

        private readonly IRepository<SurveyResult, long> _surveyResultRepository;
        private readonly IRepository<RMailCustomer, Guid> _rMailCustomerRepository;
        public SurveyServiceController(IRepository<Survey, long> surveyRepository,
            IRepository<SurveyResult, long> surveyResultRepository,
            IRepository<RMailCustomer, Guid> rMailCustomerRepository
            )
        {
            _surveyRepository = surveyRepository;
            _surveyResultRepository = surveyResultRepository;
            _rMailCustomerRepository = rMailCustomerRepository;
        }

        [HttpGet]
        public JsonResult GetActive()
        {
            var db = new SessionStorage(HttpContext.Session, _surveyRepository, _surveyResultRepository);
            return Json(db.GetSurveys());
        }

        [HttpGet]
        public string getSurvey(string surveyId)
        {
            //var db = new SessionStorage(HttpContext.Session, _surveyRepository, _surveyResultRepository);
            var survey = _surveyRepository.FirstOrDefault(x => x.Id.ToString() == surveyId);

            return survey.GetData<string>("SurveyStorage");
        }

        [HttpGet]
        public JsonResult Create(string name)
        {
            //var db = new SessionStorage(HttpContext.Session, _surveyRepository, _surveyResultRepository);
            //db.StoreSurvey(name, "{}");
            var newSurvey = new Survey
            {
                Title = name
            };
            _surveyRepository.InsertAndGetId(newSurvey);

            return Json(newSurvey);
        }

        [HttpGet]
        public JsonResult ChangeName(string id, string name)
        {
            //var db = new SessionStorage(HttpContext.Session, _surveyRepository, _surveyResultRepository);
            //db.ChangeName(id, name);
            var survey = _surveyRepository.FirstOrDefault(x => x.Id.ToString() == id);
            survey.Title = name;
            _surveyRepository.Update(survey);
            return Json("Ok");
        }

        [HttpPost]
        public string ChangeJson([FromBody]ChangeSurveyModel model)
        {
            //var db = new SessionStorage(HttpContext.Session, _surveyRepository, _surveyResultRepository);
            //db.StoreSurvey(model.Id, model.Json);

            var survey = _surveyRepository.FirstOrDefault(x => x.Id.ToString() == model.Id);
            survey.SetData("SurveyStorage",model.Json);
            _surveyRepository.Update(survey);
            return survey.GetData<string>("SurveyStorage");
        }

        [HttpGet]
        public JsonResult Delete(string id)
        {
            //var db = new SessionStorage(HttpContext.Session, _surveyRepository, _surveyResultRepository);
            //db.DeleteSurvey(id);
            var survey = _surveyRepository.FirstOrDefault(x => x.Id.ToString() == id);
            _surveyRepository.Delete(survey);
            return Json("Ok");
        }
        [HttpPost]
        public JsonResult PostResult([FromBody]PostSurveyResultModel model)
        {
            //var db = new SessionStorage(HttpContext.Session, _surveyRepository, _surveyResultRepository);
            //db.PostResults(model.postId, model.surveyResult);
            if (!string.IsNullOrEmpty(model.privateCode))
            {
                model.privateCode = model.privateCode.ToUpper();
            }
            var person = _rMailCustomerRepository.FirstOrDefault(x => x.Id.ToString() == model.privateCode);
            if (person != null)
            {
                var surveyDoing = new SurveyResult
                {
                    SurveyId = Convert.ToInt64(model.postId),
                    Json = model.surveyResult,
                    UserId = 2,
                    Text = person.Email
                };
                surveyDoing.SetData("ResultsStorage", model.surveyResult);
                _surveyResultRepository.InsertAndGetId(surveyDoing);
            }
            
            //var surveyResult = _surveyResultRepository.FirstOrDefault(x => x.SurveyId.ToString() == model.postId);

            //surveyResult.SetData("ResultsStorage", model.surveyResult);
            return Json("OK");
        }

        [HttpGet]
        public JsonResult GetResults(string postId, string email)
        {
            var survey = _surveyResultRepository.GetAll().Where(x => x.SurveyId.ToString() == postId && x.Text == email).ToList();

            return Json(survey.Select(x => x.GetData<string>("ResultsStorage")));
            //var db = new SessionStorage(HttpContext.Session, _surveyRepository, _surveyResultRepository);
            //return Json(db.GetResults(postId));
        }
        public JsonResult getSurveyResults(string surveyId)
        {
            var survey = _surveyResultRepository.GetAll().Where(x => x.SurveyId.ToString() == surveyId).ToList();
            JsonSerializerSettings sa = new JsonSerializerSettings();
            var data = new ResultAnalyticsModel
            {
                ResultCount =  survey.Count,
                Data = survey.Select(x => x.GetData<string>("ResultsStorage")).ToList()
            };
            return Json(data);
            //return Json(null);
        }
    }
}
