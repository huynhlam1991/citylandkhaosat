﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.RealTime;
using Microsoft.AspNetCore.SignalR;
using RASP.AbpTemplate.Tiki;

namespace RASP.AbpTemplate.Web.Tiki.SignalR
{
    public class SignalRTikiCommunicator : ITikiCommunicator,ITransientDependency
    {
        private readonly IHubContext<TikiHub> _tikiHub;
        public SignalRTikiCommunicator(IHubContext<TikiHub> tikiHub)
        {
            _tikiHub = tikiHub;
        }
        public async Task SendMessageToClientAsync(IReadOnlyList<IOnlineClient> clients, string message)
        {
            foreach (var client in clients)
            {
                var signalRClient = GetSignalRClientOrNull(client);
                if (signalRClient == null)
                {
                    return;
                }

                await signalRClient.SendAsync("getTikiMessage", message);
            }
        }
        private IClientProxy GetSignalRClientOrNull(IOnlineClient client)
        {
            var signalRClient = _tikiHub.Clients.Client(client.ConnectionId);
            if (signalRClient == null)
            {
                return null;
            }

            return signalRClient;
        }
    }

}
