﻿using Abp;
using Abp.AspNetCore.SignalR.Hubs;
using Abp.Auditing;
using Abp.Localization;
using Abp.RealTime;
using Abp.Runtime.Session;
using Abp.UI;
using Castle.Core.Logging;
using Castle.Windsor;
using RASP.AbpTemplate.Chat;
using RASP.AbpTemplate.Tiki;
using RASP.AbpTemplate.Web.Chat.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RASP.AbpTemplate.Web.Tiki.SignalR
{
    public class TikiHub : OnlineClientHubBase
    {
        private readonly IChatMessageManager _chatMessageManager;
        private readonly ILocalizationManager _localizationManager;
        private readonly IWindsorContainer _windsorContainer;
        private bool _isCallByRelease;
        private readonly ITikiMessageManager _tikiMessageManager;
        /// <summary>
        /// Initializes a new instance of the <see cref="TikiHub"/> class.
        /// </summary>
        public TikiHub(
            ILocalizationManager localizationManager,
            IWindsorContainer windsorContainer,
            IOnlineClientManager<ChatChannel> onlineClientManager,
            IClientInfoProvider clientInfoProvider,
                ITikiMessageManager tikiMessageManager) : base(onlineClientManager, clientInfoProvider)
        {
            _localizationManager = localizationManager;
            _windsorContainer = windsorContainer;

            Logger = NullLogger.Instance;
            AbpSession = NullAbpSession.Instance;
            _tikiMessageManager = tikiMessageManager;
        }

        public async Task<string> SendMessage(SendChatMessageInput input)
        {
            var sender = AbpSession.ToUserIdentifier();
            var receiver = sender;

            try
            {
                await _tikiMessageManager.BatDau(sender, receiver, input.Message);
                return string.Empty;
            }
            catch (UserFriendlyException ex)
            {
                Logger.Warn("Could not send chat message to user: " + receiver);
                Logger.Warn(ex.ToString(), ex);
                return ex.Message;
            }
            catch (Exception ex)
            {
                Logger.Warn("Could not send chat message to user: " + receiver);
                Logger.Warn(ex.ToString(), ex);
                return _localizationManager.GetSource("AbpWeb").GetString("InternalServerError");
            }
        }

        public void Register()
        {
            Logger.Debug("A client is registered: " + Context.ConnectionId);
        }

        protected override void Dispose(bool disposing)
        {
            if (_isCallByRelease)
            {
                return;
            }
            base.Dispose(disposing);
            if (disposing)
            {
                _isCallByRelease = true;
                _windsorContainer.Release(this);
            }
        }
    }
}
