﻿using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using RASP.AbpTemplate.RSurvey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RASP.AbpTemplate.Web
{
    public class SessionStorage
    {
        private ISession session;
        private readonly IRepository<Survey, long> _surveyRepository;
        private readonly IRepository<SurveyResult, long> _surveyResultRepository;
        public SessionStorage(ISession session,
            IRepository<Survey, long> surveyRepository,
            IRepository<SurveyResult, long> surveyResultRepository)
        {
            this.session = session;
            _surveyRepository = surveyRepository;
            _surveyResultRepository = surveyResultRepository;
        }

        public T GetFromSession<T>(string storageId, T defaultValue)
        {
            if (string.IsNullOrEmpty(session.GetString(storageId)))
            {
                session.SetString(storageId, JsonConvert.SerializeObject(defaultValue));
            }
            var value = session.GetString(storageId);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }

        public Dictionary<string, string> GetSurveys()
        {
            Dictionary<string, string> surveys = new Dictionary<string, string>();
            var data = _surveyRepository.GetAll().ToList();
            foreach(var item in data)
            {
                if (!string.IsNullOrEmpty(item.ExtensionData))
                {
                    surveys[item.Id.ToString()] = item.GetData<string>("SurveyStorage");
                }
                else
                {
                    surveys[item.Id.ToString()] = "{}";
                }
            }
            return GetFromSession<Dictionary<string, string>>("SurveyStorage", surveys);
        }

        public Dictionary<string, List<string>> GetResults()
        {
            Dictionary<string, List<string>> results = new Dictionary<string, List<string>>();

            var data = _surveyResultRepository.GetAll().ToList();
            foreach(var item in data)
            {
                results[item.Id.ToString()] = item.GetData<List<string>>("ResultsStorage");
            }
            //results["Product feedback survey"] = new List<string>(
            //    new string[] {
            //        @"{""Quality"":{""affordable"":""5"",""better then others"":""5"",""does what it claims"":""5"",""easy to use"":""5""},""satisfaction"":5,""recommend friends"":5,""suggestions"":""I am happy!"",""price to competitors"":""Not sure"",""price"":""low"",""pricelimit"":{""mostamount"":""100"",""leastamount"":""100""}}",
            //        @"{""Quality"":{""affordable"":""3"",""does what it claims"":""2"",""better then others"":""2"",""easy to use"":""3""},""satisfaction"":3,""suggestions"":""better support"",""price to competitors"":""Not sure"",""price"":""high"",""pricelimit"":{""mostamount"":""60"",""leastamount"":""10""}}"
            //    }
            //);
            //results["Customer and his/her partner income survey"] = new List<string>(
            //    new string[] {
            //        @"{""member_arrray_employer"":[{}],""partner_arrray_employer"":[{}],""maritalstatus_c"":""Married"",""member_receives_income_from_employment"":""0"",""partner_receives_income_from_employment"":""0""}",
            //        @"{""member_arrray_employer"":[{}],""partner_arrray_employer"":[{}],""maritalstatus_c"":""Single"",""member_receives_income_from_employment"":""1"",""member_type_of_employment"":[""Self employment""],""member_seasonal_intermittent_or_contract_work"":""0""}"
            //    }
            //);
            return GetFromSession<Dictionary<string, List<string>>>("ResultsStorage", results);
        }

        public string GetSurvey(string surveyId)
        {
            return GetSurveys()[surveyId];
        }

        public void StoreSurvey(string surveyId, string jsonString)
        {
            var storage = GetSurveys();
            storage[surveyId] = jsonString;
            session.SetString("SurveyStorage", JsonConvert.SerializeObject(storage));
        }

        public void ChangeName(string id, string name)
        {
            var storage = GetSurveys();
            storage[name] = storage[id];
            storage.Remove(id);
            session.SetString("SurveyStorage", JsonConvert.SerializeObject(storage));
        }

        public void DeleteSurvey(string surveyId)
        {
            var storage = GetSurveys();
            storage.Remove(surveyId);
            session.SetString("SurveyStorage", JsonConvert.SerializeObject(storage));
        }

        public void PostResults(string postId, string resultJson)
        {
            var storage = GetResults();
            if (!storage.ContainsKey(postId))
            {
                storage[postId] = new List<string>();
            }
            storage[postId].Add(resultJson);
            session.SetString("ResultsStorage", JsonConvert.SerializeObject(storage));
        }

        public List<string> GetResults(string postId)
        {
            var storage = GetResults();
            return storage.ContainsKey(postId) ? storage[postId] : new List<string>();
        }
    }
}
