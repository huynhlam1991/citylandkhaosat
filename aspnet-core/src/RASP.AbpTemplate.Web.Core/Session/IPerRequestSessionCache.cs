﻿using System.Threading.Tasks;
using RASP.AbpTemplate.Sessions.Dto;

namespace RASP.AbpTemplate.Web.Session
{
    public interface IPerRequestSessionCache
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformationsAsync();
    }
}
