﻿using System;
using Abp.AutoMapper;
using RASP.AbpTemplate.Sessions.Dto;

namespace RASP.AbpTemplate.Models.Common
{
    [AutoMapFrom(typeof(ApplicationInfoDto)),
     AutoMapTo(typeof(ApplicationInfoDto))]
    public class ApplicationInfoPersistanceModel
    {
        public string Version { get; set; }

        public DateTime ReleaseDate { get; set; }
    }
}