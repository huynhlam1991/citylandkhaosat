﻿using Abp.AutoMapper;
using RASP.AbpTemplate.Organizations.Dto;

namespace RASP.AbpTemplate.Models.Users
{
    [AutoMapFrom(typeof(OrganizationUnitDto))]
    public class OrganizationUnitModel : OrganizationUnitDto
    {
        public bool IsAssigned { get; set; }
    }
}