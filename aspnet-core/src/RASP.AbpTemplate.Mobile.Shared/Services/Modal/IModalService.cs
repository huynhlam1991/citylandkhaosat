﻿using System.Threading.Tasks;
using RASP.AbpTemplate.Views;
using Xamarin.Forms;

namespace RASP.AbpTemplate.Services.Modal
{
    public interface IModalService
    {
        Task ShowModalAsync(Page page);

        Task ShowModalAsync<TView>(object navigationParameter) where TView : IXamarinView;

        Task<Page> CloseModalAsync();
    }
}
