﻿using System.Collections.Generic;
using MvvmHelpers;
using RASP.AbpTemplate.Models.NavigationMenu;

namespace RASP.AbpTemplate.Services.Navigation
{
    public interface IMenuProvider
    {
        ObservableRangeCollection<NavigationMenuItem> GetAuthorizedMenuItems(Dictionary<string, string> grantedPermissions);
    }
}