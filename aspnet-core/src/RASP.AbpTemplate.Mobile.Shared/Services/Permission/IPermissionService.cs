﻿namespace RASP.AbpTemplate.Services.Permission
{
    public interface IPermissionService
    {
        bool HasPermission(string key);
    }
}