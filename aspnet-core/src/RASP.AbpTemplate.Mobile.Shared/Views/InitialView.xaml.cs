﻿using Xamarin.Forms;

namespace RASP.AbpTemplate.Views
{
    public partial class InitialView : ContentPage, IXamarinView
    {
        public InitialView()
        {
            InitializeComponent();
        }
    }
}