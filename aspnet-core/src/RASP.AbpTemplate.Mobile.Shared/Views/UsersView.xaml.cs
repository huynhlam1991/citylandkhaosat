﻿using RASP.AbpTemplate.Models.Users;
using RASP.AbpTemplate.ViewModels;
using Xamarin.Forms;

namespace RASP.AbpTemplate.Views
{
    public partial class UsersView : ContentPage, IXamarinView
    {
        public UsersView()
        {
            InitializeComponent();
        }

        public async void ListView_OnItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            await ((UsersViewModel) BindingContext).LoadMoreUserIfNeedsAsync(e.Item as UserListModel);
        }
    }
}