﻿using Xamarin.Forms.Internals;

namespace RASP.AbpTemplate.Behaviors
{
    [Preserve(AllMembers = true)]
    public interface IAction
    {
        bool Execute(object sender, object parameter);
    }
}