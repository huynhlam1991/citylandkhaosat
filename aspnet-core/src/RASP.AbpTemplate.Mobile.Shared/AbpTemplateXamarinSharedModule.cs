﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace RASP.AbpTemplate
{
    [DependsOn(typeof(AbpTemplateClientModule), typeof(AbpAutoMapperModule))]
    public class AbpTemplateXamarinSharedModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Localization.IsEnabled = false;
            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(AbpTemplateXamarinSharedModule).GetAssembly());
        }
    }
}