﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace RASP.AbpTemplate
{
    [DependsOn(typeof(AbpTemplateXamarinSharedModule))]
    public class AbpTemplateXamarinIosModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(AbpTemplateXamarinIosModule).GetAssembly());
        }
    }
}