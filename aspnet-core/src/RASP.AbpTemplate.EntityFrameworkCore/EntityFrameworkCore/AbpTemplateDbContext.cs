using RASP.AbpTemplate.RMarketing;
using RASP.AbpTemplate.RSurvey;
using RASP.AbpTemplate.RModel;
using Abp.IdentityServer4;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using RASP.AbpTemplate.Authorization.Roles;
using RASP.AbpTemplate.Authorization.Users;
using RASP.AbpTemplate.Chat;
using RASP.AbpTemplate.Editions;
using RASP.AbpTemplate.Friendships;
using RASP.AbpTemplate.MultiTenancy;
using RASP.AbpTemplate.MultiTenancy.Accounting;
using RASP.AbpTemplate.MultiTenancy.Payments;
using RASP.AbpTemplate.Storage;

namespace RASP.AbpTemplate.EntityFrameworkCore
{
    public class AbpTemplateDbContext : AbpZeroDbContext<Tenant, Role, User, AbpTemplateDbContext>, IAbpPersistedGrantDbContext
    {
        public virtual DbSet<RMailCustomer> RMailCustomers { get; set; }

        public virtual DbSet<SurveyResult> SurveyResults { get; set; }

        public virtual DbSet<PersonAnswer> PersonAnswers { get; set; }

        public virtual DbSet<Answer> Answers { get; set; }

        public virtual DbSet<Question> Questions { get; set; }

        public virtual DbSet<Survey> Surveies { get; set; }

        public virtual DbSet<RProductCategory> RProductCategories { get; set; }

        public virtual DbSet<Product> Products { get; set; }

        public virtual DbSet<Coupon> Coupons { get; set; }

        public virtual DbSet<RSlider> RSliders { get; set; }

        public virtual DbSet<Article> Articles { get; set; }

        public virtual DbSet<RCategory> RCategories { get; set; }

        /* Define an IDbSet for each entity of the application */

        public virtual DbSet<BinaryObject> BinaryObjects { get; set; }

        public virtual DbSet<Friendship> Friendships { get; set; }

        public virtual DbSet<ChatMessage> ChatMessages { get; set; }

        public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }

        public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }

        public virtual DbSet<Invoice> Invoices { get; set; }

        public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }

        public AbpTemplateDbContext(DbContextOptions<AbpTemplateDbContext> options)
            : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<BinaryObject>(b =>
            {
                b.HasIndex(e => new { e.TenantId });
            });

            modelBuilder.Entity<ChatMessage>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
            });

            modelBuilder.Entity<Friendship>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId });
                b.HasIndex(e => new { e.TenantId, e.FriendUserId });
                b.HasIndex(e => new { e.FriendTenantId, e.UserId });
                b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
            });

            modelBuilder.Entity<Tenant>(b =>
            {
                b.HasIndex(e => new { e.SubscriptionEndDateUtc });
                b.HasIndex(e => new { e.CreationTime });
            });

            modelBuilder.Entity<SubscriptionPayment>(b =>
            {
                b.HasIndex(e => new { e.Status, e.CreationTime });
                b.HasIndex(e => new { PaymentId = e.ExternalPaymentId, e.Gateway });
            });

            modelBuilder.ConfigurePersistedGrantEntity();
        }
    }
}
