﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using RASP.AbpTemplate.Configuration;
using RASP.AbpTemplate.Web;

namespace RASP.AbpTemplate.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class AbpTemplateDbContextFactory : IDesignTimeDbContextFactory<AbpTemplateDbContext>
    {
        public AbpTemplateDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<AbpTemplateDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder(), addUserSecrets: true);

            AbpTemplateDbContextConfigurer.Configure(builder, configuration.GetConnectionString(AbpTemplateConsts.ConnectionStringName));

            return new AbpTemplateDbContext(builder.Options);
        }
    }
}