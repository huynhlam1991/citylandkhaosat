﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RASP.AbpTemplate.Migrations
{
    public partial class AddColumnJson : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ExtensionData",
                table: "Surveies",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Text",
                table: "Surveies",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExtensionData",
                table: "Surveies");

            migrationBuilder.DropColumn(
                name: "Text",
                table: "Surveies");
        }
    }
}
