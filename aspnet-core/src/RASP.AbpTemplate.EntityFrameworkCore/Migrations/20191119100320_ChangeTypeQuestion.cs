﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RASP.AbpTemplate.Migrations
{
    public partial class ChangeTypeQuestion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "QuestionType",
                table: "Questions",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 20);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "QuestionType",
                table: "Questions",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(int));
        }
    }
}
