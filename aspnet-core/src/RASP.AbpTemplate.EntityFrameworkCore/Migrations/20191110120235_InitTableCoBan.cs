﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RASP.AbpTemplate.Migrations
{
    public partial class InitTableCoBan : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Coupons",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Title = table.Column<string>(maxLength: 500, nullable: false),
                    Description = table.Column<string>(maxLength: 2000, nullable: true),
                    Content = table.Column<string>(nullable: true),
                    ValueCoupon = table.Column<string>(nullable: false),
                    TimeStart = table.Column<DateTime>(nullable: true),
                    TimeEnd = table.Column<DateTime>(nullable: true),
                    IsCountDownToEnd = table.Column<bool>(nullable: false),
                    IsCountDownToStart = table.Column<bool>(nullable: false),
                    IsAvailable = table.Column<bool>(nullable: false),
                    Author = table.Column<string>(maxLength: 250, nullable: true),
                    IsShowCarousel = table.Column<bool>(nullable: false),
                    ImageName = table.Column<string>(maxLength: 500, nullable: true),
                    Hit = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coupons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RCategories",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    MetaTitle = table.Column<string>(maxLength: 500, nullable: false),
                    MetaDescription = table.Column<string>(nullable: false),
                    ImageName = table.Column<string>(maxLength: 500, nullable: true),
                    Title = table.Column<string>(maxLength: 2000, nullable: false),
                    Description = table.Column<string>(maxLength: 2000, nullable: true),
                    Content = table.Column<string>(nullable: true),
                    Tag = table.Column<string>(nullable: true),
                    Author = table.Column<string>(maxLength: 250, nullable: true),
                    ParentId = table.Column<long>(nullable: true),
                    IsAvailable = table.Column<bool>(nullable: false),
                    Type = table.Column<string>(maxLength: 100, nullable: true),
                    IsShowMenuTop = table.Column<bool>(nullable: false),
                    IsShowMenuBottom = table.Column<bool>(nullable: false),
                    Priority = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RProductCategories",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    MetaTitle = table.Column<string>(maxLength: 500, nullable: true),
                    MetaDescription = table.Column<string>(maxLength: 2000, nullable: true),
                    Title = table.Column<string>(maxLength: 1000, nullable: true),
                    Description = table.Column<string>(maxLength: 2000, nullable: true),
                    Content = table.Column<string>(nullable: true),
                    Tag = table.Column<string>(nullable: true),
                    ParentId = table.Column<long>(nullable: true),
                    IsAvailable = table.Column<bool>(nullable: false),
                    IsShowMenuTop = table.Column<bool>(nullable: false),
                    IsShowMenuBottom = table.Column<bool>(nullable: false),
                    Priority = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RProductCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RSliders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Title = table.Column<string>(maxLength: 1000, nullable: false),
                    FileName = table.Column<string>(maxLength: 500, nullable: false),
                    Link = table.Column<string>(nullable: true),
                    Description = table.Column<string>(maxLength: 1000, nullable: true),
                    IsAvailable = table.Column<bool>(nullable: false),
                    PositionCode = table.Column<string>(maxLength: 250, nullable: true),
                    Priority = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RSliders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Articles",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    MetaTitle = table.Column<string>(maxLength: 500, nullable: false),
                    MetaDescription = table.Column<string>(nullable: false),
                    ImageName = table.Column<string>(maxLength: 500, nullable: true),
                    Tag = table.Column<string>(nullable: true),
                    Title = table.Column<string>(maxLength: 2000, nullable: false),
                    Description = table.Column<string>(maxLength: 2000, nullable: false),
                    Content = table.Column<string>(nullable: true),
                    Author = table.Column<string>(maxLength: 200, nullable: true),
                    IsAvailable = table.Column<bool>(nullable: false),
                    IsHot = table.Column<string>(nullable: true),
                    IsNew = table.Column<string>(nullable: true),
                    Priority = table.Column<int>(nullable: false),
                    Hit = table.Column<long>(nullable: false),
                    RCategoryId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Articles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Articles_RCategories_RCategoryId",
                        column: x => x.RCategoryId,
                        principalTable: "RCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    MetaTitle = table.Column<string>(maxLength: 500, nullable: false),
                    MetaDescription = table.Column<string>(maxLength: 2000, nullable: true),
                    Title = table.Column<string>(maxLength: 2000, nullable: false),
                    Description = table.Column<string>(maxLength: 2000, nullable: true),
                    Content = table.Column<string>(nullable: true),
                    Priority = table.Column<int>(nullable: false),
                    IsAvailable = table.Column<bool>(nullable: false),
                    IsHot = table.Column<bool>(nullable: false),
                    IsNew = table.Column<bool>(nullable: false),
                    ImageName = table.Column<string>(nullable: true),
                    PriceOrigin = table.Column<decimal>(nullable: true),
                    PricePromotion = table.Column<decimal>(nullable: true),
                    Hit = table.Column<long>(nullable: false),
                    SaleMan = table.Column<string>(maxLength: 500, nullable: true),
                    LinkReference = table.Column<string>(nullable: true),
                    ImageList = table.Column<string>(nullable: true),
                    IsContact = table.Column<string>(nullable: true),
                    DisplayIsContact = table.Column<string>(maxLength: 500, nullable: true),
                    RProductCategoryId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_RProductCategories_RProductCategoryId",
                        column: x => x.RProductCategoryId,
                        principalTable: "RProductCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Articles_RCategoryId",
                table: "Articles",
                column: "RCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_RProductCategoryId",
                table: "Products",
                column: "RProductCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Articles");

            migrationBuilder.DropTable(
                name: "Coupons");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "RSliders");

            migrationBuilder.DropTable(
                name: "RCategories");

            migrationBuilder.DropTable(
                name: "RProductCategories");
        }
    }
}
