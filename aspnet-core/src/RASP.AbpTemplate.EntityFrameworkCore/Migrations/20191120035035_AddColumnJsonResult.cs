﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RASP.AbpTemplate.Migrations
{
    public partial class AddColumnJsonResult : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ExtensionData",
                table: "SurveyResults",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExtensionData",
                table: "SurveyResults");
        }
    }
}
