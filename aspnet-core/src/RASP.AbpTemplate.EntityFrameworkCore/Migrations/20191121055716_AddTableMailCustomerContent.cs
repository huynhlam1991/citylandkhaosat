﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RASP.AbpTemplate.Migrations
{
    public partial class AddTableMailCustomerContent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ContentMail",
                table: "RMailCustomers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "RMailCustomers",
                maxLength: 100,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContentMail",
                table: "RMailCustomers");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "RMailCustomers");
        }
    }
}
