﻿using System.Collections.Generic;
using RASP.AbpTemplate.Authorization.Permissions.Dto;

namespace RASP.AbpTemplate.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}