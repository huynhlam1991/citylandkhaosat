﻿using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.Authorization.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}
