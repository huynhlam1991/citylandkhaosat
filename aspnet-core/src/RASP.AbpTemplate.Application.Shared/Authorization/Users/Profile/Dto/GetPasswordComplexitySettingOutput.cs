﻿using RASP.AbpTemplate.Security;

namespace RASP.AbpTemplate.Authorization.Users.Profile.Dto
{
    public class GetPasswordComplexitySettingOutput
    {
        public PasswordComplexitySetting Setting { get; set; }
    }
}
