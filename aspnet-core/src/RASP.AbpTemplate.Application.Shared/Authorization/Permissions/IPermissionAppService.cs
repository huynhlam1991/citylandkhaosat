﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Authorization.Permissions.Dto;

namespace RASP.AbpTemplate.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
