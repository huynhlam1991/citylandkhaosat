﻿using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.Authorization.Accounts.Dto
{
    public class SendEmailActivationLinkInput
    {
        [Required]
        public string EmailAddress { get; set; }
    }
}