﻿using System.Collections.Generic;

namespace RASP.AbpTemplate.Authorization.Roles.Dto
{
    public class GetRolesInput
    {
        public List<string> Permissions { get; set; }
    }
}
