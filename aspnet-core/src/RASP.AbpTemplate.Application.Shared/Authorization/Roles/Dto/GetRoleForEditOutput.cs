﻿using System.Collections.Generic;
using RASP.AbpTemplate.Authorization.Permissions.Dto;

namespace RASP.AbpTemplate.Authorization.Roles.Dto
{
    public class GetRoleForEditOutput
    {
        public RoleEditDto Role { get; set; }

        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}