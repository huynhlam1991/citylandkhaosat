﻿using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.Localization.Dto
{
    public class CreateOrUpdateLanguageInput
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}