﻿using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.Friendships.Dto
{
    public class AcceptFriendshipRequestInput
    {
        [Range(1, long.MaxValue)]
        public long UserId { get; set; }

        public int? TenantId { get; set; }
    }
}