﻿using System.Threading.Tasks;
using Abp.Application.Services;
using RASP.AbpTemplate.Configuration.Tenants.Dto;

namespace RASP.AbpTemplate.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);

        Task ClearLogo();

        Task ClearCustomCss();
    }
}
