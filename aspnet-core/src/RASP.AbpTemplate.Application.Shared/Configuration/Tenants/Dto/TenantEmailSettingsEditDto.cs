﻿using Abp.Auditing;
using RASP.AbpTemplate.Configuration.Dto;

namespace RASP.AbpTemplate.Configuration.Tenants.Dto
{
    public class TenantEmailSettingsEditDto : EmailSettingsEditDto
    {
        public bool UseHostDefaultEmailSettings { get; set; }
    }
}