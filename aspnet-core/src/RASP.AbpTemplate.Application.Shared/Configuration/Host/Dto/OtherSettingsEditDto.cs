﻿namespace RASP.AbpTemplate.Configuration.Host.Dto
{
    public class OtherSettingsEditDto
    {
        public bool IsQuickThemeSelectEnabled { get; set; }
    }
}