﻿using System.Threading.Tasks;
using Abp.Application.Services;
using RASP.AbpTemplate.Configuration.Host.Dto;

namespace RASP.AbpTemplate.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);

        Task SendTestEmail(SendTestEmailInput input);
    }
}
