﻿namespace RASP.AbpTemplate.Configuration.Dto
{
    public class ThemeFooterSettingsDto
    {
        public bool FixedFooter { get; set; }
    }
}