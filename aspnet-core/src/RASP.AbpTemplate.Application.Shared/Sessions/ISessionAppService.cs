﻿using System.Threading.Tasks;
using Abp.Application.Services;
using RASP.AbpTemplate.Sessions.Dto;

namespace RASP.AbpTemplate.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();

        Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken();
    }
}
