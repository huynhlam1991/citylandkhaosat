﻿using System.Collections.Generic;

namespace RASP.AbpTemplate.Logging.Dto
{
    public class GetLatestWebLogsOutput
    {
        public List<string> LatestWebLogLines { get; set; }
    }
}
