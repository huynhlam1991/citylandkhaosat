﻿using Abp.Application.Services;
using RASP.AbpTemplate.Dto;
using RASP.AbpTemplate.Logging.Dto;

namespace RASP.AbpTemplate.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}
