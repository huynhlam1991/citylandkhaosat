﻿using System.Threading.Tasks;
using Abp.Application.Services;
using RASP.AbpTemplate.Editions.Dto;
using RASP.AbpTemplate.MultiTenancy.Dto;

namespace RASP.AbpTemplate.MultiTenancy
{
    public interface ITenantRegistrationAppService: IApplicationService
    {
        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}