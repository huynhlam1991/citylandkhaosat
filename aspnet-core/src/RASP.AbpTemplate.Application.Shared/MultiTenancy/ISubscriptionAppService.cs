﻿using System.Threading.Tasks;
using Abp.Application.Services;

namespace RASP.AbpTemplate.MultiTenancy
{
    public interface ISubscriptionAppService : IApplicationService
    {
        Task DisableRecurringPayments();

        Task EnableRecurringPayments();
    }
}
