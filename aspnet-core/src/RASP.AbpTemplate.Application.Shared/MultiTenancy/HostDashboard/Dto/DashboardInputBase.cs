﻿using System;

namespace RASP.AbpTemplate.MultiTenancy.HostDashboard.Dto
{
    public class DashboardInputBase
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}