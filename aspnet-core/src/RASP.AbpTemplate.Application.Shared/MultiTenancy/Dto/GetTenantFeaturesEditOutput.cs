﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Editions.Dto;

namespace RASP.AbpTemplate.MultiTenancy.Dto
{
    public class GetTenantFeaturesEditOutput
    {
        public List<NameValueDto> FeatureValues { get; set; }

        public List<FlatFeatureDto> Features { get; set; }
    }
}