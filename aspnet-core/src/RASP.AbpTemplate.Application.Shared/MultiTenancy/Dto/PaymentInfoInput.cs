﻿namespace RASP.AbpTemplate.MultiTenancy.Dto
{
    public class PaymentInfoInput
    {
        public int? UpgradeEditionId { get; set; }
    }
}
