﻿namespace RASP.AbpTemplate.MultiTenancy.Payments.Stripe.Dto
{
    public class StripeConfigurationDto
    {
        public string PublishableKey { get; set; }
    }
}
