﻿namespace RASP.AbpTemplate.MultiTenancy.Payments.Stripe.Dto
{
    public class StripeUpdateSubscriptionInput
    {
        public long PaymentId { get; set; }
    }
}