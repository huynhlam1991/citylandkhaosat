﻿using System.Threading.Tasks;
using Abp.Application.Services;
using RASP.AbpTemplate.MultiTenancy.Payments.PayPal.Dto;

namespace RASP.AbpTemplate.MultiTenancy.Payments.PayPal
{
    public interface IPayPalPaymentAppService : IApplicationService
    {
        Task ConfirmPayment(long paymentId, string paypalOrderId);

        PayPalConfigurationDto GetConfiguration();
    }
}
