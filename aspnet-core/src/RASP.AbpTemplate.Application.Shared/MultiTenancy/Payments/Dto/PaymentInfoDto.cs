﻿using RASP.AbpTemplate.Editions.Dto;

namespace RASP.AbpTemplate.MultiTenancy.Payments.Dto
{
    public class PaymentInfoDto
    {
        public EditionSelectDto Edition { get; set; }

        public decimal AdditionalPrice { get; set; }
    }
}
