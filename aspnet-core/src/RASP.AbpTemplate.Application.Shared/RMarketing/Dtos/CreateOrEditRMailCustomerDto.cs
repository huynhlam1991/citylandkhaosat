
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RMarketing.Dtos
{
    public class CreateOrEditRMailCustomerDto : EntityDto<Guid?>
    {

		[StringLength(RMailCustomerConsts.MaxNameLength, MinimumLength = RMailCustomerConsts.MinNameLength)]
		public string Name { get; set; }
		
		
		[Required]
		[StringLength(RMailCustomerConsts.MaxEmailLength, MinimumLength = RMailCustomerConsts.MinEmailLength)]
		public string Email { get; set; }
		
		
		[StringLength(RMailCustomerConsts.MaxPrivateCodeLength, MinimumLength = RMailCustomerConsts.MinPrivateCodeLength)]
		public string PrivateCode { get; set; }
		
		
		public string ContentMail { get; set; }
		
		
		[StringLength(RMailCustomerConsts.MaxPhoneNumberLength, MinimumLength = RMailCustomerConsts.MinPhoneNumberLength)]
		public string PhoneNumber { get; set; }
		
		

    }
}