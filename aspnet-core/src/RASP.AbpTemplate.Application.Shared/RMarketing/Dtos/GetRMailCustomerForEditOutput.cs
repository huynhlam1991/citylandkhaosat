using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RMarketing.Dtos
{
    public class GetRMailCustomerForEditOutput
    {
		public CreateOrEditRMailCustomerDto RMailCustomer { get; set; }


    }
}