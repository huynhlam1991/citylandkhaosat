
using System;
using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RMarketing.Dtos
{
    public class RMailCustomerDto : EntityDto<Guid>
    {
		public string Name { get; set; }

		public string Email { get; set; }

		public string PrivateCode { get; set; }

		public string PhoneNumber { get; set; }



    }
}