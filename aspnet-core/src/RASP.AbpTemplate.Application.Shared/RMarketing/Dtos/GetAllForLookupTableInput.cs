using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RMarketing.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}