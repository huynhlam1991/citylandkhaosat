using Abp.Application.Services.Dto;
using System;

namespace RASP.AbpTemplate.RMarketing.Dtos
{
    public class GetAllRMailCustomersForExcelInput
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }

		public string EmailFilter { get; set; }

		public string PrivateCodeFilter { get; set; }

		public string ContentMailFilter { get; set; }

		public string PhoneNumberFilter { get; set; }



    }
}