using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.RMarketing.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RMarketing
{
    public interface IRMailCustomersAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRMailCustomerForViewDto>> GetAll(GetAllRMailCustomersInput input);

        Task<GetRMailCustomerForViewDto> GetRMailCustomerForView(Guid id);

		Task<GetRMailCustomerForEditOutput> GetRMailCustomerForEdit(EntityDto<Guid> input);

		Task CreateOrEdit(CreateOrEditRMailCustomerDto input);

		Task Delete(EntityDto<Guid> input);

		Task<FileDto> GetRMailCustomersToExcel(GetAllRMailCustomersForExcelInput input);

		
    }
}