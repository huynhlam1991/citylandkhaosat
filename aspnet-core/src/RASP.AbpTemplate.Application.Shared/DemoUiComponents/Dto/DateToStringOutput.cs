﻿namespace RASP.AbpTemplate.DemoUiComponents.Dto
{
    public class DateToStringOutput
    {
        public string DateString { get; set; }
    }
}
