﻿namespace RASP.AbpTemplate.Tenants.Dashboard.Dto
{
    public class GetSalesSummaryInput
    {
        public SalesSummaryDatePeriod SalesSummaryDatePeriod { get; set; }
    }
}