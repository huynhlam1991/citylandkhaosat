﻿using System;
using Abp.Notifications;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.Notifications.Dto
{
    public class GetUserNotificationsInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}