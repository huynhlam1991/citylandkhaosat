using Abp.Application.Services.Dto;
using System;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class GetAllCouponsForExcelInput
    {
		public string Filter { get; set; }

		public string TitleFilter { get; set; }

		public string DescriptionFilter { get; set; }

		public string ContentFilter { get; set; }

		public string ValueCouponFilter { get; set; }

		public DateTime? MaxTimeStartFilter { get; set; }
		public DateTime? MinTimeStartFilter { get; set; }

		public DateTime? MaxTimeEndFilter { get; set; }
		public DateTime? MinTimeEndFilter { get; set; }

		public int IsCountDownToEndFilter { get; set; }

		public int IsCountDownToStartFilter { get; set; }

		public int IsAvailableFilter { get; set; }

		public string AuthorFilter { get; set; }

		public int IsShowCarouselFilter { get; set; }

		public string ImageNameFilter { get; set; }



    }
}