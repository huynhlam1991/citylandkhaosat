using Abp.Application.Services.Dto;
using System;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class GetAllProductsForExcelInput
    {
		public string Filter { get; set; }

		public string MetaTitleFilter { get; set; }

		public string MetaDescriptionFilter { get; set; }

		public string TitleFilter { get; set; }

		public string DescriptionFilter { get; set; }

		public string ContentFilter { get; set; }

		public int? MaxPriorityFilter { get; set; }
		public int? MinPriorityFilter { get; set; }

		public int IsAvailableFilter { get; set; }

		public int IsHotFilter { get; set; }

		public int IsNewFilter { get; set; }

		public decimal? MaxPriceOriginFilter { get; set; }
		public decimal? MinPriceOriginFilter { get; set; }

		public decimal? MaxPricePromotionFilter { get; set; }
		public decimal? MinPricePromotionFilter { get; set; }

		public string SaleManFilter { get; set; }

		public string LinkReferenceFilter { get; set; }

		public string IsContactFilter { get; set; }


		 public string RProductCategoryTitleFilter { get; set; }

		 
    }
}