
using System;
using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class ProductDto : EntityDto<long>
    {
		public string Title { get; set; }

		public int Priority { get; set; }

		public bool IsAvailable { get; set; }

		public bool IsHot { get; set; }

		public bool IsNew { get; set; }

		public string ImageName { get; set; }

		public decimal? PriceOrigin { get; set; }

		public decimal? PricePromotion { get; set; }

		public long Hit { get; set; }


		 public long? RProductCategoryId { get; set; }

		 
    }
}