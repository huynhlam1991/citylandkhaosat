
using System;
using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class CouponDto : EntityDto<long>
    {
		public string Title { get; set; }

		public string Description { get; set; }

		public string Content { get; set; }

		public string ValueCoupon { get; set; }

		public DateTime? TimeStart { get; set; }

		public DateTime? TimeEnd { get; set; }

		public bool IsCountDownToEnd { get; set; }

		public bool IsCountDownToStart { get; set; }

		public bool IsAvailable { get; set; }

		public string Author { get; set; }

		public bool IsShowCarousel { get; set; }

		public string ImageName { get; set; }

		public long Hit { get; set; }



    }
}