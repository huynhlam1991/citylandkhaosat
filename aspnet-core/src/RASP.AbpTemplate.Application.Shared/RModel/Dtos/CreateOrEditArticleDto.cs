
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class CreateOrEditArticleDto : EntityDto<long?>
    {

		[Required]
		[StringLength(ArticleConsts.MaxMetaTitleLength, MinimumLength = ArticleConsts.MinMetaTitleLength)]
		public string MetaTitle { get; set; }
		
		
		[Required]
		public string MetaDescription { get; set; }
		
		
		[StringLength(ArticleConsts.MaxImageNameLength, MinimumLength = ArticleConsts.MinImageNameLength)]
		public string ImageName { get; set; }
		
		
		public string Tag { get; set; }
		
		
		[Required]
		[StringLength(ArticleConsts.MaxTitleLength, MinimumLength = ArticleConsts.MinTitleLength)]
		public string Title { get; set; }
		
		
		[Required]
		[StringLength(ArticleConsts.MaxDescriptionLength, MinimumLength = ArticleConsts.MinDescriptionLength)]
		public string Description { get; set; }
		
		
		public string Content { get; set; }
		
		
		[StringLength(ArticleConsts.MaxAuthorLength, MinimumLength = ArticleConsts.MinAuthorLength)]
		public string Author { get; set; }
		
		
		public bool IsAvailable { get; set; }
		
		
		public string IsHot { get; set; }
		
		
		public string IsNew { get; set; }
		
		
		public int Priority { get; set; }
		
		
		public long Hit { get; set; }
		
		
		 public long? RCategoryId { get; set; }
		 
		 
    }
}