using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class GetRCategoryForEditOutput
    {
		public CreateOrEditRCategoryDto RCategory { get; set; }


    }
}