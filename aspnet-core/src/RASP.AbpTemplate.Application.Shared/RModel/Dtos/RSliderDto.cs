
using System;
using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class RSliderDto : EntityDto
    {
		public string Title { get; set; }

		public string FileName { get; set; }

		public string Description { get; set; }

		public bool IsAvailable { get; set; }

		public string PositionCode { get; set; }

		public int Priority { get; set; }



    }
}