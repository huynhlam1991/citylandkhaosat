
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class CreateOrEditCouponDto : EntityDto<long?>
    {

		[Required]
		[StringLength(CouponConsts.MaxTitleLength, MinimumLength = CouponConsts.MinTitleLength)]
		public string Title { get; set; }
		
		
		[StringLength(CouponConsts.MaxDescriptionLength, MinimumLength = CouponConsts.MinDescriptionLength)]
		public string Description { get; set; }
		
		
		public string Content { get; set; }
		
		
		[Required]
		public string ValueCoupon { get; set; }
		
		
		public DateTime? TimeStart { get; set; }
		
		
		public DateTime? TimeEnd { get; set; }
		
		
		public bool IsCountDownToEnd { get; set; }
		
		
		public bool IsCountDownToStart { get; set; }
		
		
		public bool IsAvailable { get; set; }
		
		
		[StringLength(CouponConsts.MaxAuthorLength, MinimumLength = CouponConsts.MinAuthorLength)]
		public string Author { get; set; }
		
		
		public bool IsShowCarousel { get; set; }
		
		
		[StringLength(CouponConsts.MaxImageNameLength, MinimumLength = CouponConsts.MinImageNameLength)]
		public string ImageName { get; set; }
		
		
		public long Hit { get; set; }
		
		

    }
}