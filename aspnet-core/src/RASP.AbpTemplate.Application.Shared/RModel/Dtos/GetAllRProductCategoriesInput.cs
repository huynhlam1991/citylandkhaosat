using Abp.Application.Services.Dto;
using System;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class GetAllRProductCategoriesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string MetaTitleFilter { get; set; }

		public string MetaDescriptionFilter { get; set; }

		public string TitleFilter { get; set; }

		public string DescriptionFilter { get; set; }

		public string ContentFilter { get; set; }

		public string TagFilter { get; set; }

		public long? MaxParentIdFilter { get; set; }
		public long? MinParentIdFilter { get; set; }

		public int IsAvailableFilter { get; set; }

		public int IsShowMenuTopFilter { get; set; }

		public int IsShowMenuBottomFilter { get; set; }

		public int? MaxPriorityFilter { get; set; }
		public int? MinPriorityFilter { get; set; }



    }
}