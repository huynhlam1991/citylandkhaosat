using Abp.Application.Services.Dto;
using System;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class GetAllRSlidersForExcelInput
    {
		public string Filter { get; set; }

		public string TitleFilter { get; set; }

		public string FileNameFilter { get; set; }

		public string LinkFilter { get; set; }

		public string DescriptionFilter { get; set; }

		public int IsAvailableFilter { get; set; }

		public string PositionCodeFilter { get; set; }

		public int? MaxPriorityFilter { get; set; }
		public int? MinPriorityFilter { get; set; }



    }
}