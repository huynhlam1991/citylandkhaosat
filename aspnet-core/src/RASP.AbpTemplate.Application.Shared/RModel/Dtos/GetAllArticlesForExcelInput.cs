using Abp.Application.Services.Dto;
using System;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class GetAllArticlesForExcelInput
    {
		public string Filter { get; set; }

		public string MetaTitleFilter { get; set; }

		public string MetaDescriptionFilter { get; set; }

		public string TagFilter { get; set; }

		public string TitleFilter { get; set; }

		public string DescriptionFilter { get; set; }

		public string ContentFilter { get; set; }

		public string AuthorFilter { get; set; }

		public int IsAvailableFilter { get; set; }

		public string IsHotFilter { get; set; }

		public string IsNewFilter { get; set; }

		public int? MaxPriorityFilter { get; set; }
		public int? MinPriorityFilter { get; set; }

		public long? MaxHitFilter { get; set; }
		public long? MinHitFilter { get; set; }


		 public string RCategoryTitleFilter { get; set; }

		 
    }
}