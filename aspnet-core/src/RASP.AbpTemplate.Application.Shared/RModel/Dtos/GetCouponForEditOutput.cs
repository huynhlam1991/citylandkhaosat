using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class GetCouponForEditOutput
    {
		public CreateOrEditCouponDto Coupon { get; set; }


    }
}