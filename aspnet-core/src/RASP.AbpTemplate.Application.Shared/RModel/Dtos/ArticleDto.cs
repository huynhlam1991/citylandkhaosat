
using System;
using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class ArticleDto : EntityDto<long>
    {
		public string MetaTitle { get; set; }

		public string MetaDescription { get; set; }

		public string ImageName { get; set; }

		public string Title { get; set; }

		public string Author { get; set; }

		public bool IsAvailable { get; set; }

		public string IsHot { get; set; }

		public string IsNew { get; set; }

		public int Priority { get; set; }

		public long Hit { get; set; }


		 public long? RCategoryId { get; set; }

		 
    }
}