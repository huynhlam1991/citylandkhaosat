using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}