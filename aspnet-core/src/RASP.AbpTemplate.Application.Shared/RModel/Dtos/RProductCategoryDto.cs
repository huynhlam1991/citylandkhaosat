
using System;
using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class RProductCategoryDto : EntityDto<long>
    {
		public string MetaTitle { get; set; }

		public string MetaDescription { get; set; }

		public string Title { get; set; }

		public string Description { get; set; }

		public bool IsAvailable { get; set; }

		public bool IsShowMenuTop { get; set; }

		public bool IsShowMenuBottom { get; set; }

		public int Priority { get; set; }



    }
}