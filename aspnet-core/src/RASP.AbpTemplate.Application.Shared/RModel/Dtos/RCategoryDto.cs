
using System;
using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class RCategoryDto : EntityDto<long>
    {
		public string MetaTitle { get; set; }

		public string Title { get; set; }

		public string Description { get; set; }

		public string Tag { get; set; }

		public string Author { get; set; }

		public long? ParentId { get; set; }

		public bool IsAvailable { get; set; }

		public bool IsShowMenuTop { get; set; }

		public bool IsShowMenuBottom { get; set; }

		public int Priority { get; set; }



    }
}