
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class CreateOrEditProductDto : EntityDto<long?>
    {

		[Required]
		[StringLength(ProductConsts.MaxMetaTitleLength, MinimumLength = ProductConsts.MinMetaTitleLength)]
		public string MetaTitle { get; set; }
		
		
		[StringLength(ProductConsts.MaxMetaDescriptionLength, MinimumLength = ProductConsts.MinMetaDescriptionLength)]
		public string MetaDescription { get; set; }
		
		
		[Required]
		[StringLength(ProductConsts.MaxTitleLength, MinimumLength = ProductConsts.MinTitleLength)]
		public string Title { get; set; }
		
		
		[StringLength(ProductConsts.MaxDescriptionLength, MinimumLength = ProductConsts.MinDescriptionLength)]
		public string Description { get; set; }
		
		
		public string Content { get; set; }
		
		
		public int Priority { get; set; }
		
		
		public bool IsAvailable { get; set; }
		
		
		public bool IsHot { get; set; }
		
		
		public bool IsNew { get; set; }
		
		
		public string ImageName { get; set; }
		
		
		public decimal? PriceOrigin { get; set; }
		
		
		public decimal? PricePromotion { get; set; }
		
		
		public long Hit { get; set; }
		
		
		[StringLength(ProductConsts.MaxSaleManLength, MinimumLength = ProductConsts.MinSaleManLength)]
		public string SaleMan { get; set; }
		
		
		public string LinkReference { get; set; }
		
		
		public string ImageList { get; set; }
		
		
		public string IsContact { get; set; }
		
		
		[StringLength(ProductConsts.MaxDisplayIsContactLength, MinimumLength = ProductConsts.MinDisplayIsContactLength)]
		public string DisplayIsContact { get; set; }
		
		
		 public long? RProductCategoryId { get; set; }
		 
		 
    }
}