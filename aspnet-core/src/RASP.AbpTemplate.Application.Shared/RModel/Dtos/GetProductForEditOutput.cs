using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class GetProductForEditOutput
    {
		public CreateOrEditProductDto Product { get; set; }

		public string RProductCategoryTitle { get; set;}


    }
}