
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class CreateOrEditRCategoryDto : EntityDto<long?>
    {

		[Required]
		[StringLength(RCategoryConsts.MaxMetaTitleLength, MinimumLength = RCategoryConsts.MinMetaTitleLength)]
		public string MetaTitle { get; set; }
		
		
		[Required]
		public string MetaDescription { get; set; }
		
		
		[StringLength(RCategoryConsts.MaxImageNameLength, MinimumLength = RCategoryConsts.MinImageNameLength)]
		public string ImageName { get; set; }
		
		
		[Required]
		[StringLength(RCategoryConsts.MaxTitleLength, MinimumLength = RCategoryConsts.MinTitleLength)]
		public string Title { get; set; }
		
		
		[Required]
		[StringLength(RCategoryConsts.MaxDescriptionLength, MinimumLength = RCategoryConsts.MinDescriptionLength)]
		public string Description { get; set; }
		
		
		public string Content { get; set; }
		
		
		public string Tag { get; set; }
		
		
		[StringLength(RCategoryConsts.MaxAuthorLength, MinimumLength = RCategoryConsts.MinAuthorLength)]
		public string Author { get; set; }
		
		
		public long? ParentId { get; set; }
		
		
		public bool IsAvailable { get; set; }
		
		
		public bool IsShowMenuTop { get; set; }
		
		
		public bool IsShowMenuBottom { get; set; }
		
		
		public int Priority { get; set; }
		
		

    }
}