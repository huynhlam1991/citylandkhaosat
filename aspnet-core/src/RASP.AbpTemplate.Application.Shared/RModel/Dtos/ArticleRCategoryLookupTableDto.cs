using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class ArticleRCategoryLookupTableDto
    {
		public long Id { get; set; }

		public string DisplayName { get; set; }
    }
}