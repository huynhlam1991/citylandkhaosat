
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class CreateOrEditRSliderDto : EntityDto<int?>
    {

		[Required]
		[StringLength(RSliderConsts.MaxTitleLength, MinimumLength = RSliderConsts.MinTitleLength)]
		public string Title { get; set; }
		
		
		[Required]
		[StringLength(RSliderConsts.MaxFileNameLength, MinimumLength = RSliderConsts.MinFileNameLength)]
		public string FileName { get; set; }
		
		
		public string Link { get; set; }
		
		
		[StringLength(RSliderConsts.MaxDescriptionLength, MinimumLength = RSliderConsts.MinDescriptionLength)]
		public string Description { get; set; }
		
		
		public bool IsAvailable { get; set; }
		
		
		[StringLength(RSliderConsts.MaxPositionCodeLength, MinimumLength = RSliderConsts.MinPositionCodeLength)]
		public string PositionCode { get; set; }
		
		
		public int Priority { get; set; }
		
		

    }
}