using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class GetArticleForEditOutput
    {
		public CreateOrEditArticleDto Article { get; set; }

		public string RCategoryTitle { get; set;}


    }
}