
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RModel.Dtos
{
    public class CreateOrEditRProductCategoryDto : EntityDto<long?>
    {

		[StringLength(RProductCategoryConsts.MaxMetaTitleLength, MinimumLength = RProductCategoryConsts.MinMetaTitleLength)]
		public string MetaTitle { get; set; }
		
		
		[StringLength(RProductCategoryConsts.MaxMetaDescriptionLength, MinimumLength = RProductCategoryConsts.MinMetaDescriptionLength)]
		public string MetaDescription { get; set; }
		
		
		[StringLength(RProductCategoryConsts.MaxTitleLength, MinimumLength = RProductCategoryConsts.MinTitleLength)]
		public string Title { get; set; }
		
		
		[StringLength(RProductCategoryConsts.MaxDescriptionLength, MinimumLength = RProductCategoryConsts.MinDescriptionLength)]
		public string Description { get; set; }
		
		
		public string Content { get; set; }
		
		
		public string Tag { get; set; }
		
		
		public long? ParentId { get; set; }
		
		
		public bool IsAvailable { get; set; }
		
		
		public bool IsShowMenuTop { get; set; }
		
		
		public bool IsShowMenuBottom { get; set; }
		
		
		public int Priority { get; set; }
		
		

    }
}