using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RModel
{
    public interface IArticlesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetArticleForViewDto>> GetAll(GetAllArticlesInput input);

        Task<GetArticleForViewDto> GetArticleForView(long id);

		Task<GetArticleForEditOutput> GetArticleForEdit(EntityDto<long> input);

		Task CreateOrEdit(CreateOrEditArticleDto input);

		Task Delete(EntityDto<long> input);

		Task<FileDto> GetArticlesToExcel(GetAllArticlesForExcelInput input);

		
		Task<PagedResultDto<ArticleRCategoryLookupTableDto>> GetAllRCategoryForLookupTable(GetAllForLookupTableInput input);
		
    }
}