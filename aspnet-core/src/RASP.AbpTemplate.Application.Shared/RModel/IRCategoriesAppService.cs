using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RModel
{
    public interface IRCategoriesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRCategoryForViewDto>> GetAll(GetAllRCategoriesInput input);

        Task<GetRCategoryForViewDto> GetRCategoryForView(long id);

		Task<GetRCategoryForEditOutput> GetRCategoryForEdit(EntityDto<long> input);

		Task CreateOrEdit(CreateOrEditRCategoryDto input);

		Task Delete(EntityDto<long> input);

		Task<FileDto> GetRCategoriesToExcel(GetAllRCategoriesForExcelInput input);

		
    }
}