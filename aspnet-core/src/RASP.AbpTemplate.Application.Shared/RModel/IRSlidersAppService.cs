using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RModel
{
    public interface IRSlidersAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRSliderForViewDto>> GetAll(GetAllRSlidersInput input);

        Task<GetRSliderForViewDto> GetRSliderForView(int id);

		Task<GetRSliderForEditOutput> GetRSliderForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditRSliderDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetRSlidersToExcel(GetAllRSlidersForExcelInput input);

		
    }
}