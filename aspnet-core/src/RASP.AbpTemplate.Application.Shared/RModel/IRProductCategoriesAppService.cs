using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RModel
{
    public interface IRProductCategoriesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRProductCategoryForViewDto>> GetAll(GetAllRProductCategoriesInput input);

        Task<GetRProductCategoryForViewDto> GetRProductCategoryForView(long id);

		Task<GetRProductCategoryForEditOutput> GetRProductCategoryForEdit(EntityDto<long> input);

		Task CreateOrEdit(CreateOrEditRProductCategoryDto input);

		Task Delete(EntityDto<long> input);

		Task<FileDto> GetRProductCategoriesToExcel(GetAllRProductCategoriesForExcelInput input);

		
    }
}