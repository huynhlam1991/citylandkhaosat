using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RModel
{
    public interface IProductsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetProductForViewDto>> GetAll(GetAllProductsInput input);

        Task<GetProductForViewDto> GetProductForView(long id);

		Task<GetProductForEditOutput> GetProductForEdit(EntityDto<long> input);

		Task CreateOrEdit(CreateOrEditProductDto input);

		Task Delete(EntityDto<long> input);

		Task<FileDto> GetProductsToExcel(GetAllProductsForExcelInput input);

		
		Task<PagedResultDto<ProductRProductCategoryLookupTableDto>> GetAllRProductCategoryForLookupTable(GetAllForLookupTableInput input);
		
    }
}