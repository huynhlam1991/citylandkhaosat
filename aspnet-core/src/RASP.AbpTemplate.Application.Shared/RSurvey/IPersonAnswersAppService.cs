using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RSurvey
{
    public interface IPersonAnswersAppService : IApplicationService 
    {
        Task<PagedResultDto<GetPersonAnswerForViewDto>> GetAll(GetAllPersonAnswersInput input);

        Task<GetPersonAnswerForViewDto> GetPersonAnswerForView(long id);

		Task<GetPersonAnswerForEditOutput> GetPersonAnswerForEdit(EntityDto<long> input);

		Task CreateOrEdit(CreateOrEditPersonAnswerDto input);

		Task Delete(EntityDto<long> input);

		Task<FileDto> GetPersonAnswersToExcel(GetAllPersonAnswersForExcelInput input);

		
		Task<PagedResultDto<PersonAnswerSurveyLookupTableDto>> GetAllSurveyForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<PersonAnswerQuestionLookupTableDto>> GetAllQuestionForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<PersonAnswerAnswerLookupTableDto>> GetAllAnswerForLookupTable(GetAllForLookupTableInput input);
		
    }
}