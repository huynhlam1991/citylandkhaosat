using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RSurvey
{
    public interface ISurveyResultsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetSurveyResultForViewDto>> GetAll(GetAllSurveyResultsInput input);

        Task<GetSurveyResultForViewDto> GetSurveyResultForView(long id);

		Task<GetSurveyResultForEditOutput> GetSurveyResultForEdit(EntityDto<long> input);

		Task CreateOrEdit(CreateOrEditSurveyResultDto input);

		Task Delete(EntityDto<long> input);

		Task<FileDto> GetSurveyResultsToExcel(GetAllSurveyResultsForExcelInput input);

		
		Task<PagedResultDto<SurveyResultSurveyLookupTableDto>> GetAllSurveyForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<SurveyResultUserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input);
		
    }
}