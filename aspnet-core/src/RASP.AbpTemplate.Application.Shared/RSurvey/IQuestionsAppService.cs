using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RSurvey
{
    public interface IQuestionsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetQuestionForViewDto>> GetAll(GetAllQuestionsInput input);

        Task<GetQuestionForViewDto> GetQuestionForView(long id);

		Task<GetQuestionForEditOutput> GetQuestionForEdit(EntityDto<long> input);

		Task CreateOrEdit(CreateOrEditQuestionDto input);

		Task Delete(EntityDto<long> input);

		Task<FileDto> GetQuestionsToExcel(GetAllQuestionsForExcelInput input);

		
		Task<PagedResultDto<QuestionSurveyLookupTableDto>> GetAllSurveyForLookupTable(GetAllForLookupTableInput input);
		
    }
}