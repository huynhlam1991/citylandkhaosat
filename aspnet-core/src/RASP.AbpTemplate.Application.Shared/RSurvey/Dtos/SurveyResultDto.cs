
using System;
using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class SurveyResultDto : EntityDto<long>
    {
		public string Text { get; set; }

		public string Json { get; set; }


		 public long SurveyId { get; set; }

		 		 public long UserId { get; set; }

		 
    }
}