using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class GetPersonAnswerForEditOutput
    {
		public CreateOrEditPersonAnswerDto PersonAnswer { get; set; }

		public string SurveyTitle { get; set;}

		public string QuestionQuestionText { get; set;}

		public string AnswerAnswerText { get; set;}


    }
}