using RASP.AbpTemplate.RSurvey;

using System;
using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class QuestionDto : EntityDto<long>
    {
		public string QuestionText { get; set; }

		public QuestionTypeEnum QuestionType { get; set; }


		 public long? SurveyId { get; set; }

		 
    }
}