using Abp.Application.Services.Dto;
using System;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class GetAllPersonAnswersInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string OtherTextFilter { get; set; }


		 public string SurveyTitleFilter { get; set; }

		 		 public string QuestionQuestionTextFilter { get; set; }

		 		 public string AnswerAnswerTextFilter { get; set; }

		 
    }
}