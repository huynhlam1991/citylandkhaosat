using Abp.Application.Services.Dto;
using System;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class GetAllSurveiesForExcelInput
    {
		public long SurveyId { get; set; }
		public string Filter { get; set; }

		public string TitleFilter { get; set; }

		public string DescriptionFilter { get; set; }

		public DateTime? MaxStartDateFilter { get; set; }
		public DateTime? MinStartDateFilter { get; set; }

		public DateTime? MaxEndDateFilter { get; set; }
		public DateTime? MinEndDateFilter { get; set; }

		public int IsAvailableFilter { get; set; }



    }
    public class GetSurveyResult
    {
        public long SurveyId { get; set; }
    }
}