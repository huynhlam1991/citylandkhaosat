
using System;
using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class AnswerDto : EntityDto<long>
    {
		public string AnswerText { get; set; }


		 public long? QuestionId { get; set; }

		 
    }
}