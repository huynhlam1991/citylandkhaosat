namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class GetPersonAnswerForViewDto
    {
		public PersonAnswerDto PersonAnswer { get; set; }

		public string SurveyTitle { get; set;}

		public string QuestionQuestionText { get; set;}

		public string AnswerAnswerText { get; set;}


    }
}