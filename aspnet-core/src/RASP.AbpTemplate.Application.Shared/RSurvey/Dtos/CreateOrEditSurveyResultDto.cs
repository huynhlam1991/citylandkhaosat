
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class CreateOrEditSurveyResultDto : EntityDto<long?>, IExtendableObject
    {

		public string Text { get; set; }
		
		
		public string Json { get; set; }
		
		
		 public long SurveyId { get; set; }
		 
		 		 public long UserId { get; set; }

        public string ExtensionData { get; set; }
    }
}