using Abp.Application.Services.Dto;
using System;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class GetAllSurveyResultsForExcelInput
    {
		public string Filter { get; set; }

		public string TextFilter { get; set; }

		public string JsonFilter { get; set; }


		 public string SurveyTitleFilter { get; set; }

		 		 public string UserNameFilter { get; set; }

		 
    }
}