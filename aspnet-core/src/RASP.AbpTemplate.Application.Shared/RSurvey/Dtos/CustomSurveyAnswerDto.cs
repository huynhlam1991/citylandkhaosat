﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class CustomSurveyAnswerDto
    {
        public CustomSurveyAnswerDto()
        {
            Matrixs = new List<MatrixSurvey>();
            Options = new List<string>();
        }
        public string QuestionTitle { get; set; }
        public List<MatrixSurvey> Matrixs { get; set; }
        public List<string> Options { get; set; }
        public string Text { get; set; }
        public string CreatedDate { get; set; }
        public string Email { get; set; }

    }
    public class MatrixSurvey
    {
        public string Column { get; set; }
        public string Row { get; set; }
        public string Value { get; set; }
    }
    
    
}
