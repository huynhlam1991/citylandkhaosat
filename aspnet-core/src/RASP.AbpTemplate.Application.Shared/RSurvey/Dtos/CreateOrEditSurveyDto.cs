
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class CreateOrEditSurveyDto : EntityDto<long?>
    {

		[Required]
		[StringLength(SurveyConsts.MaxTitleLength, MinimumLength = SurveyConsts.MinTitleLength)]
		public string Title { get; set; }
		
		
		[Required]
		[StringLength(SurveyConsts.MaxDescriptionLength, MinimumLength = SurveyConsts.MinDescriptionLength)]
		public string Description { get; set; }
		
		
		public DateTime? StartDate { get; set; }
		
		
		public DateTime? EndDate { get; set; }
		
		
		public bool IsAvailable { get; set; }
		
		
		public string Text { get; set; }
		
		

    }
}