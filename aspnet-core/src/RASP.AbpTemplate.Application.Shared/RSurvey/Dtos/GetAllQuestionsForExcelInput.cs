using Abp.Application.Services.Dto;
using System;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class GetAllQuestionsForExcelInput
    {
		public string Filter { get; set; }

		public string QuestionTextFilter { get; set; }

		public int QuestionTypeFilter { get; set; }


		 public string SurveyTitleFilter { get; set; }

		 
    }
}