
using System;
using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class SurveyDto : EntityDto<long>
    {
		public string Title { get; set; }

		public string Description { get; set; }

		public DateTime? StartDate { get; set; }

		public DateTime? EndDate { get; set; }

		public bool IsAvailable { get; set; }

		public string Text { get; set; }



    }
}