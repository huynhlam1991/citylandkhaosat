
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class CreateOrEditAnswerDto : EntityDto<long?>
    {

		[Required]
		public string AnswerText { get; set; }
		
		
		 public long? QuestionId { get; set; }
		 
		 
    }
}