using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class PersonAnswerAnswerLookupTableDto
    {
		public long Id { get; set; }

		public string DisplayName { get; set; }
    }
}