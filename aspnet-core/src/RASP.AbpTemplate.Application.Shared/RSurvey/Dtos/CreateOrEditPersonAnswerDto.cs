
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class CreateOrEditPersonAnswerDto : EntityDto<long?>
    {

		public string OtherText { get; set; }
		
		
		 public long? SurveyId { get; set; }
		 
		 		 public long? QuestionId { get; set; }
		 
		 		 public long? AnswerId { get; set; }
		 
		 
    }
}