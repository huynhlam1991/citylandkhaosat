namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class GetSurveyResultForViewDto
    {
		public SurveyResultDto SurveyResult { get; set; }

		public string SurveyTitle { get; set;}

		public string UserName { get; set;}


    }
}