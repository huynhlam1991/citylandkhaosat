using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}