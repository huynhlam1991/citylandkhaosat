using Abp.Application.Services.Dto;
using System;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class GetAllAnswersForExcelInput
    {
		public string Filter { get; set; }

		public string AnswerTextFilter { get; set; }


		 public string QuestionQuestionTextFilter { get; set; }

		 
    }
}