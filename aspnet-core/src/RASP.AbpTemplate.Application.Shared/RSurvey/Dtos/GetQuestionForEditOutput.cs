using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class GetQuestionForEditOutput
    {
		public CreateOrEditQuestionDto Question { get; set; }

		public string SurveyTitle { get; set;}


    }
}