using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class GetSurveyResultForEditOutput
    {
		public CreateOrEditSurveyResultDto SurveyResult { get; set; }

		public string SurveyTitle { get; set;}

		public string UserName { get; set;}


    }
}