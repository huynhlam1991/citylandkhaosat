﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class CustomSurveyResultDto
    {
        public CustomSurveyResultDto()
        {
            pages = new List<PageSurveyDto>();
        }
        public string title { get; set; }
        public List<PageSurveyDto> pages { get; set; }
    }
    public class PageSurveyDto
    {
        public PageSurveyDto()
        {
            elements = new List<ElementSurveyDto>();
        }
        public string name { get; set; }
        public List<ElementSurveyDto> elements { get; set; }
    }
    public class ElementSurveyDto
    {
        public ElementSurveyDto()
        {
            choices = new List<ItemSurveyDto>();
            columns = new List<string>();
        }
        public string type { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public bool isRequired { get; set; }
        private string _visibleIf;

        public string visibleIf
        {
            get
            {
                return _visibleIf;
            }
            set
            {

                _visibleIf = value;
                if (!string.IsNullOrEmpty(_visibleIf) && _visibleIf.Split(' ').Length > 2)
                {
                    var strTemp = _visibleIf.Split(' ')[2];
                    strTemp = strTemp.TrimStart(new char[] { '"' });
                    _visibleIf = strTemp.TrimEnd(new char[] { '"' });
                }
            }
        }
        public List<ItemSurveyDto> choices { get; set; }
        public List<string> columns { get; set; }
        public List<ItemSurveyRowDto> rows { get; set; }
        public string otherText { get; set; }
        public bool isAllRowRequired { get; set; }
    }
    public class ItemSurveyDto
    {
        public string value { get; set; }
        public string text { get; set; }
    }
    public class ItemSurveyRowDto
    {
        public string value { get; set; }
        public string text { get; set; }
        

    }
    //Custom
    public class ExportSurvey
    {
        public ExportSurvey()
        {
            Titles = new List<TitleExport>();
            RowSelect = new List<RowSurveyExport>();
            elements = new List<ElementSurveyDto>();
            answer = new List<AnswerQuestion>();
        }
        public List<TitleExport> Titles { get; set; }
        public List<RowSurveyExport> RowSelect { get; set; }

        public List<ElementSurveyDto> elements { get; set; }
        public List<AnswerQuestion> answer { get; set; }
    }
    public class TitleExport
    {
        public string Title { get; set; }
        public string Name { get; set; }

    }
    public class RowSurveyExport
    {
        public int STT { get; set; }
        public string NameQuestion { get; set; }
        public string Value { get; set; }
    }


    public class AnswerQuestion
    {

        public string Email { get; set; }
        public string CreationTime { get; set; }
        public string questionKey { get; set; }
        public string questionText { get; set; }

        public string AnswerText { get; set; }
    }
}
