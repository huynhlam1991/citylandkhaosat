
using System;
using Abp.Application.Services.Dto;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class PersonAnswerDto : EntityDto<long>
    {
		public string OtherText { get; set; }


		 public long? SurveyId { get; set; }

		 		 public long? QuestionId { get; set; }

		 		 public long? AnswerId { get; set; }

		 
    }
}