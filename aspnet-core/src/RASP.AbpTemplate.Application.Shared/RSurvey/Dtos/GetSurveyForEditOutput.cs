using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class GetSurveyForEditOutput
    {
		public CreateOrEditSurveyDto Survey { get; set; }


    }
}