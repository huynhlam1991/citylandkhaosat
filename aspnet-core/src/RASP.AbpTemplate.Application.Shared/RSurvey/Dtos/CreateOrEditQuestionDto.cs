using RASP.AbpTemplate.RSurvey;

using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class CreateOrEditQuestionDto : EntityDto<long?>
    {

		[Required]
		public string QuestionText { get; set; }
		
		
		public QuestionTypeEnum QuestionType { get; set; }
		
		
		 public long? SurveyId { get; set; }
		 
		 
    }
}