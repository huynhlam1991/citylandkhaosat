using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace RASP.AbpTemplate.RSurvey.Dtos
{
    public class GetAnswerForEditOutput
    {
		public CreateOrEditAnswerDto Answer { get; set; }

		public string QuestionQuestionText { get; set;}


    }
}