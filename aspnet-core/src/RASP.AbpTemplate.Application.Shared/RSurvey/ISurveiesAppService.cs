using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RSurvey
{
    public interface ISurveiesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetSurveyForViewDto>> GetAll(GetAllSurveiesInput input);

        Task<GetSurveyForViewDto> GetSurveyForView(long id);

		Task<GetSurveyForEditOutput> GetSurveyForEdit(EntityDto<long> input);

		Task CreateOrEdit(CreateOrEditSurveyDto input);

		Task Delete(EntityDto<long> input);

		Task<FileDto> GetSurveiesToExcel(GetAllSurveiesForExcelInput input);
        Task<FileDto> GetSurveiesToExcelDetail(GetAllSurveiesForExcelInput input);
        FileDto GetSurveiesResultToExcel(GetSurveyResult filter);


    }
}