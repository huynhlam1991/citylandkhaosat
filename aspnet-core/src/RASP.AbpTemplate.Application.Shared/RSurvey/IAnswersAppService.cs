using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RSurvey
{
    public interface IAnswersAppService : IApplicationService 
    {
        Task<PagedResultDto<GetAnswerForViewDto>> GetAll(GetAllAnswersInput input);

        Task<GetAnswerForViewDto> GetAnswerForView(long id);

		Task<GetAnswerForEditOutput> GetAnswerForEdit(EntityDto<long> input);

		Task CreateOrEdit(CreateOrEditAnswerDto input);

		Task Delete(EntityDto<long> input);

		Task<FileDto> GetAnswersToExcel(GetAllAnswersForExcelInput input);

		
		Task<PagedResultDto<AnswerQuestionLookupTableDto>> GetAllQuestionForLookupTable(GetAllForLookupTableInput input);
		
    }
}