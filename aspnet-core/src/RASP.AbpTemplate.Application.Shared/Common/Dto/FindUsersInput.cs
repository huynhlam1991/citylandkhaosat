﻿using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.Common.Dto
{
    public class FindUsersInput : PagedAndFilteredInputDto
    {
        public int? TenantId { get; set; }
    }
}