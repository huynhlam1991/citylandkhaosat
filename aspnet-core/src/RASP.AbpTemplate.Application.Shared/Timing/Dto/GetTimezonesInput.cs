﻿using Abp.Configuration;

namespace RASP.AbpTemplate.Timing.Dto
{
    public class GetTimezonesInput
    {
        public SettingScopes DefaultTimezoneScope { get; set; }
    }
}
