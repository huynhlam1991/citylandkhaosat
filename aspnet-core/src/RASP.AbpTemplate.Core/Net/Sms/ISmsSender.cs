﻿using System.Threading.Tasks;

namespace RASP.AbpTemplate.Net.Sms
{
    public interface ISmsSender
    {
        Task SendAsync(string number, string message);
    }
}