﻿using Abp.Domain.Services;

namespace RASP.AbpTemplate
{
    public abstract class AbpTemplateDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected AbpTemplateDomainServiceBase()
        {
            LocalizationSourceName = AbpTemplateConsts.LocalizationSourceName;
        }
    }
}
