using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace RASP.AbpTemplate.RMarketing
{
	[Table("RMailCustomers")]
    public class RMailCustomer : FullAuditedEntity<Guid> 
    {

		[StringLength(RMailCustomerConsts.MaxNameLength, MinimumLength = RMailCustomerConsts.MinNameLength)]
		public virtual string Name { get; set; }
		
		[Required]
		[StringLength(RMailCustomerConsts.MaxEmailLength, MinimumLength = RMailCustomerConsts.MinEmailLength)]
		public virtual string Email { get; set; }
		
		[StringLength(RMailCustomerConsts.MaxPrivateCodeLength, MinimumLength = RMailCustomerConsts.MinPrivateCodeLength)]
		public virtual string PrivateCode { get; set; }
		
		public virtual string ContentMail { get; set; }
		
		[StringLength(RMailCustomerConsts.MaxPhoneNumberLength, MinimumLength = RMailCustomerConsts.MinPhoneNumberLength)]
		public virtual string PhoneNumber { get; set; }
		

    }
}