using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace RASP.AbpTemplate.RSurvey
{
	[Table("Surveies")]
    public class Survey : FullAuditedEntity<long>, IExtendableObject
    {

		[Required]
		[StringLength(SurveyConsts.MaxTitleLength, MinimumLength = SurveyConsts.MinTitleLength)]
		public virtual string Title { get; set; }
		
		[Required]
		[StringLength(SurveyConsts.MaxDescriptionLength, MinimumLength = SurveyConsts.MinDescriptionLength)]
		public virtual string Description { get; set; }
		
		public virtual DateTime? StartDate { get; set; }
		
		public virtual DateTime? EndDate { get; set; }
		
		public virtual bool IsAvailable { get; set; }
		
		public virtual string Text { get; set; }
        public virtual string ExtensionData { get; set; }

    }
}