using RASP.AbpTemplate.RSurvey;
using RASP.AbpTemplate.RSurvey;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace RASP.AbpTemplate.RSurvey
{
	[Table("Questions")]
    public class Question : FullAuditedEntity<long> 
    {

		[Required]
		public virtual string QuestionText { get; set; }
		
		public virtual QuestionTypeEnum QuestionType { get; set; }
		

		public virtual long? SurveyId { get; set; }
		
        [ForeignKey("SurveyId")]
		public Survey SurveyFk { get; set; }
		
    }
}