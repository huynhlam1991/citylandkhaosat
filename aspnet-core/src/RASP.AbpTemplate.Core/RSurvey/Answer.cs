using RASP.AbpTemplate.RSurvey;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace RASP.AbpTemplate.RSurvey
{
	[Table("Answers")]
    public class Answer : FullAuditedEntity<long> 
    {

		[Required]
		public virtual string AnswerText { get; set; }
		

		public virtual long? QuestionId { get; set; }
		
        [ForeignKey("QuestionId")]
		public Question QuestionFk { get; set; }
		
    }
}