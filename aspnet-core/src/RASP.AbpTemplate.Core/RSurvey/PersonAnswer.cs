using RASP.AbpTemplate.RSurvey;
using RASP.AbpTemplate.RSurvey;
using RASP.AbpTemplate.RSurvey;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace RASP.AbpTemplate.RSurvey
{
	[Table("PersonAnswers")]
    public class PersonAnswer : FullAuditedEntity<long> 
    {

		public virtual string OtherText { get; set; }
		

		public virtual long? SurveyId { get; set; }
		
        [ForeignKey("SurveyId")]
		public Survey SurveyFk { get; set; }
		
		public virtual long? QuestionId { get; set; }
		
        [ForeignKey("QuestionId")]
		public Question QuestionFk { get; set; }
		
		public virtual long? AnswerId { get; set; }
		
        [ForeignKey("AnswerId")]
		public Answer AnswerFk { get; set; }
		
    }
}