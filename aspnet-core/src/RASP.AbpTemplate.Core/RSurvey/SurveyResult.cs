using RASP.AbpTemplate.RSurvey;
using RASP.AbpTemplate.Authorization.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace RASP.AbpTemplate.RSurvey
{
	[Table("SurveyResults")]
    public class SurveyResult : FullAuditedEntity<long>, IExtendableObject 
    {

		public virtual string Text { get; set; }
		
		public virtual string Json { get; set; }
		

		public virtual long SurveyId { get; set; }
		
        [ForeignKey("SurveyId")]
		public Survey SurveyFk { get; set; }
		
		public virtual long UserId { get; set; }
		
        [ForeignKey("UserId")]
		public User UserFk { get; set; }
        public virtual string ExtensionData { get; set; }

    }
}