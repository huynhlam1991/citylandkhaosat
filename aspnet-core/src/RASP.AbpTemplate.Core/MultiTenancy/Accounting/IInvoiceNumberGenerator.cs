﻿using System.Threading.Tasks;
using Abp.Dependency;

namespace RASP.AbpTemplate.MultiTenancy.Accounting
{
    public interface IInvoiceNumberGenerator : ITransientDependency
    {
        Task<string> GetNewInvoiceNumber();
    }
}