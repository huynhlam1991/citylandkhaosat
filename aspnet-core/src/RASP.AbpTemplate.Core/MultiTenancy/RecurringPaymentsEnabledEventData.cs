﻿using Abp.Events.Bus;

namespace RASP.AbpTemplate.MultiTenancy
{
    public class RecurringPaymentsEnabledEventData : EventData
    {
        public int TenantId { get; set; }
    }
}