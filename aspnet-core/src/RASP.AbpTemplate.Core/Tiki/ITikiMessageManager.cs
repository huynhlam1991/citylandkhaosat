﻿using Abp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RASP.AbpTemplate.Tiki
{
    public interface ITikiMessageManager
    {
        Task BatDau(UserIdentifier sender, UserIdentifier receiver, string message);
    }
}
