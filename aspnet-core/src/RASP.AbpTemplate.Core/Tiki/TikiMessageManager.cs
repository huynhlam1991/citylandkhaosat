﻿using Abp;
using Abp.RealTime;
using RASP.AbpTemplate.Chat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RASP.AbpTemplate.Tiki
{
    public class TikiMessageManager: AbpTemplateDomainServiceBase, ITikiMessageManager
    {
        private readonly IOnlineClientManager<ChatChannel> _onlineClientManager;
        private readonly ITikiCommunicator _tikiCommunicator;
        public TikiMessageManager(IOnlineClientManager<ChatChannel> onlineClientManager,
            ITikiCommunicator tikiCommunicator)
        {
            _onlineClientManager = onlineClientManager;
            _tikiCommunicator = tikiCommunicator;
        }
        public async Task BatDau(UserIdentifier sender, UserIdentifier receiver, string message)
        {
            var clients = _onlineClientManager.GetAllByUserId(receiver);
            if (clients.Any())
            {
                var count = Convert.ToInt32(message) + 1;
                await _tikiCommunicator.SendMessageToClientAsync(clients, count.ToString());
            }
        }
    }
}
