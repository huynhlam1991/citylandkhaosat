﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.RealTime;

namespace RASP.AbpTemplate.Tiki
{
    public interface ITikiCommunicator
    {
        Task SendMessageToClientAsync(IReadOnlyList<IOnlineClient> clients, string message);
    }
}
