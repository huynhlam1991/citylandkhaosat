﻿using Abp.RealTime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RASP.AbpTemplate.Tiki
{
    public class NullTikiCommunicator: ITikiCommunicator
    {
        public async Task SendMessageToClientAsync(IReadOnlyList<IOnlineClient> clients, string message)
        {
            await Task.CompletedTask;
        }
    }
}
