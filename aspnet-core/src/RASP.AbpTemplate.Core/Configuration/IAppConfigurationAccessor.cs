﻿using Microsoft.Extensions.Configuration;

namespace RASP.AbpTemplate.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}
