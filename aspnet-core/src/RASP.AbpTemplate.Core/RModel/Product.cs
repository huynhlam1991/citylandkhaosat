using RASP.AbpTemplate.RModel;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;

namespace RASP.AbpTemplate.RModel
{
	[Table("Products")]
    [Audited]
    public class Product : FullAuditedEntity<long> 
    {

		[Required]
		[StringLength(ProductConsts.MaxMetaTitleLength, MinimumLength = ProductConsts.MinMetaTitleLength)]
		public virtual string MetaTitle { get; set; }
		
		[StringLength(ProductConsts.MaxMetaDescriptionLength, MinimumLength = ProductConsts.MinMetaDescriptionLength)]
		public virtual string MetaDescription { get; set; }
		
		[Required]
		[StringLength(ProductConsts.MaxTitleLength, MinimumLength = ProductConsts.MinTitleLength)]
		public virtual string Title { get; set; }
		
		[StringLength(ProductConsts.MaxDescriptionLength, MinimumLength = ProductConsts.MinDescriptionLength)]
		public virtual string Description { get; set; }
		
		public virtual string Content { get; set; }
		
		public virtual int Priority { get; set; }
		
		public virtual bool IsAvailable { get; set; }
		
		public virtual bool IsHot { get; set; }
		
		public virtual bool IsNew { get; set; }
		
		public virtual string ImageName { get; set; }
		
		public virtual decimal? PriceOrigin { get; set; }
		
		public virtual decimal? PricePromotion { get; set; }
		
		public virtual long Hit { get; set; }
		
		[StringLength(ProductConsts.MaxSaleManLength, MinimumLength = ProductConsts.MinSaleManLength)]
		public virtual string SaleMan { get; set; }
		
		public virtual string LinkReference { get; set; }
		
		public virtual string ImageList { get; set; }
		
		public virtual string IsContact { get; set; }
		
		[StringLength(ProductConsts.MaxDisplayIsContactLength, MinimumLength = ProductConsts.MinDisplayIsContactLength)]
		public virtual string DisplayIsContact { get; set; }
		

		public virtual long? RProductCategoryId { get; set; }
		
        [ForeignKey("RProductCategoryId")]
		public RProductCategory RProductCategoryFk { get; set; }
		
    }
}