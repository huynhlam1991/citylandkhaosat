using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;

namespace RASP.AbpTemplate.RModel
{
	[Table("RCategories")]
    [Audited]
    public class RCategory : FullAuditedEntity<long> 
    {

		[Required]
		[StringLength(RCategoryConsts.MaxMetaTitleLength, MinimumLength = RCategoryConsts.MinMetaTitleLength)]
		public virtual string MetaTitle { get; set; }
		
		[Required]
		public virtual string MetaDescription { get; set; }
		
		[StringLength(RCategoryConsts.MaxImageNameLength, MinimumLength = RCategoryConsts.MinImageNameLength)]
		public virtual string ImageName { get; set; }
		
		[Required]
		[StringLength(RCategoryConsts.MaxTitleLength, MinimumLength = RCategoryConsts.MinTitleLength)]
		public virtual string Title { get; set; }
		
		[Required]
		[StringLength(RCategoryConsts.MaxDescriptionLength, MinimumLength = RCategoryConsts.MinDescriptionLength)]
		public virtual string Description { get; set; }
		
		public virtual string Content { get; set; }
		
		public virtual string Tag { get; set; }
		
		[StringLength(RCategoryConsts.MaxAuthorLength, MinimumLength = RCategoryConsts.MinAuthorLength)]
		public virtual string Author { get; set; }
		
		public virtual long? ParentId { get; set; }
		
		public virtual bool IsAvailable { get; set; }
		
		[StringLength(RCategoryConsts.MaxTypeLength, MinimumLength = RCategoryConsts.MinTypeLength)]
		public virtual string Type { get; set; }
		
		public virtual bool IsShowMenuTop { get; set; }
		
		public virtual bool IsShowMenuBottom { get; set; }
		
		public virtual int Priority { get; set; }
		

    }
}