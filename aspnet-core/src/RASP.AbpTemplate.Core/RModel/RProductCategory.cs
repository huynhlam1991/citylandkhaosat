using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;

namespace RASP.AbpTemplate.RModel
{
	[Table("RProductCategories")]
    [Audited]
    public class RProductCategory : FullAuditedEntity<long> 
    {

		[StringLength(RProductCategoryConsts.MaxMetaTitleLength, MinimumLength = RProductCategoryConsts.MinMetaTitleLength)]
		public virtual string MetaTitle { get; set; }
		
		[StringLength(RProductCategoryConsts.MaxMetaDescriptionLength, MinimumLength = RProductCategoryConsts.MinMetaDescriptionLength)]
		public virtual string MetaDescription { get; set; }
		
		[StringLength(RProductCategoryConsts.MaxTitleLength, MinimumLength = RProductCategoryConsts.MinTitleLength)]
		public virtual string Title { get; set; }
		
		[StringLength(RProductCategoryConsts.MaxDescriptionLength, MinimumLength = RProductCategoryConsts.MinDescriptionLength)]
		public virtual string Description { get; set; }
		
		public virtual string Content { get; set; }
		
		public virtual string Tag { get; set; }
		
		public virtual long? ParentId { get; set; }
		
		public virtual bool IsAvailable { get; set; }
		
		public virtual bool IsShowMenuTop { get; set; }
		
		public virtual bool IsShowMenuBottom { get; set; }
		
		public virtual int Priority { get; set; }
		

    }
}