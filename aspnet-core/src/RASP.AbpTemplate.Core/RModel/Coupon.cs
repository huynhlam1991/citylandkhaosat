using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace RASP.AbpTemplate.RModel
{
	[Table("Coupons")]
    public class Coupon : FullAuditedEntity<long> 
    {

		[Required]
		[StringLength(CouponConsts.MaxTitleLength, MinimumLength = CouponConsts.MinTitleLength)]
		public virtual string Title { get; set; }
		
		[StringLength(CouponConsts.MaxDescriptionLength, MinimumLength = CouponConsts.MinDescriptionLength)]
		public virtual string Description { get; set; }
		
		public virtual string Content { get; set; }
		
		[Required]
		public virtual string ValueCoupon { get; set; }
		
		public virtual DateTime? TimeStart { get; set; }
		
		public virtual DateTime? TimeEnd { get; set; }
		
		public virtual bool IsCountDownToEnd { get; set; }
		
		public virtual bool IsCountDownToStart { get; set; }
		
		public virtual bool IsAvailable { get; set; }
		
		[StringLength(CouponConsts.MaxAuthorLength, MinimumLength = CouponConsts.MinAuthorLength)]
		public virtual string Author { get; set; }
		
		public virtual bool IsShowCarousel { get; set; }
		
		[StringLength(CouponConsts.MaxImageNameLength, MinimumLength = CouponConsts.MinImageNameLength)]
		public virtual string ImageName { get; set; }
		
		public virtual long Hit { get; set; }
		

    }
}