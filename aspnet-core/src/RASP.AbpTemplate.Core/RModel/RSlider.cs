using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace RASP.AbpTemplate.RModel
{
	[Table("RSliders")]
    public class RSlider : FullAuditedEntity 
    {

		[Required]
		[StringLength(RSliderConsts.MaxTitleLength, MinimumLength = RSliderConsts.MinTitleLength)]
		public virtual string Title { get; set; }
		
		[Required]
		[StringLength(RSliderConsts.MaxFileNameLength, MinimumLength = RSliderConsts.MinFileNameLength)]
		public virtual string FileName { get; set; }
		
		public virtual string Link { get; set; }
		
		[StringLength(RSliderConsts.MaxDescriptionLength, MinimumLength = RSliderConsts.MinDescriptionLength)]
		public virtual string Description { get; set; }
		
		public virtual bool IsAvailable { get; set; }
		
		[StringLength(RSliderConsts.MaxPositionCodeLength, MinimumLength = RSliderConsts.MinPositionCodeLength)]
		public virtual string PositionCode { get; set; }
		
		public virtual int Priority { get; set; }
		

    }
}