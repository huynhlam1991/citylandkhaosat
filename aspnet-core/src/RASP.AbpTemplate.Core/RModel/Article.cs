using RASP.AbpTemplate.RModel;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;

namespace RASP.AbpTemplate.RModel
{
	[Table("Articles")]
    [Audited]
    public class Article : FullAuditedEntity<long> 
    {

		[Required]
		[StringLength(ArticleConsts.MaxMetaTitleLength, MinimumLength = ArticleConsts.MinMetaTitleLength)]
		public virtual string MetaTitle { get; set; }
		
		[Required]
		public virtual string MetaDescription { get; set; }
		
		[StringLength(ArticleConsts.MaxImageNameLength, MinimumLength = ArticleConsts.MinImageNameLength)]
		public virtual string ImageName { get; set; }
		
		public virtual string Tag { get; set; }
		
		[Required]
		[StringLength(ArticleConsts.MaxTitleLength, MinimumLength = ArticleConsts.MinTitleLength)]
		public virtual string Title { get; set; }
		
		[Required]
		[StringLength(ArticleConsts.MaxDescriptionLength, MinimumLength = ArticleConsts.MinDescriptionLength)]
		public virtual string Description { get; set; }
		
		public virtual string Content { get; set; }
		
		[StringLength(ArticleConsts.MaxAuthorLength, MinimumLength = ArticleConsts.MinAuthorLength)]
		public virtual string Author { get; set; }
		
		public virtual bool IsAvailable { get; set; }
		
		public virtual string IsHot { get; set; }
		
		public virtual string IsNew { get; set; }
		
		public virtual int Priority { get; set; }
		
		public virtual long Hit { get; set; }
		

		public virtual long? RCategoryId { get; set; }
		
        [ForeignKey("RCategoryId")]
		public RCategory RCategoryFk { get; set; }
		
    }
}