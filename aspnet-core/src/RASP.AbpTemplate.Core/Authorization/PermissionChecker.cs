﻿using Abp.Authorization;
using RASP.AbpTemplate.Authorization.Roles;
using RASP.AbpTemplate.Authorization.Users;

namespace RASP.AbpTemplate.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
