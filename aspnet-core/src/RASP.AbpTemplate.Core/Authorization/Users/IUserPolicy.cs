﻿using System.Threading.Tasks;
using Abp.Domain.Policies;

namespace RASP.AbpTemplate.Authorization.Users
{
    public interface IUserPolicy : IPolicy
    {
        Task CheckMaxUserCountAsync(int tenantId);
    }
}
