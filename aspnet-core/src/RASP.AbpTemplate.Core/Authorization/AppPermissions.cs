namespace RASP.AbpTemplate.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        public const string Pages_Administration_RMailCustomers = "Pages.Administration.RMailCustomers";
        public const string Pages_Administration_RMailCustomers_Create = "Pages.Administration.RMailCustomers.Create";
        public const string Pages_Administration_RMailCustomers_Edit = "Pages.Administration.RMailCustomers.Edit";
        public const string Pages_Administration_RMailCustomers_Delete = "Pages.Administration.RMailCustomers.Delete";

        public const string Pages_Administration_SurveyResults = "Pages.Administration.SurveyResults";
        public const string Pages_Administration_SurveyResults_Create = "Pages.Administration.SurveyResults.Create";
        public const string Pages_Administration_SurveyResults_Edit = "Pages.Administration.SurveyResults.Edit";
        public const string Pages_Administration_SurveyResults_Delete = "Pages.Administration.SurveyResults.Delete";

        public const string Pages_Administration_PersonAnswers = "Pages.Administration.PersonAnswers";
        public const string Pages_Administration_PersonAnswers_Create = "Pages.Administration.PersonAnswers.Create";
        public const string Pages_Administration_PersonAnswers_Edit = "Pages.Administration.PersonAnswers.Edit";
        public const string Pages_Administration_PersonAnswers_Delete = "Pages.Administration.PersonAnswers.Delete";

        public const string Pages_Administration_Answers = "Pages.Administration.Answers";
        public const string Pages_Administration_Answers_Create = "Pages.Administration.Answers.Create";
        public const string Pages_Administration_Answers_Edit = "Pages.Administration.Answers.Edit";
        public const string Pages_Administration_Answers_Delete = "Pages.Administration.Answers.Delete";

        public const string Pages_Administration_Questions = "Pages.Administration.Questions";
        public const string Pages_Administration_Questions_Create = "Pages.Administration.Questions.Create";
        public const string Pages_Administration_Questions_Edit = "Pages.Administration.Questions.Edit";
        public const string Pages_Administration_Questions_Delete = "Pages.Administration.Questions.Delete";

        public const string Pages_Administration_Surveies = "Pages.Administration.Surveies";
        public const string Pages_Administration_Surveies_Create = "Pages.Administration.Surveies.Create";
        public const string Pages_Administration_Surveies_Edit = "Pages.Administration.Surveies.Edit";
        public const string Pages_Administration_Surveies_Delete = "Pages.Administration.Surveies.Delete";

        public const string Pages_Administration_RProductCategories = "Pages.Administration.RProductCategories";
        public const string Pages_Administration_RProductCategories_Create = "Pages.Administration.RProductCategories.Create";
        public const string Pages_Administration_RProductCategories_Edit = "Pages.Administration.RProductCategories.Edit";
        public const string Pages_Administration_RProductCategories_Delete = "Pages.Administration.RProductCategories.Delete";

        public const string Pages_Administration_Products = "Pages.Administration.Products";
        public const string Pages_Administration_Products_Create = "Pages.Administration.Products.Create";
        public const string Pages_Administration_Products_Edit = "Pages.Administration.Products.Edit";
        public const string Pages_Administration_Products_Delete = "Pages.Administration.Products.Delete";

        public const string Pages_Administration_Coupons = "Pages.Administration.Coupons";
        public const string Pages_Administration_Coupons_Create = "Pages.Administration.Coupons.Create";
        public const string Pages_Administration_Coupons_Edit = "Pages.Administration.Coupons.Edit";
        public const string Pages_Administration_Coupons_Delete = "Pages.Administration.Coupons.Delete";

        public const string Pages_Administration_RSliders = "Pages.Administration.RSliders";
        public const string Pages_Administration_RSliders_Create = "Pages.Administration.RSliders.Create";
        public const string Pages_Administration_RSliders_Edit = "Pages.Administration.RSliders.Edit";
        public const string Pages_Administration_RSliders_Delete = "Pages.Administration.RSliders.Delete";

        public const string Pages_Administration_Articles = "Pages.Administration.Articles";
        public const string Pages_Administration_Articles_Create = "Pages.Administration.Articles.Create";
        public const string Pages_Administration_Articles_Edit = "Pages.Administration.Articles.Edit";
        public const string Pages_Administration_Articles_Delete = "Pages.Administration.Articles.Delete";

        public const string Pages_Administration_RCategories = "Pages.Administration.RCategories";
        public const string Pages_Administration_RCategories_Create = "Pages.Administration.RCategories.Create";
        public const string Pages_Administration_RCategories_Edit = "Pages.Administration.RCategories.Edit";
        public const string Pages_Administration_RCategories_Delete = "Pages.Administration.RCategories.Delete";

        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";

        public const string Pages_DemoUiComponents= "Pages.DemoUiComponents";
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";
        public const string Pages_Administration_Users_Unlock = "Pages.Administration.Users.Unlock";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";
        public const string Pages_Administration_OrganizationUnits_ManageRoles = "Pages.Administration.OrganizationUnits.ManageRoles";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        public const string Pages_Administration_UiCustomization = "Pages.Administration.UiCustomization";

        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Administration_Tenant_SubscriptionManagement = "Pages.Administration.Tenant.SubscriptionManagement";

        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";
        public const string Pages_Editions_MoveTenantsToAnotherEdition = "Pages.Editions.MoveTenantsToAnotherEdition";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
        public const string Pages_Administration_Host_Dashboard = "Pages.Administration.Host.Dashboard";

    }
}