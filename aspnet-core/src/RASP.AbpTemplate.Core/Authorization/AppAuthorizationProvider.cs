using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace RASP.AbpTemplate.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        private readonly bool _isMultiTenancyEnabled;

        public AppAuthorizationProvider(bool isMultiTenancyEnabled)
        {
            _isMultiTenancyEnabled = isMultiTenancyEnabled;
        }

        public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
        {
            _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
        }

        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));
            pages.CreateChildPermission(AppPermissions.Pages_DemoUiComponents, L("DemoUiComponents"));

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

            var rMailCustomers = administration.CreateChildPermission(AppPermissions.Pages_Administration_RMailCustomers, L("RMailCustomers"));
            rMailCustomers.CreateChildPermission(AppPermissions.Pages_Administration_RMailCustomers_Create, L("CreateNewRMailCustomer"));
            rMailCustomers.CreateChildPermission(AppPermissions.Pages_Administration_RMailCustomers_Edit, L("EditRMailCustomer"));
            rMailCustomers.CreateChildPermission(AppPermissions.Pages_Administration_RMailCustomers_Delete, L("DeleteRMailCustomer"));



            var surveyResults = administration.CreateChildPermission(AppPermissions.Pages_Administration_SurveyResults, L("SurveyResults"));
            surveyResults.CreateChildPermission(AppPermissions.Pages_Administration_SurveyResults_Create, L("CreateNewSurveyResult"));
            surveyResults.CreateChildPermission(AppPermissions.Pages_Administration_SurveyResults_Edit, L("EditSurveyResult"));
            surveyResults.CreateChildPermission(AppPermissions.Pages_Administration_SurveyResults_Delete, L("DeleteSurveyResult"));



            var personAnswers = administration.CreateChildPermission(AppPermissions.Pages_Administration_PersonAnswers, L("PersonAnswers"));
            personAnswers.CreateChildPermission(AppPermissions.Pages_Administration_PersonAnswers_Create, L("CreateNewPersonAnswer"));
            personAnswers.CreateChildPermission(AppPermissions.Pages_Administration_PersonAnswers_Edit, L("EditPersonAnswer"));
            personAnswers.CreateChildPermission(AppPermissions.Pages_Administration_PersonAnswers_Delete, L("DeletePersonAnswer"));



            var answers = administration.CreateChildPermission(AppPermissions.Pages_Administration_Answers, L("Answers"));
            answers.CreateChildPermission(AppPermissions.Pages_Administration_Answers_Create, L("CreateNewAnswer"));
            answers.CreateChildPermission(AppPermissions.Pages_Administration_Answers_Edit, L("EditAnswer"));
            answers.CreateChildPermission(AppPermissions.Pages_Administration_Answers_Delete, L("DeleteAnswer"));



            var questions = administration.CreateChildPermission(AppPermissions.Pages_Administration_Questions, L("Questions"));
            questions.CreateChildPermission(AppPermissions.Pages_Administration_Questions_Create, L("CreateNewQuestion"));
            questions.CreateChildPermission(AppPermissions.Pages_Administration_Questions_Edit, L("EditQuestion"));
            questions.CreateChildPermission(AppPermissions.Pages_Administration_Questions_Delete, L("DeleteQuestion"));



            var surveies = administration.CreateChildPermission(AppPermissions.Pages_Administration_Surveies, L("Surveies"));
            surveies.CreateChildPermission(AppPermissions.Pages_Administration_Surveies_Create, L("CreateNewSurvey"));
            surveies.CreateChildPermission(AppPermissions.Pages_Administration_Surveies_Edit, L("EditSurvey"));
            surveies.CreateChildPermission(AppPermissions.Pages_Administration_Surveies_Delete, L("DeleteSurvey"));



            var rProductCategories = administration.CreateChildPermission(AppPermissions.Pages_Administration_RProductCategories, L("RProductCategories"));
            rProductCategories.CreateChildPermission(AppPermissions.Pages_Administration_RProductCategories_Create, L("CreateNewRProductCategory"));
            rProductCategories.CreateChildPermission(AppPermissions.Pages_Administration_RProductCategories_Edit, L("EditRProductCategory"));
            rProductCategories.CreateChildPermission(AppPermissions.Pages_Administration_RProductCategories_Delete, L("DeleteRProductCategory"));



            var products = administration.CreateChildPermission(AppPermissions.Pages_Administration_Products, L("Products"));
            products.CreateChildPermission(AppPermissions.Pages_Administration_Products_Create, L("CreateNewProduct"));
            products.CreateChildPermission(AppPermissions.Pages_Administration_Products_Edit, L("EditProduct"));
            products.CreateChildPermission(AppPermissions.Pages_Administration_Products_Delete, L("DeleteProduct"));



            var coupons = administration.CreateChildPermission(AppPermissions.Pages_Administration_Coupons, L("Coupons"));
            coupons.CreateChildPermission(AppPermissions.Pages_Administration_Coupons_Create, L("CreateNewCoupon"));
            coupons.CreateChildPermission(AppPermissions.Pages_Administration_Coupons_Edit, L("EditCoupon"));
            coupons.CreateChildPermission(AppPermissions.Pages_Administration_Coupons_Delete, L("DeleteCoupon"));



            var rSliders = administration.CreateChildPermission(AppPermissions.Pages_Administration_RSliders, L("RSliders"));
            rSliders.CreateChildPermission(AppPermissions.Pages_Administration_RSliders_Create, L("CreateNewRSlider"));
            rSliders.CreateChildPermission(AppPermissions.Pages_Administration_RSliders_Edit, L("EditRSlider"));
            rSliders.CreateChildPermission(AppPermissions.Pages_Administration_RSliders_Delete, L("DeleteRSlider"));



            var articles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Articles, L("Articles"));
            articles.CreateChildPermission(AppPermissions.Pages_Administration_Articles_Create, L("CreateNewArticle"));
            articles.CreateChildPermission(AppPermissions.Pages_Administration_Articles_Edit, L("EditArticle"));
            articles.CreateChildPermission(AppPermissions.Pages_Administration_Articles_Delete, L("DeleteArticle"));



            var rCategories = administration.CreateChildPermission(AppPermissions.Pages_Administration_RCategories, L("RCategories"));
            rCategories.CreateChildPermission(AppPermissions.Pages_Administration_RCategories_Create, L("CreateNewRCategory"));
            rCategories.CreateChildPermission(AppPermissions.Pages_Administration_RCategories_Edit, L("EditRCategory"));
            rCategories.CreateChildPermission(AppPermissions.Pages_Administration_RCategories_Delete, L("DeleteRCategory"));



            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Unlock, L("Unlock"));

            var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

            var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles, L("ManagingRoles"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization, L("VisualSettings"));

            //TENANT-SPECIFIC PERMISSIONS

            pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement, L("Subscription"), multiTenancySides: MultiTenancySides.Tenant);

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_MoveTenantsToAnotherEdition, L("MoveTenantsToAnotherEdition"), multiTenancySides: MultiTenancySides.Host); 

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Host);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, AbpTemplateConsts.LocalizationSourceName);
        }
    }
}
