﻿using System.Globalization;

namespace RASP.AbpTemplate.Localization
{
    public interface IApplicationCulturesProvider
    {
        CultureInfo[] GetAllCultures();
    }
}