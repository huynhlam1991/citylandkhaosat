﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using RASP.AbpTemplate.EntityFrameworkCore;

namespace RASP.AbpTemplate.HealthChecks
{
    public class AbpTemplateDbContextHealthCheck : IHealthCheck
    {
        private readonly DatabaseCheckHelper _checkHelper;

        public AbpTemplateDbContextHealthCheck(DatabaseCheckHelper checkHelper)
        {
            _checkHelper = checkHelper;
        }

        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            if (_checkHelper.Exist("db"))
            {
                return Task.FromResult(HealthCheckResult.Healthy("AbpTemplateDbContext connected to database."));
            }

            return Task.FromResult(HealthCheckResult.Unhealthy("AbpTemplateDbContext could not connect to database"));
        }
    }
}
