﻿using System.Collections.Generic;
using RASP.AbpTemplate.Authorization.Users.Dto;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}