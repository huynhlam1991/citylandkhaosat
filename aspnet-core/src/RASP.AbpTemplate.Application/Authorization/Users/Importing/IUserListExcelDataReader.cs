﻿using System.Collections.Generic;
using RASP.AbpTemplate.Authorization.Users.Importing.Dto;
using Abp.Dependency;

namespace RASP.AbpTemplate.Authorization.Users.Importing
{
    public interface IUserListExcelDataReader: ITransientDependency
    {
        List<ImportUserDto> GetUsersFromExcel(byte[] fileBytes);
    }
}
