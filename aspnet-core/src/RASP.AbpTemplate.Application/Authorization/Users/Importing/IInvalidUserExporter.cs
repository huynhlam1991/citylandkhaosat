﻿using System.Collections.Generic;
using RASP.AbpTemplate.Authorization.Users.Importing.Dto;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.Authorization.Users.Importing
{
    public interface IInvalidUserExporter
    {
        FileDto ExportToFile(List<ImportUserDto> userListDtos);
    }
}
