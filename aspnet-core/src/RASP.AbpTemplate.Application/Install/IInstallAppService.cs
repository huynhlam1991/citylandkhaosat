﻿using System.Threading.Tasks;
using Abp.Application.Services;
using RASP.AbpTemplate.Install.Dto;

namespace RASP.AbpTemplate.Install
{
    public interface IInstallAppService : IApplicationService
    {
        Task Setup(InstallDto input);

        AppSettingsJsonDto GetAppSettingsJson();

        CheckDatabaseOutput CheckDatabase();
    }
}