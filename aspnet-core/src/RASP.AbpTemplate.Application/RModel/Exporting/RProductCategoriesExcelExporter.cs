using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using RASP.AbpTemplate.DataExporting.Excel.EpPlus;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;
using RASP.AbpTemplate.Storage;

namespace RASP.AbpTemplate.RModel.Exporting
{
    public class RProductCategoriesExcelExporter : EpPlusExcelExporterBase, IRProductCategoriesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RProductCategoriesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRProductCategoryForViewDto> rProductCategories)
        {
            return CreateExcelPackage(
                "RProductCategories.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("RProductCategories"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("MetaTitle"),
                        L("MetaDescription"),
                        L("Title"),
                        L("Description"),
                        L("IsAvailable"),
                        L("IsShowMenuTop"),
                        L("IsShowMenuBottom"),
                        L("Priority")
                        );

                    AddObjects(
                        sheet, 2, rProductCategories,
                        _ => _.RProductCategory.MetaTitle,
                        _ => _.RProductCategory.MetaDescription,
                        _ => _.RProductCategory.Title,
                        _ => _.RProductCategory.Description,
                        _ => _.RProductCategory.IsAvailable,
                        _ => _.RProductCategory.IsShowMenuTop,
                        _ => _.RProductCategory.IsShowMenuBottom,
                        _ => _.RProductCategory.Priority
                        );

					

                });
        }
    }
}
