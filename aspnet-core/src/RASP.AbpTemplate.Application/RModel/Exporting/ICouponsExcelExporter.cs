using System.Collections.Generic;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RModel.Exporting
{
    public interface ICouponsExcelExporter
    {
        FileDto ExportToFile(List<GetCouponForViewDto> coupons);
    }
}