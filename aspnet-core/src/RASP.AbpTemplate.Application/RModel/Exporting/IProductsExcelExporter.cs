using System.Collections.Generic;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RModel.Exporting
{
    public interface IProductsExcelExporter
    {
        FileDto ExportToFile(List<GetProductForViewDto> products);
    }
}