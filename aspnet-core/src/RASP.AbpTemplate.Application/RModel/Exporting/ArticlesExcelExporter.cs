using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using RASP.AbpTemplate.DataExporting.Excel.EpPlus;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;
using RASP.AbpTemplate.Storage;

namespace RASP.AbpTemplate.RModel.Exporting
{
    public class ArticlesExcelExporter : EpPlusExcelExporterBase, IArticlesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ArticlesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetArticleForViewDto> articles)
        {
            return CreateExcelPackage(
                "Articles.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Articles"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("MetaTitle"),
                        L("MetaDescription"),
                        L("ImageName"),
                        L("Title"),
                        L("Author"),
                        L("IsAvailable"),
                        L("IsHot"),
                        L("IsNew"),
                        L("Priority"),
                        L("Hit"),
                        (L("RCategory")) + L("Title")
                        );

                    AddObjects(
                        sheet, 2, articles,
                        _ => _.Article.MetaTitle,
                        _ => _.Article.MetaDescription,
                        _ => _.Article.ImageName,
                        _ => _.Article.Title,
                        _ => _.Article.Author,
                        _ => _.Article.IsAvailable,
                        _ => _.Article.IsHot,
                        _ => _.Article.IsNew,
                        _ => _.Article.Priority,
                        _ => _.Article.Hit,
                        _ => _.RCategoryTitle
                        );

					

                });
        }
    }
}
