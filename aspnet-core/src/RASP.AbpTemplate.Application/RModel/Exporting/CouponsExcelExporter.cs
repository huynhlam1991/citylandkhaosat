using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using RASP.AbpTemplate.DataExporting.Excel.EpPlus;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;
using RASP.AbpTemplate.Storage;

namespace RASP.AbpTemplate.RModel.Exporting
{
    public class CouponsExcelExporter : EpPlusExcelExporterBase, ICouponsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public CouponsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetCouponForViewDto> coupons)
        {
            return CreateExcelPackage(
                "Coupons.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Coupons"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Title"),
                        L("Description"),
                        L("Content"),
                        L("ValueCoupon"),
                        L("TimeStart"),
                        L("TimeEnd"),
                        L("IsCountDownToEnd"),
                        L("IsCountDownToStart"),
                        L("IsAvailable"),
                        L("Author"),
                        L("IsShowCarousel"),
                        L("ImageName"),
                        L("Hit")
                        );

                    AddObjects(
                        sheet, 2, coupons,
                        _ => _.Coupon.Title,
                        _ => _.Coupon.Description,
                        _ => _.Coupon.Content,
                        _ => _.Coupon.ValueCoupon,
                        _ => _timeZoneConverter.Convert(_.Coupon.TimeStart, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _timeZoneConverter.Convert(_.Coupon.TimeEnd, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.Coupon.IsCountDownToEnd,
                        _ => _.Coupon.IsCountDownToStart,
                        _ => _.Coupon.IsAvailable,
                        _ => _.Coupon.Author,
                        _ => _.Coupon.IsShowCarousel,
                        _ => _.Coupon.ImageName,
                        _ => _.Coupon.Hit
                        );

					var timeStartColumn = sheet.Column(5);
                    timeStartColumn.Style.Numberformat.Format = "yyyy-mm-dd";
					timeStartColumn.AutoFit();
					var timeEndColumn = sheet.Column(6);
                    timeEndColumn.Style.Numberformat.Format = "yyyy-mm-dd";
					timeEndColumn.AutoFit();
					

                });
        }
    }
}
