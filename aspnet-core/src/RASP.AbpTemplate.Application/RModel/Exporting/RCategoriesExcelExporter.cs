using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using RASP.AbpTemplate.DataExporting.Excel.EpPlus;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;
using RASP.AbpTemplate.Storage;

namespace RASP.AbpTemplate.RModel.Exporting
{
    public class RCategoriesExcelExporter : EpPlusExcelExporterBase, IRCategoriesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RCategoriesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRCategoryForViewDto> rCategories)
        {
            return CreateExcelPackage(
                "RCategories.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("RCategories"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("MetaTitle"),
                        L("Title"),
                        L("Description"),
                        L("Tag"),
                        L("Author"),
                        L("ParentId"),
                        L("IsAvailable"),
                        L("IsShowMenuTop"),
                        L("IsShowMenuBottom"),
                        L("Priority")
                        );

                    AddObjects(
                        sheet, 2, rCategories,
                        _ => _.RCategory.MetaTitle,
                        _ => _.RCategory.Title,
                        _ => _.RCategory.Description,
                        _ => _.RCategory.Tag,
                        _ => _.RCategory.Author,
                        _ => _.RCategory.ParentId,
                        _ => _.RCategory.IsAvailable,
                        _ => _.RCategory.IsShowMenuTop,
                        _ => _.RCategory.IsShowMenuBottom,
                        _ => _.RCategory.Priority
                        );

					

                });
        }
    }
}
