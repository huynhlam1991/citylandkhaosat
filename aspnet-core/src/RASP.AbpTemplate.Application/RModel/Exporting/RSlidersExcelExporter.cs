using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using RASP.AbpTemplate.DataExporting.Excel.EpPlus;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;
using RASP.AbpTemplate.Storage;

namespace RASP.AbpTemplate.RModel.Exporting
{
    public class RSlidersExcelExporter : EpPlusExcelExporterBase, IRSlidersExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RSlidersExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRSliderForViewDto> rSliders)
        {
            return CreateExcelPackage(
                "RSliders.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("RSliders"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Title"),
                        L("FileName"),
                        L("Description"),
                        L("IsAvailable"),
                        L("PositionCode"),
                        L("Priority")
                        );

                    AddObjects(
                        sheet, 2, rSliders,
                        _ => _.RSlider.Title,
                        _ => _.RSlider.FileName,
                        _ => _.RSlider.Description,
                        _ => _.RSlider.IsAvailable,
                        _ => _.RSlider.PositionCode,
                        _ => _.RSlider.Priority
                        );

					

                });
        }
    }
}
