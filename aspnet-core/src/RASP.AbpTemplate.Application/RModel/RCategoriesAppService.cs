

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using RASP.AbpTemplate.RModel.Exporting;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace RASP.AbpTemplate.RModel
{
	[AbpAuthorize(AppPermissions.Pages_Administration_RCategories)]
    public class RCategoriesAppService : AbpTemplateAppServiceBase, IRCategoriesAppService
    {
		 private readonly IRepository<RCategory, long> _rCategoryRepository;
		 private readonly IRCategoriesExcelExporter _rCategoriesExcelExporter;
		 

		  public RCategoriesAppService(IRepository<RCategory, long> rCategoryRepository, IRCategoriesExcelExporter rCategoriesExcelExporter ) 
		  {
			_rCategoryRepository = rCategoryRepository;
			_rCategoriesExcelExporter = rCategoriesExcelExporter;
			
		  }

		 public async Task<PagedResultDto<GetRCategoryForViewDto>> GetAll(GetAllRCategoriesInput input)
         {
			
			var filteredRCategories = _rCategoryRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.MetaTitle.Contains(input.Filter) || e.MetaDescription.Contains(input.Filter) || e.ImageName.Contains(input.Filter) || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Content.Contains(input.Filter) || e.Tag.Contains(input.Filter) || e.Author.Contains(input.Filter) || e.Type.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.MetaTitleFilter),  e => e.MetaTitle.ToLower() == input.MetaTitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.MetaDescriptionFilter),  e => e.MetaDescription.ToLower() == input.MetaDescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter),  e => e.Title.ToLower() == input.TitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter),  e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ContentFilter),  e => e.Content.ToLower() == input.ContentFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.TagFilter),  e => e.Tag.ToLower() == input.TagFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.AuthorFilter),  e => e.Author.ToLower() == input.AuthorFilter.ToLower().Trim())
						.WhereIf(input.MinParentIdFilter != null, e => e.ParentId >= input.MinParentIdFilter)
						.WhereIf(input.MaxParentIdFilter != null, e => e.ParentId <= input.MaxParentIdFilter)
						.WhereIf(input.IsAvailableFilter > -1,  e => Convert.ToInt32(e.IsAvailable) == input.IsAvailableFilter )
						.WhereIf(input.IsShowMenuTopFilter > -1,  e => Convert.ToInt32(e.IsShowMenuTop) == input.IsShowMenuTopFilter )
						.WhereIf(input.IsShowMenuBottomFilter > -1,  e => Convert.ToInt32(e.IsShowMenuBottom) == input.IsShowMenuBottomFilter )
						.WhereIf(input.MinPriorityFilter != null, e => e.Priority >= input.MinPriorityFilter)
						.WhereIf(input.MaxPriorityFilter != null, e => e.Priority <= input.MaxPriorityFilter);

			var pagedAndFilteredRCategories = filteredRCategories
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

			var rCategories = from o in pagedAndFilteredRCategories
                         select new GetRCategoryForViewDto() {
							RCategory = new RCategoryDto
							{
                                MetaTitle = o.MetaTitle,
                                Title = o.Title,
                                Description = o.Description,
                                Tag = o.Tag,
                                Author = o.Author,
                                ParentId = o.ParentId,
                                IsAvailable = o.IsAvailable,
                                IsShowMenuTop = o.IsShowMenuTop,
                                IsShowMenuBottom = o.IsShowMenuBottom,
                                Priority = o.Priority,
                                Id = o.Id
							}
						};

            var totalCount = await filteredRCategories.CountAsync();

            return new PagedResultDto<GetRCategoryForViewDto>(
                totalCount,
                await rCategories.ToListAsync()
            );
         }
		 
		 public async Task<GetRCategoryForViewDto> GetRCategoryForView(long id)
         {
            var rCategory = await _rCategoryRepository.GetAsync(id);

            var output = new GetRCategoryForViewDto { RCategory = ObjectMapper.Map<RCategoryDto>(rCategory) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Administration_RCategories_Edit)]
		 public async Task<GetRCategoryForEditOutput> GetRCategoryForEdit(EntityDto<long> input)
         {
            var rCategory = await _rCategoryRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetRCategoryForEditOutput {RCategory = ObjectMapper.Map<CreateOrEditRCategoryDto>(rCategory)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditRCategoryDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_RCategories_Create)]
		 protected virtual async Task Create(CreateOrEditRCategoryDto input)
         {
            var rCategory = ObjectMapper.Map<RCategory>(input);

			

            await _rCategoryRepository.InsertAsync(rCategory);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_RCategories_Edit)]
		 protected virtual async Task Update(CreateOrEditRCategoryDto input)
         {
            var rCategory = await _rCategoryRepository.FirstOrDefaultAsync((long)input.Id);
             ObjectMapper.Map(input, rCategory);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_RCategories_Delete)]
         public async Task Delete(EntityDto<long> input)
         {
            await _rCategoryRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetRCategoriesToExcel(GetAllRCategoriesForExcelInput input)
         {
			
			var filteredRCategories = _rCategoryRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.MetaTitle.Contains(input.Filter) || e.MetaDescription.Contains(input.Filter) || e.ImageName.Contains(input.Filter) || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Content.Contains(input.Filter) || e.Tag.Contains(input.Filter) || e.Author.Contains(input.Filter) || e.Type.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.MetaTitleFilter),  e => e.MetaTitle.ToLower() == input.MetaTitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.MetaDescriptionFilter),  e => e.MetaDescription.ToLower() == input.MetaDescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter),  e => e.Title.ToLower() == input.TitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter),  e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ContentFilter),  e => e.Content.ToLower() == input.ContentFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.TagFilter),  e => e.Tag.ToLower() == input.TagFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.AuthorFilter),  e => e.Author.ToLower() == input.AuthorFilter.ToLower().Trim())
						.WhereIf(input.MinParentIdFilter != null, e => e.ParentId >= input.MinParentIdFilter)
						.WhereIf(input.MaxParentIdFilter != null, e => e.ParentId <= input.MaxParentIdFilter)
						.WhereIf(input.IsAvailableFilter > -1,  e => Convert.ToInt32(e.IsAvailable) == input.IsAvailableFilter )
						.WhereIf(input.IsShowMenuTopFilter > -1,  e => Convert.ToInt32(e.IsShowMenuTop) == input.IsShowMenuTopFilter )
						.WhereIf(input.IsShowMenuBottomFilter > -1,  e => Convert.ToInt32(e.IsShowMenuBottom) == input.IsShowMenuBottomFilter )
						.WhereIf(input.MinPriorityFilter != null, e => e.Priority >= input.MinPriorityFilter)
						.WhereIf(input.MaxPriorityFilter != null, e => e.Priority <= input.MaxPriorityFilter);

			var query = (from o in filteredRCategories
                         select new GetRCategoryForViewDto() { 
							RCategory = new RCategoryDto
							{
                                MetaTitle = o.MetaTitle,
                                Title = o.Title,
                                Description = o.Description,
                                Tag = o.Tag,
                                Author = o.Author,
                                ParentId = o.ParentId,
                                IsAvailable = o.IsAvailable,
                                IsShowMenuTop = o.IsShowMenuTop,
                                IsShowMenuBottom = o.IsShowMenuBottom,
                                Priority = o.Priority,
                                Id = o.Id
							}
						 });


            var rCategoryListDtos = await query.ToListAsync();

            return _rCategoriesExcelExporter.ExportToFile(rCategoryListDtos);
         }


    }
}