

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using RASP.AbpTemplate.RModel.Exporting;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace RASP.AbpTemplate.RModel
{
	[AbpAuthorize(AppPermissions.Pages_Administration_RProductCategories)]
    public class RProductCategoriesAppService : AbpTemplateAppServiceBase, IRProductCategoriesAppService
    {
		 private readonly IRepository<RProductCategory, long> _rProductCategoryRepository;
		 private readonly IRProductCategoriesExcelExporter _rProductCategoriesExcelExporter;
		 

		  public RProductCategoriesAppService(IRepository<RProductCategory, long> rProductCategoryRepository, IRProductCategoriesExcelExporter rProductCategoriesExcelExporter ) 
		  {
			_rProductCategoryRepository = rProductCategoryRepository;
			_rProductCategoriesExcelExporter = rProductCategoriesExcelExporter;
			
		  }

		 public async Task<PagedResultDto<GetRProductCategoryForViewDto>> GetAll(GetAllRProductCategoriesInput input)
         {
			
			var filteredRProductCategories = _rProductCategoryRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.MetaTitle.Contains(input.Filter) || e.MetaDescription.Contains(input.Filter) || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Content.Contains(input.Filter) || e.Tag.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.MetaTitleFilter),  e => e.MetaTitle.ToLower() == input.MetaTitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.MetaDescriptionFilter),  e => e.MetaDescription.ToLower() == input.MetaDescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter),  e => e.Title.ToLower() == input.TitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter),  e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ContentFilter),  e => e.Content.ToLower() == input.ContentFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.TagFilter),  e => e.Tag.ToLower() == input.TagFilter.ToLower().Trim())
						.WhereIf(input.MinParentIdFilter != null, e => e.ParentId >= input.MinParentIdFilter)
						.WhereIf(input.MaxParentIdFilter != null, e => e.ParentId <= input.MaxParentIdFilter)
						.WhereIf(input.IsAvailableFilter > -1,  e => Convert.ToInt32(e.IsAvailable) == input.IsAvailableFilter )
						.WhereIf(input.IsShowMenuTopFilter > -1,  e => Convert.ToInt32(e.IsShowMenuTop) == input.IsShowMenuTopFilter )
						.WhereIf(input.IsShowMenuBottomFilter > -1,  e => Convert.ToInt32(e.IsShowMenuBottom) == input.IsShowMenuBottomFilter )
						.WhereIf(input.MinPriorityFilter != null, e => e.Priority >= input.MinPriorityFilter)
						.WhereIf(input.MaxPriorityFilter != null, e => e.Priority <= input.MaxPriorityFilter);

			var pagedAndFilteredRProductCategories = filteredRProductCategories
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

			var rProductCategories = from o in pagedAndFilteredRProductCategories
                         select new GetRProductCategoryForViewDto() {
							RProductCategory = new RProductCategoryDto
							{
                                MetaTitle = o.MetaTitle,
                                MetaDescription = o.MetaDescription,
                                Title = o.Title,
                                Description = o.Description,
                                IsAvailable = o.IsAvailable,
                                IsShowMenuTop = o.IsShowMenuTop,
                                IsShowMenuBottom = o.IsShowMenuBottom,
                                Priority = o.Priority,
                                Id = o.Id
							}
						};

            var totalCount = await filteredRProductCategories.CountAsync();

            return new PagedResultDto<GetRProductCategoryForViewDto>(
                totalCount,
                await rProductCategories.ToListAsync()
            );
         }
		 
		 public async Task<GetRProductCategoryForViewDto> GetRProductCategoryForView(long id)
         {
            var rProductCategory = await _rProductCategoryRepository.GetAsync(id);

            var output = new GetRProductCategoryForViewDto { RProductCategory = ObjectMapper.Map<RProductCategoryDto>(rProductCategory) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Administration_RProductCategories_Edit)]
		 public async Task<GetRProductCategoryForEditOutput> GetRProductCategoryForEdit(EntityDto<long> input)
         {
            var rProductCategory = await _rProductCategoryRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetRProductCategoryForEditOutput {RProductCategory = ObjectMapper.Map<CreateOrEditRProductCategoryDto>(rProductCategory)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditRProductCategoryDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_RProductCategories_Create)]
		 protected virtual async Task Create(CreateOrEditRProductCategoryDto input)
         {
            var rProductCategory = ObjectMapper.Map<RProductCategory>(input);

			

            await _rProductCategoryRepository.InsertAsync(rProductCategory);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_RProductCategories_Edit)]
		 protected virtual async Task Update(CreateOrEditRProductCategoryDto input)
         {
            var rProductCategory = await _rProductCategoryRepository.FirstOrDefaultAsync((long)input.Id);
             ObjectMapper.Map(input, rProductCategory);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_RProductCategories_Delete)]
         public async Task Delete(EntityDto<long> input)
         {
            await _rProductCategoryRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetRProductCategoriesToExcel(GetAllRProductCategoriesForExcelInput input)
         {
			
			var filteredRProductCategories = _rProductCategoryRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.MetaTitle.Contains(input.Filter) || e.MetaDescription.Contains(input.Filter) || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Content.Contains(input.Filter) || e.Tag.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.MetaTitleFilter),  e => e.MetaTitle.ToLower() == input.MetaTitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.MetaDescriptionFilter),  e => e.MetaDescription.ToLower() == input.MetaDescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter),  e => e.Title.ToLower() == input.TitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter),  e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ContentFilter),  e => e.Content.ToLower() == input.ContentFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.TagFilter),  e => e.Tag.ToLower() == input.TagFilter.ToLower().Trim())
						.WhereIf(input.MinParentIdFilter != null, e => e.ParentId >= input.MinParentIdFilter)
						.WhereIf(input.MaxParentIdFilter != null, e => e.ParentId <= input.MaxParentIdFilter)
						.WhereIf(input.IsAvailableFilter > -1,  e => Convert.ToInt32(e.IsAvailable) == input.IsAvailableFilter )
						.WhereIf(input.IsShowMenuTopFilter > -1,  e => Convert.ToInt32(e.IsShowMenuTop) == input.IsShowMenuTopFilter )
						.WhereIf(input.IsShowMenuBottomFilter > -1,  e => Convert.ToInt32(e.IsShowMenuBottom) == input.IsShowMenuBottomFilter )
						.WhereIf(input.MinPriorityFilter != null, e => e.Priority >= input.MinPriorityFilter)
						.WhereIf(input.MaxPriorityFilter != null, e => e.Priority <= input.MaxPriorityFilter);

			var query = (from o in filteredRProductCategories
                         select new GetRProductCategoryForViewDto() { 
							RProductCategory = new RProductCategoryDto
							{
                                MetaTitle = o.MetaTitle,
                                MetaDescription = o.MetaDescription,
                                Title = o.Title,
                                Description = o.Description,
                                IsAvailable = o.IsAvailable,
                                IsShowMenuTop = o.IsShowMenuTop,
                                IsShowMenuBottom = o.IsShowMenuBottom,
                                Priority = o.Priority,
                                Id = o.Id
							}
						 });


            var rProductCategoryListDtos = await query.ToListAsync();

            return _rProductCategoriesExcelExporter.ExportToFile(rProductCategoryListDtos);
         }


    }
}