using RASP.AbpTemplate.RModel;


using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using RASP.AbpTemplate.RModel.Exporting;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace RASP.AbpTemplate.RModel
{
	[AbpAuthorize(AppPermissions.Pages_Administration_Products)]
    public class ProductsAppService : AbpTemplateAppServiceBase, IProductsAppService
    {
		 private readonly IRepository<Product, long> _productRepository;
		 private readonly IProductsExcelExporter _productsExcelExporter;
		 private readonly IRepository<RProductCategory,long> _lookup_rProductCategoryRepository;
		 

		  public ProductsAppService(IRepository<Product, long> productRepository, IProductsExcelExporter productsExcelExporter , IRepository<RProductCategory, long> lookup_rProductCategoryRepository) 
		  {
			_productRepository = productRepository;
			_productsExcelExporter = productsExcelExporter;
			_lookup_rProductCategoryRepository = lookup_rProductCategoryRepository;
		
		  }

		 public async Task<PagedResultDto<GetProductForViewDto>> GetAll(GetAllProductsInput input)
         {
			
			var filteredProducts = _productRepository.GetAll()
						.Include( e => e.RProductCategoryFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.MetaTitle.Contains(input.Filter) || e.MetaDescription.Contains(input.Filter) || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Content.Contains(input.Filter) || e.ImageName.Contains(input.Filter) || e.SaleMan.Contains(input.Filter) || e.LinkReference.Contains(input.Filter) || e.ImageList.Contains(input.Filter) || e.IsContact.Contains(input.Filter) || e.DisplayIsContact.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.MetaTitleFilter),  e => e.MetaTitle.ToLower() == input.MetaTitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.MetaDescriptionFilter),  e => e.MetaDescription.ToLower() == input.MetaDescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter),  e => e.Title.ToLower() == input.TitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter),  e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ContentFilter),  e => e.Content.ToLower() == input.ContentFilter.ToLower().Trim())
						.WhereIf(input.MinPriorityFilter != null, e => e.Priority >= input.MinPriorityFilter)
						.WhereIf(input.MaxPriorityFilter != null, e => e.Priority <= input.MaxPriorityFilter)
						.WhereIf(input.IsAvailableFilter > -1,  e => Convert.ToInt32(e.IsAvailable) == input.IsAvailableFilter )
						.WhereIf(input.IsHotFilter > -1,  e => Convert.ToInt32(e.IsHot) == input.IsHotFilter )
						.WhereIf(input.IsNewFilter > -1,  e => Convert.ToInt32(e.IsNew) == input.IsNewFilter )
						.WhereIf(input.MinPriceOriginFilter != null, e => e.PriceOrigin >= input.MinPriceOriginFilter)
						.WhereIf(input.MaxPriceOriginFilter != null, e => e.PriceOrigin <= input.MaxPriceOriginFilter)
						.WhereIf(input.MinPricePromotionFilter != null, e => e.PricePromotion >= input.MinPricePromotionFilter)
						.WhereIf(input.MaxPricePromotionFilter != null, e => e.PricePromotion <= input.MaxPricePromotionFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.SaleManFilter),  e => e.SaleMan.ToLower() == input.SaleManFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.LinkReferenceFilter),  e => e.LinkReference.ToLower() == input.LinkReferenceFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.IsContactFilter),  e => e.IsContact.ToLower() == input.IsContactFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.RProductCategoryTitleFilter), e => e.RProductCategoryFk != null && e.RProductCategoryFk.Title.ToLower() == input.RProductCategoryTitleFilter.ToLower().Trim());

			var pagedAndFilteredProducts = filteredProducts
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

			var products = from o in pagedAndFilteredProducts
                         join o1 in _lookup_rProductCategoryRepository.GetAll() on o.RProductCategoryId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetProductForViewDto() {
							Product = new ProductDto
							{
                                Title = o.Title,
                                Priority = o.Priority,
                                IsAvailable = o.IsAvailable,
                                IsHot = o.IsHot,
                                IsNew = o.IsNew,
                                ImageName = o.ImageName,
                                PriceOrigin = o.PriceOrigin,
                                PricePromotion = o.PricePromotion,
                                Hit = o.Hit,
                                Id = o.Id
							},
                         	RProductCategoryTitle = s1 == null ? "" : s1.Title.ToString()
						};

            var totalCount = await filteredProducts.CountAsync();

            return new PagedResultDto<GetProductForViewDto>(
                totalCount,
                await products.ToListAsync()
            );
         }
		 
		 public async Task<GetProductForViewDto> GetProductForView(long id)
         {
            var product = await _productRepository.GetAsync(id);

            var output = new GetProductForViewDto { Product = ObjectMapper.Map<ProductDto>(product) };

		    if (output.Product.RProductCategoryId != null)
            {
                var _lookupRProductCategory = await _lookup_rProductCategoryRepository.FirstOrDefaultAsync((long)output.Product.RProductCategoryId);
                output.RProductCategoryTitle = _lookupRProductCategory.Title.ToString();
            }
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Administration_Products_Edit)]
		 public async Task<GetProductForEditOutput> GetProductForEdit(EntityDto<long> input)
         {
            var product = await _productRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetProductForEditOutput {Product = ObjectMapper.Map<CreateOrEditProductDto>(product)};

		    if (output.Product.RProductCategoryId != null)
            {
                var _lookupRProductCategory = await _lookup_rProductCategoryRepository.FirstOrDefaultAsync((long)output.Product.RProductCategoryId);
                output.RProductCategoryTitle = _lookupRProductCategory.Title.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditProductDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_Products_Create)]
		 protected virtual async Task Create(CreateOrEditProductDto input)
         {
            var product = ObjectMapper.Map<Product>(input);

			

            await _productRepository.InsertAsync(product);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_Products_Edit)]
		 protected virtual async Task Update(CreateOrEditProductDto input)
         {
            var product = await _productRepository.FirstOrDefaultAsync((long)input.Id);
             ObjectMapper.Map(input, product);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_Products_Delete)]
         public async Task Delete(EntityDto<long> input)
         {
            await _productRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetProductsToExcel(GetAllProductsForExcelInput input)
         {
			
			var filteredProducts = _productRepository.GetAll()
						.Include( e => e.RProductCategoryFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.MetaTitle.Contains(input.Filter) || e.MetaDescription.Contains(input.Filter) || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Content.Contains(input.Filter) || e.ImageName.Contains(input.Filter) || e.SaleMan.Contains(input.Filter) || e.LinkReference.Contains(input.Filter) || e.ImageList.Contains(input.Filter) || e.IsContact.Contains(input.Filter) || e.DisplayIsContact.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.MetaTitleFilter),  e => e.MetaTitle.ToLower() == input.MetaTitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.MetaDescriptionFilter),  e => e.MetaDescription.ToLower() == input.MetaDescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter),  e => e.Title.ToLower() == input.TitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter),  e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ContentFilter),  e => e.Content.ToLower() == input.ContentFilter.ToLower().Trim())
						.WhereIf(input.MinPriorityFilter != null, e => e.Priority >= input.MinPriorityFilter)
						.WhereIf(input.MaxPriorityFilter != null, e => e.Priority <= input.MaxPriorityFilter)
						.WhereIf(input.IsAvailableFilter > -1,  e => Convert.ToInt32(e.IsAvailable) == input.IsAvailableFilter )
						.WhereIf(input.IsHotFilter > -1,  e => Convert.ToInt32(e.IsHot) == input.IsHotFilter )
						.WhereIf(input.IsNewFilter > -1,  e => Convert.ToInt32(e.IsNew) == input.IsNewFilter )
						.WhereIf(input.MinPriceOriginFilter != null, e => e.PriceOrigin >= input.MinPriceOriginFilter)
						.WhereIf(input.MaxPriceOriginFilter != null, e => e.PriceOrigin <= input.MaxPriceOriginFilter)
						.WhereIf(input.MinPricePromotionFilter != null, e => e.PricePromotion >= input.MinPricePromotionFilter)
						.WhereIf(input.MaxPricePromotionFilter != null, e => e.PricePromotion <= input.MaxPricePromotionFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.SaleManFilter),  e => e.SaleMan.ToLower() == input.SaleManFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.LinkReferenceFilter),  e => e.LinkReference.ToLower() == input.LinkReferenceFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.IsContactFilter),  e => e.IsContact.ToLower() == input.IsContactFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.RProductCategoryTitleFilter), e => e.RProductCategoryFk != null && e.RProductCategoryFk.Title.ToLower() == input.RProductCategoryTitleFilter.ToLower().Trim());

			var query = (from o in filteredProducts
                         join o1 in _lookup_rProductCategoryRepository.GetAll() on o.RProductCategoryId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetProductForViewDto() { 
							Product = new ProductDto
							{
                                Title = o.Title,
                                Priority = o.Priority,
                                IsAvailable = o.IsAvailable,
                                IsHot = o.IsHot,
                                IsNew = o.IsNew,
                                ImageName = o.ImageName,
                                PriceOrigin = o.PriceOrigin,
                                PricePromotion = o.PricePromotion,
                                Hit = o.Hit,
                                Id = o.Id
							},
                         	RProductCategoryTitle = s1 == null ? "" : s1.Title.ToString()
						 });


            var productListDtos = await query.ToListAsync();

            return _productsExcelExporter.ExportToFile(productListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_Administration_Products)]
         public async Task<PagedResultDto<ProductRProductCategoryLookupTableDto>> GetAllRProductCategoryForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _lookup_rProductCategoryRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Title.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var rProductCategoryList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<ProductRProductCategoryLookupTableDto>();
			foreach(var rProductCategory in rProductCategoryList){
				lookupTableDtoList.Add(new ProductRProductCategoryLookupTableDto
				{
					Id = rProductCategory.Id,
					DisplayName = rProductCategory.Title?.ToString()
				});
			}

            return new PagedResultDto<ProductRProductCategoryLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}