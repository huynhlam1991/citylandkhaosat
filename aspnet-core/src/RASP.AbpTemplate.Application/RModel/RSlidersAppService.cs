

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using RASP.AbpTemplate.RModel.Exporting;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace RASP.AbpTemplate.RModel
{
	[AbpAuthorize(AppPermissions.Pages_Administration_RSliders)]
    public class RSlidersAppService : AbpTemplateAppServiceBase, IRSlidersAppService
    {
		 private readonly IRepository<RSlider> _rSliderRepository;
		 private readonly IRSlidersExcelExporter _rSlidersExcelExporter;
		 

		  public RSlidersAppService(IRepository<RSlider> rSliderRepository, IRSlidersExcelExporter rSlidersExcelExporter ) 
		  {
			_rSliderRepository = rSliderRepository;
			_rSlidersExcelExporter = rSlidersExcelExporter;
			
		  }

		 public async Task<PagedResultDto<GetRSliderForViewDto>> GetAll(GetAllRSlidersInput input)
         {
			
			var filteredRSliders = _rSliderRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Title.Contains(input.Filter) || e.FileName.Contains(input.Filter) || e.Link.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.PositionCode.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter),  e => e.Title.ToLower() == input.TitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.FileNameFilter),  e => e.FileName.ToLower() == input.FileNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.LinkFilter),  e => e.Link.ToLower() == input.LinkFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter),  e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
						.WhereIf(input.IsAvailableFilter > -1,  e => Convert.ToInt32(e.IsAvailable) == input.IsAvailableFilter )
						.WhereIf(!string.IsNullOrWhiteSpace(input.PositionCodeFilter),  e => e.PositionCode.ToLower() == input.PositionCodeFilter.ToLower().Trim())
						.WhereIf(input.MinPriorityFilter != null, e => e.Priority >= input.MinPriorityFilter)
						.WhereIf(input.MaxPriorityFilter != null, e => e.Priority <= input.MaxPriorityFilter);

			var pagedAndFilteredRSliders = filteredRSliders
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

			var rSliders = from o in pagedAndFilteredRSliders
                         select new GetRSliderForViewDto() {
							RSlider = new RSliderDto
							{
                                Title = o.Title,
                                FileName = o.FileName,
                                Description = o.Description,
                                IsAvailable = o.IsAvailable,
                                PositionCode = o.PositionCode,
                                Priority = o.Priority,
                                Id = o.Id
							}
						};

            var totalCount = await filteredRSliders.CountAsync();

            return new PagedResultDto<GetRSliderForViewDto>(
                totalCount,
                await rSliders.ToListAsync()
            );
         }
		 
		 public async Task<GetRSliderForViewDto> GetRSliderForView(int id)
         {
            var rSlider = await _rSliderRepository.GetAsync(id);

            var output = new GetRSliderForViewDto { RSlider = ObjectMapper.Map<RSliderDto>(rSlider) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Administration_RSliders_Edit)]
		 public async Task<GetRSliderForEditOutput> GetRSliderForEdit(EntityDto input)
         {
            var rSlider = await _rSliderRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetRSliderForEditOutput {RSlider = ObjectMapper.Map<CreateOrEditRSliderDto>(rSlider)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditRSliderDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_RSliders_Create)]
		 protected virtual async Task Create(CreateOrEditRSliderDto input)
         {
            var rSlider = ObjectMapper.Map<RSlider>(input);

			

            await _rSliderRepository.InsertAsync(rSlider);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_RSliders_Edit)]
		 protected virtual async Task Update(CreateOrEditRSliderDto input)
         {
            var rSlider = await _rSliderRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, rSlider);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_RSliders_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _rSliderRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetRSlidersToExcel(GetAllRSlidersForExcelInput input)
         {
			
			var filteredRSliders = _rSliderRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Title.Contains(input.Filter) || e.FileName.Contains(input.Filter) || e.Link.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.PositionCode.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter),  e => e.Title.ToLower() == input.TitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.FileNameFilter),  e => e.FileName.ToLower() == input.FileNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.LinkFilter),  e => e.Link.ToLower() == input.LinkFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter),  e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
						.WhereIf(input.IsAvailableFilter > -1,  e => Convert.ToInt32(e.IsAvailable) == input.IsAvailableFilter )
						.WhereIf(!string.IsNullOrWhiteSpace(input.PositionCodeFilter),  e => e.PositionCode.ToLower() == input.PositionCodeFilter.ToLower().Trim())
						.WhereIf(input.MinPriorityFilter != null, e => e.Priority >= input.MinPriorityFilter)
						.WhereIf(input.MaxPriorityFilter != null, e => e.Priority <= input.MaxPriorityFilter);

			var query = (from o in filteredRSliders
                         select new GetRSliderForViewDto() { 
							RSlider = new RSliderDto
							{
                                Title = o.Title,
                                FileName = o.FileName,
                                Description = o.Description,
                                IsAvailable = o.IsAvailable,
                                PositionCode = o.PositionCode,
                                Priority = o.Priority,
                                Id = o.Id
							}
						 });


            var rSliderListDtos = await query.ToListAsync();

            return _rSlidersExcelExporter.ExportToFile(rSliderListDtos);
         }


    }
}