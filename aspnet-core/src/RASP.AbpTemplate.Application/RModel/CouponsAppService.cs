

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using RASP.AbpTemplate.RModel.Exporting;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace RASP.AbpTemplate.RModel
{
	[AbpAuthorize(AppPermissions.Pages_Administration_Coupons)]
    public class CouponsAppService : AbpTemplateAppServiceBase, ICouponsAppService
    {
		 private readonly IRepository<Coupon, long> _couponRepository;
		 private readonly ICouponsExcelExporter _couponsExcelExporter;
		 

		  public CouponsAppService(IRepository<Coupon, long> couponRepository, ICouponsExcelExporter couponsExcelExporter ) 
		  {
			_couponRepository = couponRepository;
			_couponsExcelExporter = couponsExcelExporter;
			
		  }

		 public async Task<PagedResultDto<GetCouponForViewDto>> GetAll(GetAllCouponsInput input)
         {
			
			var filteredCoupons = _couponRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Content.Contains(input.Filter) || e.ValueCoupon.Contains(input.Filter) || e.Author.Contains(input.Filter) || e.ImageName.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter),  e => e.Title.ToLower() == input.TitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter),  e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ContentFilter),  e => e.Content.ToLower() == input.ContentFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ValueCouponFilter),  e => e.ValueCoupon.ToLower() == input.ValueCouponFilter.ToLower().Trim())
						.WhereIf(input.MinTimeStartFilter != null, e => e.TimeStart >= input.MinTimeStartFilter)
						.WhereIf(input.MaxTimeStartFilter != null, e => e.TimeStart <= input.MaxTimeStartFilter)
						.WhereIf(input.MinTimeEndFilter != null, e => e.TimeEnd >= input.MinTimeEndFilter)
						.WhereIf(input.MaxTimeEndFilter != null, e => e.TimeEnd <= input.MaxTimeEndFilter)
						.WhereIf(input.IsCountDownToEndFilter > -1,  e => Convert.ToInt32(e.IsCountDownToEnd) == input.IsCountDownToEndFilter )
						.WhereIf(input.IsCountDownToStartFilter > -1,  e => Convert.ToInt32(e.IsCountDownToStart) == input.IsCountDownToStartFilter )
						.WhereIf(input.IsAvailableFilter > -1,  e => Convert.ToInt32(e.IsAvailable) == input.IsAvailableFilter )
						.WhereIf(!string.IsNullOrWhiteSpace(input.AuthorFilter),  e => e.Author.ToLower() == input.AuthorFilter.ToLower().Trim())
						.WhereIf(input.IsShowCarouselFilter > -1,  e => Convert.ToInt32(e.IsShowCarousel) == input.IsShowCarouselFilter )
						.WhereIf(!string.IsNullOrWhiteSpace(input.ImageNameFilter),  e => e.ImageName.ToLower() == input.ImageNameFilter.ToLower().Trim());

			var pagedAndFilteredCoupons = filteredCoupons
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

			var coupons = from o in pagedAndFilteredCoupons
                         select new GetCouponForViewDto() {
							Coupon = new CouponDto
							{
                                Title = o.Title,
                                Description = o.Description,
                                Content = o.Content,
                                ValueCoupon = o.ValueCoupon,
                                TimeStart = o.TimeStart,
                                TimeEnd = o.TimeEnd,
                                IsCountDownToEnd = o.IsCountDownToEnd,
                                IsCountDownToStart = o.IsCountDownToStart,
                                IsAvailable = o.IsAvailable,
                                Author = o.Author,
                                IsShowCarousel = o.IsShowCarousel,
                                ImageName = o.ImageName,
                                Hit = o.Hit,
                                Id = o.Id
							}
						};

            var totalCount = await filteredCoupons.CountAsync();

            return new PagedResultDto<GetCouponForViewDto>(
                totalCount,
                await coupons.ToListAsync()
            );
         }
		 
		 public async Task<GetCouponForViewDto> GetCouponForView(long id)
         {
            var coupon = await _couponRepository.GetAsync(id);

            var output = new GetCouponForViewDto { Coupon = ObjectMapper.Map<CouponDto>(coupon) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Administration_Coupons_Edit)]
		 public async Task<GetCouponForEditOutput> GetCouponForEdit(EntityDto<long> input)
         {
            var coupon = await _couponRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetCouponForEditOutput {Coupon = ObjectMapper.Map<CreateOrEditCouponDto>(coupon)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditCouponDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_Coupons_Create)]
		 protected virtual async Task Create(CreateOrEditCouponDto input)
         {
            var coupon = ObjectMapper.Map<Coupon>(input);

			

            await _couponRepository.InsertAsync(coupon);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_Coupons_Edit)]
		 protected virtual async Task Update(CreateOrEditCouponDto input)
         {
            var coupon = await _couponRepository.FirstOrDefaultAsync((long)input.Id);
             ObjectMapper.Map(input, coupon);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_Coupons_Delete)]
         public async Task Delete(EntityDto<long> input)
         {
            await _couponRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetCouponsToExcel(GetAllCouponsForExcelInput input)
         {
			
			var filteredCoupons = _couponRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Content.Contains(input.Filter) || e.ValueCoupon.Contains(input.Filter) || e.Author.Contains(input.Filter) || e.ImageName.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter),  e => e.Title.ToLower() == input.TitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter),  e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ContentFilter),  e => e.Content.ToLower() == input.ContentFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ValueCouponFilter),  e => e.ValueCoupon.ToLower() == input.ValueCouponFilter.ToLower().Trim())
						.WhereIf(input.MinTimeStartFilter != null, e => e.TimeStart >= input.MinTimeStartFilter)
						.WhereIf(input.MaxTimeStartFilter != null, e => e.TimeStart <= input.MaxTimeStartFilter)
						.WhereIf(input.MinTimeEndFilter != null, e => e.TimeEnd >= input.MinTimeEndFilter)
						.WhereIf(input.MaxTimeEndFilter != null, e => e.TimeEnd <= input.MaxTimeEndFilter)
						.WhereIf(input.IsCountDownToEndFilter > -1,  e => Convert.ToInt32(e.IsCountDownToEnd) == input.IsCountDownToEndFilter )
						.WhereIf(input.IsCountDownToStartFilter > -1,  e => Convert.ToInt32(e.IsCountDownToStart) == input.IsCountDownToStartFilter )
						.WhereIf(input.IsAvailableFilter > -1,  e => Convert.ToInt32(e.IsAvailable) == input.IsAvailableFilter )
						.WhereIf(!string.IsNullOrWhiteSpace(input.AuthorFilter),  e => e.Author.ToLower() == input.AuthorFilter.ToLower().Trim())
						.WhereIf(input.IsShowCarouselFilter > -1,  e => Convert.ToInt32(e.IsShowCarousel) == input.IsShowCarouselFilter )
						.WhereIf(!string.IsNullOrWhiteSpace(input.ImageNameFilter),  e => e.ImageName.ToLower() == input.ImageNameFilter.ToLower().Trim());

			var query = (from o in filteredCoupons
                         select new GetCouponForViewDto() { 
							Coupon = new CouponDto
							{
                                Title = o.Title,
                                Description = o.Description,
                                Content = o.Content,
                                ValueCoupon = o.ValueCoupon,
                                TimeStart = o.TimeStart,
                                TimeEnd = o.TimeEnd,
                                IsCountDownToEnd = o.IsCountDownToEnd,
                                IsCountDownToStart = o.IsCountDownToStart,
                                IsAvailable = o.IsAvailable,
                                Author = o.Author,
                                IsShowCarousel = o.IsShowCarousel,
                                ImageName = o.ImageName,
                                Hit = o.Hit,
                                Id = o.Id
							}
						 });


            var couponListDtos = await query.ToListAsync();

            return _couponsExcelExporter.ExportToFile(couponListDtos);
         }


    }
}