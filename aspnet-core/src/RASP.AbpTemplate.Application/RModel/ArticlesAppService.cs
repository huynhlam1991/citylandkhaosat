using RASP.AbpTemplate.RModel;


using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using RASP.AbpTemplate.RModel.Exporting;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.Dto;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace RASP.AbpTemplate.RModel
{
	[AbpAuthorize(AppPermissions.Pages_Administration_Articles)]
    public class ArticlesAppService : AbpTemplateAppServiceBase, IArticlesAppService
    {
		 private readonly IRepository<Article, long> _articleRepository;
		 private readonly IArticlesExcelExporter _articlesExcelExporter;
		 private readonly IRepository<RCategory,long> _lookup_rCategoryRepository;
		 

		  public ArticlesAppService(IRepository<Article, long> articleRepository, IArticlesExcelExporter articlesExcelExporter , IRepository<RCategory, long> lookup_rCategoryRepository) 
		  {
			_articleRepository = articleRepository;
			_articlesExcelExporter = articlesExcelExporter;
			_lookup_rCategoryRepository = lookup_rCategoryRepository;
		
		  }

		 public async Task<PagedResultDto<GetArticleForViewDto>> GetAll(GetAllArticlesInput input)
         {
			
			var filteredArticles = _articleRepository.GetAll()
						.Include( e => e.RCategoryFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.MetaTitle.Contains(input.Filter) || e.MetaDescription.Contains(input.Filter) || e.ImageName.Contains(input.Filter) || e.Tag.Contains(input.Filter) || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Content.Contains(input.Filter) || e.Author.Contains(input.Filter) || e.IsHot.Contains(input.Filter) || e.IsNew.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.MetaTitleFilter),  e => e.MetaTitle.ToLower() == input.MetaTitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.MetaDescriptionFilter),  e => e.MetaDescription.ToLower() == input.MetaDescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.TagFilter),  e => e.Tag.ToLower() == input.TagFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter),  e => e.Title.ToLower() == input.TitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter),  e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ContentFilter),  e => e.Content.ToLower() == input.ContentFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.AuthorFilter),  e => e.Author.ToLower() == input.AuthorFilter.ToLower().Trim())
						.WhereIf(input.IsAvailableFilter > -1,  e => Convert.ToInt32(e.IsAvailable) == input.IsAvailableFilter )
						.WhereIf(!string.IsNullOrWhiteSpace(input.IsHotFilter),  e => e.IsHot.ToLower() == input.IsHotFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.IsNewFilter),  e => e.IsNew.ToLower() == input.IsNewFilter.ToLower().Trim())
						.WhereIf(input.MinPriorityFilter != null, e => e.Priority >= input.MinPriorityFilter)
						.WhereIf(input.MaxPriorityFilter != null, e => e.Priority <= input.MaxPriorityFilter)
						.WhereIf(input.MinHitFilter != null, e => e.Hit >= input.MinHitFilter)
						.WhereIf(input.MaxHitFilter != null, e => e.Hit <= input.MaxHitFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.RCategoryTitleFilter), e => e.RCategoryFk != null && e.RCategoryFk.Title.ToLower() == input.RCategoryTitleFilter.ToLower().Trim());

			var pagedAndFilteredArticles = filteredArticles
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

			var articles = from o in pagedAndFilteredArticles
                         join o1 in _lookup_rCategoryRepository.GetAll() on o.RCategoryId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetArticleForViewDto() {
							Article = new ArticleDto
							{
                                MetaTitle = o.MetaTitle,
                                MetaDescription = o.MetaDescription,
                                ImageName = o.ImageName,
                                Title = o.Title,
                                Author = o.Author,
                                IsAvailable = o.IsAvailable,
                                IsHot = o.IsHot,
                                IsNew = o.IsNew,
                                Priority = o.Priority,
                                Hit = o.Hit,
                                Id = o.Id
							},
                         	RCategoryTitle = s1 == null ? "" : s1.Title.ToString()
						};

            var totalCount = await filteredArticles.CountAsync();

            return new PagedResultDto<GetArticleForViewDto>(
                totalCount,
                await articles.ToListAsync()
            );
         }
		 
		 public async Task<GetArticleForViewDto> GetArticleForView(long id)
         {
            var article = await _articleRepository.GetAsync(id);

            var output = new GetArticleForViewDto { Article = ObjectMapper.Map<ArticleDto>(article) };

		    if (output.Article.RCategoryId != null)
            {
                var _lookupRCategory = await _lookup_rCategoryRepository.FirstOrDefaultAsync((long)output.Article.RCategoryId);
                output.RCategoryTitle = _lookupRCategory.Title.ToString();
            }
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Administration_Articles_Edit)]
		 public async Task<GetArticleForEditOutput> GetArticleForEdit(EntityDto<long> input)
         {
            var article = await _articleRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetArticleForEditOutput {Article = ObjectMapper.Map<CreateOrEditArticleDto>(article)};

		    if (output.Article.RCategoryId != null)
            {
                var _lookupRCategory = await _lookup_rCategoryRepository.FirstOrDefaultAsync((long)output.Article.RCategoryId);
                output.RCategoryTitle = _lookupRCategory.Title.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditArticleDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_Articles_Create)]
		 protected virtual async Task Create(CreateOrEditArticleDto input)
         {
            var article = ObjectMapper.Map<Article>(input);

			

            await _articleRepository.InsertAsync(article);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_Articles_Edit)]
		 protected virtual async Task Update(CreateOrEditArticleDto input)
         {
            var article = await _articleRepository.FirstOrDefaultAsync((long)input.Id);
             ObjectMapper.Map(input, article);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_Articles_Delete)]
         public async Task Delete(EntityDto<long> input)
         {
            await _articleRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetArticlesToExcel(GetAllArticlesForExcelInput input)
         {
			
			var filteredArticles = _articleRepository.GetAll()
						.Include( e => e.RCategoryFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.MetaTitle.Contains(input.Filter) || e.MetaDescription.Contains(input.Filter) || e.ImageName.Contains(input.Filter) || e.Tag.Contains(input.Filter) || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Content.Contains(input.Filter) || e.Author.Contains(input.Filter) || e.IsHot.Contains(input.Filter) || e.IsNew.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.MetaTitleFilter),  e => e.MetaTitle.ToLower() == input.MetaTitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.MetaDescriptionFilter),  e => e.MetaDescription.ToLower() == input.MetaDescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.TagFilter),  e => e.Tag.ToLower() == input.TagFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter),  e => e.Title.ToLower() == input.TitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter),  e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ContentFilter),  e => e.Content.ToLower() == input.ContentFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.AuthorFilter),  e => e.Author.ToLower() == input.AuthorFilter.ToLower().Trim())
						.WhereIf(input.IsAvailableFilter > -1,  e => Convert.ToInt32(e.IsAvailable) == input.IsAvailableFilter )
						.WhereIf(!string.IsNullOrWhiteSpace(input.IsHotFilter),  e => e.IsHot.ToLower() == input.IsHotFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.IsNewFilter),  e => e.IsNew.ToLower() == input.IsNewFilter.ToLower().Trim())
						.WhereIf(input.MinPriorityFilter != null, e => e.Priority >= input.MinPriorityFilter)
						.WhereIf(input.MaxPriorityFilter != null, e => e.Priority <= input.MaxPriorityFilter)
						.WhereIf(input.MinHitFilter != null, e => e.Hit >= input.MinHitFilter)
						.WhereIf(input.MaxHitFilter != null, e => e.Hit <= input.MaxHitFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.RCategoryTitleFilter), e => e.RCategoryFk != null && e.RCategoryFk.Title.ToLower() == input.RCategoryTitleFilter.ToLower().Trim());

			var query = (from o in filteredArticles
                         join o1 in _lookup_rCategoryRepository.GetAll() on o.RCategoryId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetArticleForViewDto() { 
							Article = new ArticleDto
							{
                                MetaTitle = o.MetaTitle,
                                MetaDescription = o.MetaDescription,
                                ImageName = o.ImageName,
                                Title = o.Title,
                                Author = o.Author,
                                IsAvailable = o.IsAvailable,
                                IsHot = o.IsHot,
                                IsNew = o.IsNew,
                                Priority = o.Priority,
                                Hit = o.Hit,
                                Id = o.Id
							},
                         	RCategoryTitle = s1 == null ? "" : s1.Title.ToString()
						 });


            var articleListDtos = await query.ToListAsync();

            return _articlesExcelExporter.ExportToFile(articleListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_Administration_Articles)]
         public async Task<PagedResultDto<ArticleRCategoryLookupTableDto>> GetAllRCategoryForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _lookup_rCategoryRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Title.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var rCategoryList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<ArticleRCategoryLookupTableDto>();
			foreach(var rCategory in rCategoryList){
				lookupTableDtoList.Add(new ArticleRCategoryLookupTableDto
				{
					Id = rCategory.Id,
					DisplayName = rCategory.Title?.ToString()
				});
			}

            return new PagedResultDto<ArticleRCategoryLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}