﻿using System.Threading.Tasks;

namespace RASP.AbpTemplate.Security.Recaptcha
{
    public interface IRecaptchaValidator
    {
        Task ValidateAsync(string captchaResponse);
    }
}