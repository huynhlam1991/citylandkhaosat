﻿using System.Collections.Generic;
using RASP.AbpTemplate.Chat.Dto;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.Chat.Exporting
{
    public interface IChatMessageListExcelExporter
    {
        FileDto ExportToFile(List<ChatMessageExportDto> messages);
    }
}
