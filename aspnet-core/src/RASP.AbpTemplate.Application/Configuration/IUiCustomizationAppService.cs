﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using RASP.AbpTemplate.Configuration.Dto;

namespace RASP.AbpTemplate.Configuration
{
    public interface IUiCustomizationSettingsAppService : IApplicationService
    {
        Task<List<ThemeSettingsDto>> GetUiManagementSettings();

        Task UpdateUiManagementSettings(ThemeSettingsDto settings);

        Task UpdateDefaultUiManagementSettings(ThemeSettingsDto settings);

        Task UseSystemDefaultSettings();
    }
}
