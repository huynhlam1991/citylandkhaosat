

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using RASP.AbpTemplate.RSurvey.Exporting;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.Domain.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace RASP.AbpTemplate.RSurvey
{
    [AbpAuthorize(AppPermissions.Pages_Administration_Surveies)]
    public class SurveiesAppService : AbpTemplateAppServiceBase, ISurveiesAppService
    {
        private readonly IRepository<Survey, long> _surveyRepository;
        private readonly ISurveiesExcelExporter _surveiesExcelExporter;
        private readonly IRepository<SurveyResult, long> _surveyResultRepository;

        public SurveiesAppService(IRepository<Survey, long> surveyRepository, ISurveiesExcelExporter surveiesExcelExporter,
            IRepository<SurveyResult, long> surveyResultRepository)
        {
            _surveyRepository = surveyRepository;
            _surveiesExcelExporter = surveiesExcelExporter;
            _surveyResultRepository = surveyResultRepository;

        }

        public async Task<PagedResultDto<GetSurveyForViewDto>> GetAll(GetAllSurveiesInput input)
        {

            var filteredSurveies = _surveyRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Text.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter), e => e.Title.ToLower() == input.TitleFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
                        .WhereIf(input.MinStartDateFilter != null, e => e.StartDate >= input.MinStartDateFilter)
                        .WhereIf(input.MaxStartDateFilter != null, e => e.StartDate <= input.MaxStartDateFilter)
                        .WhereIf(input.MinEndDateFilter != null, e => e.EndDate >= input.MinEndDateFilter)
                        .WhereIf(input.MaxEndDateFilter != null, e => e.EndDate <= input.MaxEndDateFilter)
                        .WhereIf(input.IsAvailableFilter > -1, e => Convert.ToInt32(e.IsAvailable) == input.IsAvailableFilter);

            var pagedAndFilteredSurveies = filteredSurveies
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var surveies = from o in pagedAndFilteredSurveies
                           select new GetSurveyForViewDto()
                           {
                               Survey = new SurveyDto
                               {
                                   Title = o.Title,
                                   Description = o.Description,
                                   StartDate = o.StartDate,
                                   EndDate = o.EndDate,
                                   IsAvailable = o.IsAvailable,
                                   Text = o.Text,
                                   Id = o.Id
                               }
                           };

            var totalCount = await filteredSurveies.CountAsync();

            return new PagedResultDto<GetSurveyForViewDto>(
                totalCount,
                await surveies.ToListAsync()
            );
        }

        public async Task<GetSurveyForViewDto> GetSurveyForView(long id)
        {
            var survey = await _surveyRepository.GetAsync(id);

            var output = new GetSurveyForViewDto { Survey = ObjectMapper.Map<SurveyDto>(survey) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Surveies_Edit)]
        public async Task<GetSurveyForEditOutput> GetSurveyForEdit(EntityDto<long> input)
        {
            var survey = await _surveyRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetSurveyForEditOutput { Survey = ObjectMapper.Map<CreateOrEditSurveyDto>(survey) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditSurveyDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Surveies_Create)]
        protected virtual async Task Create(CreateOrEditSurveyDto input)
        {
            var survey = ObjectMapper.Map<Survey>(input);



            await _surveyRepository.InsertAsync(survey);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Surveies_Edit)]
        protected virtual async Task Update(CreateOrEditSurveyDto input)
        {
            var survey = await _surveyRepository.FirstOrDefaultAsync((long)input.Id);
            ObjectMapper.Map(input, survey);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Surveies_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            await _surveyRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetSurveiesToExcel(GetAllSurveiesForExcelInput input)
        {

            //var filteredSurveies = _surveyRepository.GetAll()
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Text.Contains(input.Filter))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter), e => e.Title.ToLower() == input.TitleFilter.ToLower().Trim())
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
            //            .WhereIf(input.MinStartDateFilter != null, e => e.StartDate >= input.MinStartDateFilter)
            //            .WhereIf(input.MaxStartDateFilter != null, e => e.StartDate <= input.MaxStartDateFilter)
            //            .WhereIf(input.MinEndDateFilter != null, e => e.EndDate >= input.MinEndDateFilter)
            //            .WhereIf(input.MaxEndDateFilter != null, e => e.EndDate <= input.MaxEndDateFilter)
            //            .WhereIf(input.IsAvailableFilter > -1, e => Convert.ToInt32(e.IsAvailable) == input.IsAvailableFilter);

            //var query = (from o in filteredSurveies
            //             select new GetSurveyForViewDto()
            //             {
            //                 Survey = new SurveyDto
            //                 {
            //                     Title = o.Title,
            //                     Description = o.Description,
            //                     StartDate = o.StartDate,
            //                     EndDate = o.EndDate,
            //                     IsAvailable = o.IsAvailable,
            //                     Text = o.Text,
            //                     Id = o.Id
            //                 }
            //             });


            //var surveyListDtos = await query.ToListAsync();

            return _surveiesExcelExporter.ExportToFile(new List<GetSurveyForViewDto>(), input.SurveyId);
        }
        public async Task<FileDto> GetSurveiesToExcelDetail(GetAllSurveiesForExcelInput input)
        {

            //var filteredSurveies = _surveyRepository.GetAll()
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Text.Contains(input.Filter))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter), e => e.Title.ToLower() == input.TitleFilter.ToLower().Trim())
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
            //            .WhereIf(input.MinStartDateFilter != null, e => e.StartDate >= input.MinStartDateFilter)
            //            .WhereIf(input.MaxStartDateFilter != null, e => e.StartDate <= input.MaxStartDateFilter)
            //            .WhereIf(input.MinEndDateFilter != null, e => e.EndDate >= input.MinEndDateFilter)
            //            .WhereIf(input.MaxEndDateFilter != null, e => e.EndDate <= input.MaxEndDateFilter)
            //            .WhereIf(input.IsAvailableFilter > -1, e => Convert.ToInt32(e.IsAvailable) == input.IsAvailableFilter);

            //var query = (from o in filteredSurveies
            //             select new GetSurveyForViewDto()
            //             {
            //                 Survey = new SurveyDto
            //                 {
            //                     Title = o.Title,
            //                     Description = o.Description,
            //                     StartDate = o.StartDate,
            //                     EndDate = o.EndDate,
            //                     IsAvailable = o.IsAvailable,
            //                     Text = o.Text,
            //                     Id = o.Id
            //                 }
            //             });


            //var surveyListDtos = await query.ToListAsync();

            return _surveiesExcelExporter.ExportToFileDetail(new List<GetSurveyForViewDto>(), input.SurveyId);
        }

        public FileDto GetSurveiesResultToExcel(GetSurveyResult filter)
        {
            ExportSurvey result = new ExportSurvey();
            result.Titles = new List<TitleExport>()
            {
                new TitleExport(){Name = "CreationTime", Title = "CreationTime"},
                new TitleExport(){Name = "Email",Title = "Email"}
            };
            var surveyResult = _surveyResultRepository.GetAll().Where(x => x.SurveyId == filter.SurveyId).ToList();
            var survey = _surveyRepository.FirstOrDefault(x => x.Id == filter.SurveyId);
            var jsonSurvey = survey.GetData<string>("SurveyStorage");
            var jsonSurveyData = JsonConvert.DeserializeObject<CustomSurveyResultDto>(jsonSurvey);
            
            //var jsonSurveyResultData = JsonConvert.DeserializeObject<List<Dictionary<string,string>>>(jsonsurveyResult);
            foreach (var pageSurveyDto in jsonSurveyData.pages)
            {
                result.elements.AddRange(pageSurveyDto.elements);
                foreach (var elementSurveyDto in pageSurveyDto.elements)
                {
                    var tempTitle = new TitleExport()
                    {
                        Name = elementSurveyDto.name,
                        Title = elementSurveyDto.title
                    };
                    result.Titles.Add(tempTitle);
                }
            }
            
            foreach (var surveyResult1 in surveyResult)
            {
                
                var jsonsurveyResult = surveyResult1.GetData<string>("ResultsStorage");
                
                JObject jsonsurveyResultData = JObject.Parse(jsonsurveyResult);
                var listAnswer = new List<AnswerQuestion>();
                foreach (var resultTitle in result.Titles)
                {
                    

                    var lstAnswer = jsonsurveyResultData[resultTitle.Name];
                    var row = new AnswerQuestion()
                    {
                        Email = surveyResult1.Text,
                        CreationTime = surveyResult1.CreationTime.ToShortDateString()
                    };
                    if (lstAnswer == null && resultTitle.Name == "CreationTime")
                    {
                        row.questionKey = "CreationTime";
                        row.questionText = "CreationTime";
                        row.AnswerText = surveyResult1.CreationTime.ToShortDateString();
                        listAnswer.Add(row);
                        result.answer.AddRange(listAnswer);
                        continue;
                    }
                    if (lstAnswer == null && resultTitle.Name == "Email")
                    {
                        row.questionKey = "Email";
                        row.questionText = "Email";
                        row.AnswerText = surveyResult1.Text;
                        listAnswer.Add(row);
                        continue;
                    }

                    if (lstAnswer != null)
                    {
                        for (int i = 0; i < lstAnswer.Count(); i++)
                        {

                            var questionChose = result.elements.FirstOrDefault(x => x.name == resultTitle.Name)
                                .choices;
                            if (questionChose!=null && questionChose.Any())
                            {
                                var aswerKey = ((Newtonsoft.Json.Linq.JValue)lstAnswer[i]).Value.ToString();
                                var choose = questionChose.FirstOrDefault(x => x.value == aswerKey);

                                row.AnswerText = choose.text;
                                row.questionKey = resultTitle.Name;
                                row.questionText = resultTitle.Title;
                                listAnswer.Add(row);
                                continue;
                            }
                            var questionMaxtrix =
                                result.elements.FirstOrDefault(x => x.name == resultTitle.Name).columns;
                            if (questionMaxtrix!=null && questionMaxtrix.Any())
                            {
                                var lAnswer = lstAnswer.ToArray()[i];
                                var aswerKey =
                                    ((Newtonsoft.Json.Linq.JValue) ((Newtonsoft.Json.Linq.JProperty) lAnswer).Value)
                                    .Value.ToString();
                                var choose = questionMaxtrix.FirstOrDefault(x => x == aswerKey);

                                row.AnswerText = choose;
                                row.questionKey = resultTitle.Name;
                                row.questionText = resultTitle.Title;
                                listAnswer.Add(row);
                                continue;
                               
                            }

                        }
                    }
                    
                    result.answer.AddRange(listAnswer);

                }
                
            }
            
            return _surveiesExcelExporter.ExportSurveyResultToFile(result);
        }

    }
}