using RASP.AbpTemplate.RSurvey;


using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using RASP.AbpTemplate.RSurvey.Exporting;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace RASP.AbpTemplate.RSurvey
{
	[AbpAuthorize(AppPermissions.Pages_Administration_Answers)]
    public class AnswersAppService : AbpTemplateAppServiceBase, IAnswersAppService
    {
		 private readonly IRepository<Answer, long> _answerRepository;
		 private readonly IAnswersExcelExporter _answersExcelExporter;
		 private readonly IRepository<Question,long> _lookup_questionRepository;
		 

		  public AnswersAppService(IRepository<Answer, long> answerRepository, IAnswersExcelExporter answersExcelExporter , IRepository<Question, long> lookup_questionRepository) 
		  {
			_answerRepository = answerRepository;
			_answersExcelExporter = answersExcelExporter;
			_lookup_questionRepository = lookup_questionRepository;
		
		  }

		 public async Task<PagedResultDto<GetAnswerForViewDto>> GetAll(GetAllAnswersInput input)
         {
			
			var filteredAnswers = _answerRepository.GetAll()
						.Include( e => e.QuestionFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.AnswerText.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.AnswerTextFilter),  e => e.AnswerText.ToLower() == input.AnswerTextFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.QuestionQuestionTextFilter), e => e.QuestionFk != null && e.QuestionFk.QuestionText.ToLower() == input.QuestionQuestionTextFilter.ToLower().Trim());

			var pagedAndFilteredAnswers = filteredAnswers
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

			var answers = from o in pagedAndFilteredAnswers
                         join o1 in _lookup_questionRepository.GetAll() on o.QuestionId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetAnswerForViewDto() {
							Answer = new AnswerDto
							{
                                AnswerText = o.AnswerText,
                                Id = o.Id
							},
                         	QuestionQuestionText = s1 == null ? "" : s1.QuestionText.ToString()
						};

            var totalCount = await filteredAnswers.CountAsync();

            return new PagedResultDto<GetAnswerForViewDto>(
                totalCount,
                await answers.ToListAsync()
            );
         }
		 
		 public async Task<GetAnswerForViewDto> GetAnswerForView(long id)
         {
            var answer = await _answerRepository.GetAsync(id);

            var output = new GetAnswerForViewDto { Answer = ObjectMapper.Map<AnswerDto>(answer) };

		    if (output.Answer.QuestionId != null)
            {
                var _lookupQuestion = await _lookup_questionRepository.FirstOrDefaultAsync((long)output.Answer.QuestionId);
                output.QuestionQuestionText = _lookupQuestion.QuestionText.ToString();
            }
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Administration_Answers_Edit)]
		 public async Task<GetAnswerForEditOutput> GetAnswerForEdit(EntityDto<long> input)
         {
            var answer = await _answerRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetAnswerForEditOutput {Answer = ObjectMapper.Map<CreateOrEditAnswerDto>(answer)};

		    if (output.Answer.QuestionId != null)
            {
                var _lookupQuestion = await _lookup_questionRepository.FirstOrDefaultAsync((long)output.Answer.QuestionId);
                output.QuestionQuestionText = _lookupQuestion.QuestionText.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditAnswerDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_Answers_Create)]
		 protected virtual async Task Create(CreateOrEditAnswerDto input)
         {
            var answer = ObjectMapper.Map<Answer>(input);

			

            await _answerRepository.InsertAsync(answer);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_Answers_Edit)]
		 protected virtual async Task Update(CreateOrEditAnswerDto input)
         {
            var answer = await _answerRepository.FirstOrDefaultAsync((long)input.Id);
             ObjectMapper.Map(input, answer);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_Answers_Delete)]
         public async Task Delete(EntityDto<long> input)
         {
            await _answerRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetAnswersToExcel(GetAllAnswersForExcelInput input)
         {
			
			var filteredAnswers = _answerRepository.GetAll()
						.Include( e => e.QuestionFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.AnswerText.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.AnswerTextFilter),  e => e.AnswerText.ToLower() == input.AnswerTextFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.QuestionQuestionTextFilter), e => e.QuestionFk != null && e.QuestionFk.QuestionText.ToLower() == input.QuestionQuestionTextFilter.ToLower().Trim());

			var query = (from o in filteredAnswers
                         join o1 in _lookup_questionRepository.GetAll() on o.QuestionId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetAnswerForViewDto() { 
							Answer = new AnswerDto
							{
                                AnswerText = o.AnswerText,
                                Id = o.Id
							},
                         	QuestionQuestionText = s1 == null ? "" : s1.QuestionText.ToString()
						 });


            var answerListDtos = await query.ToListAsync();

            return _answersExcelExporter.ExportToFile(answerListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_Administration_Answers)]
         public async Task<PagedResultDto<AnswerQuestionLookupTableDto>> GetAllQuestionForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _lookup_questionRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.QuestionText.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var questionList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<AnswerQuestionLookupTableDto>();
			foreach(var question in questionList){
				lookupTableDtoList.Add(new AnswerQuestionLookupTableDto
				{
					Id = question.Id,
					DisplayName = question.QuestionText?.ToString()
				});
			}

            return new PagedResultDto<AnswerQuestionLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}