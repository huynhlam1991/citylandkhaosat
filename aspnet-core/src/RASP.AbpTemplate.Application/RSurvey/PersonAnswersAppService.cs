using RASP.AbpTemplate.RSurvey;
using RASP.AbpTemplate.RSurvey;
using RASP.AbpTemplate.RSurvey;


using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using RASP.AbpTemplate.RSurvey.Exporting;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace RASP.AbpTemplate.RSurvey
{
	[AbpAuthorize(AppPermissions.Pages_Administration_PersonAnswers)]
    public class PersonAnswersAppService : AbpTemplateAppServiceBase, IPersonAnswersAppService
    {
		 private readonly IRepository<PersonAnswer, long> _personAnswerRepository;
		 private readonly IPersonAnswersExcelExporter _personAnswersExcelExporter;
		 private readonly IRepository<Survey,long> _lookup_surveyRepository;
		 private readonly IRepository<Question,long> _lookup_questionRepository;
		 private readonly IRepository<Answer,long> _lookup_answerRepository;
		 

		  public PersonAnswersAppService(IRepository<PersonAnswer, long> personAnswerRepository, IPersonAnswersExcelExporter personAnswersExcelExporter , IRepository<Survey, long> lookup_surveyRepository, IRepository<Question, long> lookup_questionRepository, IRepository<Answer, long> lookup_answerRepository) 
		  {
			_personAnswerRepository = personAnswerRepository;
			_personAnswersExcelExporter = personAnswersExcelExporter;
			_lookup_surveyRepository = lookup_surveyRepository;
		_lookup_questionRepository = lookup_questionRepository;
		_lookup_answerRepository = lookup_answerRepository;
		
		  }

		 public async Task<PagedResultDto<GetPersonAnswerForViewDto>> GetAll(GetAllPersonAnswersInput input)
         {
			
			var filteredPersonAnswers = _personAnswerRepository.GetAll()
						.Include( e => e.SurveyFk)
						.Include( e => e.QuestionFk)
						.Include( e => e.AnswerFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.OtherText.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.OtherTextFilter),  e => e.OtherText.ToLower() == input.OtherTextFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.SurveyTitleFilter), e => e.SurveyFk != null && e.SurveyFk.Title.ToLower() == input.SurveyTitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.QuestionQuestionTextFilter), e => e.QuestionFk != null && e.QuestionFk.QuestionText.ToLower() == input.QuestionQuestionTextFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.AnswerAnswerTextFilter), e => e.AnswerFk != null && e.AnswerFk.AnswerText.ToLower() == input.AnswerAnswerTextFilter.ToLower().Trim());

			var pagedAndFilteredPersonAnswers = filteredPersonAnswers
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

			var personAnswers = from o in pagedAndFilteredPersonAnswers
                         join o1 in _lookup_surveyRepository.GetAll() on o.SurveyId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _lookup_questionRepository.GetAll() on o.QuestionId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         join o3 in _lookup_answerRepository.GetAll() on o.AnswerId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()
                         
                         select new GetPersonAnswerForViewDto() {
							PersonAnswer = new PersonAnswerDto
							{
                                OtherText = o.OtherText,
                                Id = o.Id
							},
                         	SurveyTitle = s1 == null ? "" : s1.Title.ToString(),
                         	QuestionQuestionText = s2 == null ? "" : s2.QuestionText.ToString(),
                         	AnswerAnswerText = s3 == null ? "" : s3.AnswerText.ToString()
						};

            var totalCount = await filteredPersonAnswers.CountAsync();

            return new PagedResultDto<GetPersonAnswerForViewDto>(
                totalCount,
                await personAnswers.ToListAsync()
            );
         }
		 
		 public async Task<GetPersonAnswerForViewDto> GetPersonAnswerForView(long id)
         {
            var personAnswer = await _personAnswerRepository.GetAsync(id);

            var output = new GetPersonAnswerForViewDto { PersonAnswer = ObjectMapper.Map<PersonAnswerDto>(personAnswer) };

		    if (output.PersonAnswer.SurveyId != null)
            {
                var _lookupSurvey = await _lookup_surveyRepository.FirstOrDefaultAsync((long)output.PersonAnswer.SurveyId);
                output.SurveyTitle = _lookupSurvey.Title.ToString();
            }

		    if (output.PersonAnswer.QuestionId != null)
            {
                var _lookupQuestion = await _lookup_questionRepository.FirstOrDefaultAsync((long)output.PersonAnswer.QuestionId);
                output.QuestionQuestionText = _lookupQuestion.QuestionText.ToString();
            }

		    if (output.PersonAnswer.AnswerId != null)
            {
                var _lookupAnswer = await _lookup_answerRepository.FirstOrDefaultAsync((long)output.PersonAnswer.AnswerId);
                output.AnswerAnswerText = _lookupAnswer.AnswerText.ToString();
            }
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Administration_PersonAnswers_Edit)]
		 public async Task<GetPersonAnswerForEditOutput> GetPersonAnswerForEdit(EntityDto<long> input)
         {
            var personAnswer = await _personAnswerRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetPersonAnswerForEditOutput {PersonAnswer = ObjectMapper.Map<CreateOrEditPersonAnswerDto>(personAnswer)};

		    if (output.PersonAnswer.SurveyId != null)
            {
                var _lookupSurvey = await _lookup_surveyRepository.FirstOrDefaultAsync((long)output.PersonAnswer.SurveyId);
                output.SurveyTitle = _lookupSurvey.Title.ToString();
            }

		    if (output.PersonAnswer.QuestionId != null)
            {
                var _lookupQuestion = await _lookup_questionRepository.FirstOrDefaultAsync((long)output.PersonAnswer.QuestionId);
                output.QuestionQuestionText = _lookupQuestion.QuestionText.ToString();
            }

		    if (output.PersonAnswer.AnswerId != null)
            {
                var _lookupAnswer = await _lookup_answerRepository.FirstOrDefaultAsync((long)output.PersonAnswer.AnswerId);
                output.AnswerAnswerText = _lookupAnswer.AnswerText.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditPersonAnswerDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_PersonAnswers_Create)]
		 protected virtual async Task Create(CreateOrEditPersonAnswerDto input)
         {
            var personAnswer = ObjectMapper.Map<PersonAnswer>(input);

			

            await _personAnswerRepository.InsertAsync(personAnswer);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_PersonAnswers_Edit)]
		 protected virtual async Task Update(CreateOrEditPersonAnswerDto input)
         {
            var personAnswer = await _personAnswerRepository.FirstOrDefaultAsync((long)input.Id);
             ObjectMapper.Map(input, personAnswer);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_PersonAnswers_Delete)]
         public async Task Delete(EntityDto<long> input)
         {
            await _personAnswerRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetPersonAnswersToExcel(GetAllPersonAnswersForExcelInput input)
         {
			
			var filteredPersonAnswers = _personAnswerRepository.GetAll()
						.Include( e => e.SurveyFk)
						.Include( e => e.QuestionFk)
						.Include( e => e.AnswerFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.OtherText.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.OtherTextFilter),  e => e.OtherText.ToLower() == input.OtherTextFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.SurveyTitleFilter), e => e.SurveyFk != null && e.SurveyFk.Title.ToLower() == input.SurveyTitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.QuestionQuestionTextFilter), e => e.QuestionFk != null && e.QuestionFk.QuestionText.ToLower() == input.QuestionQuestionTextFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.AnswerAnswerTextFilter), e => e.AnswerFk != null && e.AnswerFk.AnswerText.ToLower() == input.AnswerAnswerTextFilter.ToLower().Trim());

			var query = (from o in filteredPersonAnswers
                         join o1 in _lookup_surveyRepository.GetAll() on o.SurveyId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _lookup_questionRepository.GetAll() on o.QuestionId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         join o3 in _lookup_answerRepository.GetAll() on o.AnswerId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()
                         
                         select new GetPersonAnswerForViewDto() { 
							PersonAnswer = new PersonAnswerDto
							{
                                OtherText = o.OtherText,
                                Id = o.Id
							},
                         	SurveyTitle = s1 == null ? "" : s1.Title.ToString(),
                         	QuestionQuestionText = s2 == null ? "" : s2.QuestionText.ToString(),
                         	AnswerAnswerText = s3 == null ? "" : s3.AnswerText.ToString()
						 });


            var personAnswerListDtos = await query.ToListAsync();

            return _personAnswersExcelExporter.ExportToFile(personAnswerListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_Administration_PersonAnswers)]
         public async Task<PagedResultDto<PersonAnswerSurveyLookupTableDto>> GetAllSurveyForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _lookup_surveyRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Title.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var surveyList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<PersonAnswerSurveyLookupTableDto>();
			foreach(var survey in surveyList){
				lookupTableDtoList.Add(new PersonAnswerSurveyLookupTableDto
				{
					Id = survey.Id,
					DisplayName = survey.Title?.ToString()
				});
			}

            return new PagedResultDto<PersonAnswerSurveyLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_Administration_PersonAnswers)]
         public async Task<PagedResultDto<PersonAnswerQuestionLookupTableDto>> GetAllQuestionForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _lookup_questionRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.QuestionText.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var questionList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<PersonAnswerQuestionLookupTableDto>();
			foreach(var question in questionList){
				lookupTableDtoList.Add(new PersonAnswerQuestionLookupTableDto
				{
					Id = question.Id,
					DisplayName = question.QuestionText?.ToString()
				});
			}

            return new PagedResultDto<PersonAnswerQuestionLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_Administration_PersonAnswers)]
         public async Task<PagedResultDto<PersonAnswerAnswerLookupTableDto>> GetAllAnswerForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _lookup_answerRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.AnswerText.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var answerList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<PersonAnswerAnswerLookupTableDto>();
			foreach(var answer in answerList){
				lookupTableDtoList.Add(new PersonAnswerAnswerLookupTableDto
				{
					Id = answer.Id,
					DisplayName = answer.AnswerText?.ToString()
				});
			}

            return new PagedResultDto<PersonAnswerAnswerLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}