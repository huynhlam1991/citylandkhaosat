using RASP.AbpTemplate.RSurvey;
using RASP.AbpTemplate.Authorization.Users;


using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using RASP.AbpTemplate.RSurvey.Exporting;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace RASP.AbpTemplate.RSurvey
{
	[AbpAuthorize(AppPermissions.Pages_Administration_SurveyResults)]
    public class SurveyResultsAppService : AbpTemplateAppServiceBase, ISurveyResultsAppService
    {
		 private readonly IRepository<SurveyResult, long> _surveyResultRepository;
		 private readonly ISurveyResultsExcelExporter _surveyResultsExcelExporter;
		 private readonly IRepository<Survey,long> _lookup_surveyRepository;
		 private readonly IRepository<User,long> _lookup_userRepository;
		 

		  public SurveyResultsAppService(IRepository<SurveyResult, long> surveyResultRepository, ISurveyResultsExcelExporter surveyResultsExcelExporter , IRepository<Survey, long> lookup_surveyRepository, IRepository<User, long> lookup_userRepository) 
		  {
			_surveyResultRepository = surveyResultRepository;
			_surveyResultsExcelExporter = surveyResultsExcelExporter;
			_lookup_surveyRepository = lookup_surveyRepository;
		_lookup_userRepository = lookup_userRepository;
		
		  }

		 public async Task<PagedResultDto<GetSurveyResultForViewDto>> GetAll(GetAllSurveyResultsInput input)
         {
			
			var filteredSurveyResults = _surveyResultRepository.GetAll()
						.Include( e => e.SurveyFk)
						.Include( e => e.UserFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Text.Contains(input.Filter) || e.Json.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.TextFilter),  e => e.Text.ToLower() == input.TextFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.JsonFilter),  e => e.Json.ToLower() == input.JsonFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.SurveyTitleFilter), e => e.SurveyFk != null && e.SurveyFk.Title.ToLower() == input.SurveyTitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserFk != null && e.UserFk.Name.ToLower() == input.UserNameFilter.ToLower().Trim());

			var pagedAndFilteredSurveyResults = filteredSurveyResults
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

			var surveyResults = from o in pagedAndFilteredSurveyResults
                         join o1 in _lookup_surveyRepository.GetAll() on o.SurveyId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _lookup_userRepository.GetAll() on o.UserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetSurveyResultForViewDto() {
							SurveyResult = new SurveyResultDto
							{
                                Text = o.Text,
                                Json = o.Json,
                                Id = o.Id,
                                SurveyId = o.SurveyId
							},
                         	SurveyTitle = s1 == null ? "" : s1.Title.ToString(),
                         	UserName = s2 == null ? "" : s2.Name.ToString()
						};

            var totalCount = await filteredSurveyResults.CountAsync();

            return new PagedResultDto<GetSurveyResultForViewDto>(
                totalCount,
                await surveyResults.ToListAsync()
            );
         }
		 
		 public async Task<GetSurveyResultForViewDto> GetSurveyResultForView(long id)
         {
            var surveyResult = await _surveyResultRepository.GetAsync(id);

            var output = new GetSurveyResultForViewDto { SurveyResult = ObjectMapper.Map<SurveyResultDto>(surveyResult) };

		    if (output.SurveyResult.SurveyId != null)
            {
                var _lookupSurvey = await _lookup_surveyRepository.FirstOrDefaultAsync((long)output.SurveyResult.SurveyId);
                output.SurveyTitle = _lookupSurvey.Title.ToString();
            }

		    if (output.SurveyResult.UserId != null)
            {
                var _lookupUser = await _lookup_userRepository.FirstOrDefaultAsync((long)output.SurveyResult.UserId);
                output.UserName = _lookupUser.Name.ToString();
            }
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Administration_SurveyResults_Edit)]
		 public async Task<GetSurveyResultForEditOutput> GetSurveyResultForEdit(EntityDto<long> input)
         {
            var surveyResult = await _surveyResultRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetSurveyResultForEditOutput {SurveyResult = ObjectMapper.Map<CreateOrEditSurveyResultDto>(surveyResult)};

		    if (output.SurveyResult.SurveyId != null)
            {
                var _lookupSurvey = await _lookup_surveyRepository.FirstOrDefaultAsync((long)output.SurveyResult.SurveyId);
                output.SurveyTitle = _lookupSurvey.Title.ToString();
            }

		    if (output.SurveyResult.UserId != null)
            {
                var _lookupUser = await _lookup_userRepository.FirstOrDefaultAsync((long)output.SurveyResult.UserId);
                output.UserName = _lookupUser.Name.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditSurveyResultDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_SurveyResults_Create)]
		 protected virtual async Task Create(CreateOrEditSurveyResultDto input)
         {
            var surveyResult = ObjectMapper.Map<SurveyResult>(input);

			

            await _surveyResultRepository.InsertAsync(surveyResult);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_SurveyResults_Edit)]
		 protected virtual async Task Update(CreateOrEditSurveyResultDto input)
         {
            var surveyResult = await _surveyResultRepository.FirstOrDefaultAsync((long)input.Id);
             ObjectMapper.Map(input, surveyResult);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_SurveyResults_Delete)]
         public async Task Delete(EntityDto<long> input)
         {
            await _surveyResultRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetSurveyResultsToExcel(GetAllSurveyResultsForExcelInput input)
         {
			
			var filteredSurveyResults = _surveyResultRepository.GetAll()
						.Include( e => e.SurveyFk)
						.Include( e => e.UserFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Text.Contains(input.Filter) || e.Json.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.TextFilter),  e => e.Text.ToLower() == input.TextFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.JsonFilter),  e => e.Json.ToLower() == input.JsonFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.SurveyTitleFilter), e => e.SurveyFk != null && e.SurveyFk.Title.ToLower() == input.SurveyTitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserFk != null && e.UserFk.Name.ToLower() == input.UserNameFilter.ToLower().Trim());

			var query = (from o in filteredSurveyResults
                         join o1 in _lookup_surveyRepository.GetAll() on o.SurveyId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _lookup_userRepository.GetAll() on o.UserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetSurveyResultForViewDto() { 
							SurveyResult = new SurveyResultDto
							{
                                Text = o.Text,
                                Json = o.Json,
                                Id = o.Id
							},
                         	SurveyTitle = s1 == null ? "" : s1.Title.ToString(),
                         	UserName = s2 == null ? "" : s2.Name.ToString()
						 });


            var surveyResultListDtos = await query.ToListAsync();

            return _surveyResultsExcelExporter.ExportToFile(surveyResultListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_Administration_SurveyResults)]
         public async Task<PagedResultDto<SurveyResultSurveyLookupTableDto>> GetAllSurveyForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _lookup_surveyRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Title.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var surveyList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<SurveyResultSurveyLookupTableDto>();
			foreach(var survey in surveyList){
				lookupTableDtoList.Add(new SurveyResultSurveyLookupTableDto
				{
					Id = survey.Id,
					DisplayName = survey.Title?.ToString()
				});
			}

            return new PagedResultDto<SurveyResultSurveyLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_Administration_SurveyResults)]
         public async Task<PagedResultDto<SurveyResultUserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _lookup_userRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Name.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var userList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<SurveyResultUserLookupTableDto>();
			foreach(var user in userList){
				lookupTableDtoList.Add(new SurveyResultUserLookupTableDto
				{
					Id = user.Id,
					DisplayName = user.Name?.ToString()
				});
			}

            return new PagedResultDto<SurveyResultUserLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}