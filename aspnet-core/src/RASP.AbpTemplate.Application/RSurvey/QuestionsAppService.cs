using RASP.AbpTemplate.RSurvey;

using RASP.AbpTemplate.RSurvey;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using RASP.AbpTemplate.RSurvey.Exporting;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace RASP.AbpTemplate.RSurvey
{
	[AbpAuthorize(AppPermissions.Pages_Administration_Questions)]
    public class QuestionsAppService : AbpTemplateAppServiceBase, IQuestionsAppService
    {
		 private readonly IRepository<Question, long> _questionRepository;
		 private readonly IQuestionsExcelExporter _questionsExcelExporter;
		 private readonly IRepository<Survey,long> _lookup_surveyRepository;
		 

		  public QuestionsAppService(IRepository<Question, long> questionRepository, IQuestionsExcelExporter questionsExcelExporter , IRepository<Survey, long> lookup_surveyRepository) 
		  {
			_questionRepository = questionRepository;
			_questionsExcelExporter = questionsExcelExporter;
			_lookup_surveyRepository = lookup_surveyRepository;
		
		  }

		 public async Task<PagedResultDto<GetQuestionForViewDto>> GetAll(GetAllQuestionsInput input)
         {
			var questionTypeFilter = (QuestionTypeEnum) input.QuestionTypeFilter;
			
			var filteredQuestions = _questionRepository.GetAll()
						.Include( e => e.SurveyFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.QuestionText.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.QuestionTextFilter),  e => e.QuestionText.ToLower() == input.QuestionTextFilter.ToLower().Trim())
						.WhereIf(input.QuestionTypeFilter > -1, e => e.QuestionType == questionTypeFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.SurveyTitleFilter), e => e.SurveyFk != null && e.SurveyFk.Title.ToLower() == input.SurveyTitleFilter.ToLower().Trim());

			var pagedAndFilteredQuestions = filteredQuestions
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

			var questions = from o in pagedAndFilteredQuestions
                         join o1 in _lookup_surveyRepository.GetAll() on o.SurveyId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetQuestionForViewDto() {
							Question = new QuestionDto
							{
                                QuestionText = o.QuestionText,
                                QuestionType = o.QuestionType,
                                Id = o.Id
							},
                         	SurveyTitle = s1 == null ? "" : s1.Title.ToString()
						};

            var totalCount = await filteredQuestions.CountAsync();

            return new PagedResultDto<GetQuestionForViewDto>(
                totalCount,
                await questions.ToListAsync()
            );
         }
		 
		 public async Task<GetQuestionForViewDto> GetQuestionForView(long id)
         {
            var question = await _questionRepository.GetAsync(id);

            var output = new GetQuestionForViewDto { Question = ObjectMapper.Map<QuestionDto>(question) };

		    if (output.Question.SurveyId != null)
            {
                var _lookupSurvey = await _lookup_surveyRepository.FirstOrDefaultAsync((long)output.Question.SurveyId);
                output.SurveyTitle = _lookupSurvey.Title.ToString();
            }
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Administration_Questions_Edit)]
		 public async Task<GetQuestionForEditOutput> GetQuestionForEdit(EntityDto<long> input)
         {
            var question = await _questionRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetQuestionForEditOutput {Question = ObjectMapper.Map<CreateOrEditQuestionDto>(question)};

		    if (output.Question.SurveyId != null)
            {
                var _lookupSurvey = await _lookup_surveyRepository.FirstOrDefaultAsync((long)output.Question.SurveyId);
                output.SurveyTitle = _lookupSurvey.Title.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditQuestionDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_Questions_Create)]
		 protected virtual async Task Create(CreateOrEditQuestionDto input)
         {
            var question = ObjectMapper.Map<Question>(input);

			

            await _questionRepository.InsertAsync(question);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_Questions_Edit)]
		 protected virtual async Task Update(CreateOrEditQuestionDto input)
         {
            var question = await _questionRepository.FirstOrDefaultAsync((long)input.Id);
             ObjectMapper.Map(input, question);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_Questions_Delete)]
         public async Task Delete(EntityDto<long> input)
         {
            await _questionRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetQuestionsToExcel(GetAllQuestionsForExcelInput input)
         {
			var questionTypeFilter = (QuestionTypeEnum) input.QuestionTypeFilter;
			
			var filteredQuestions = _questionRepository.GetAll()
						.Include( e => e.SurveyFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.QuestionText.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.QuestionTextFilter),  e => e.QuestionText.ToLower() == input.QuestionTextFilter.ToLower().Trim())
						.WhereIf(input.QuestionTypeFilter > -1, e => e.QuestionType == questionTypeFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.SurveyTitleFilter), e => e.SurveyFk != null && e.SurveyFk.Title.ToLower() == input.SurveyTitleFilter.ToLower().Trim());

			var query = (from o in filteredQuestions
                         join o1 in _lookup_surveyRepository.GetAll() on o.SurveyId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetQuestionForViewDto() { 
							Question = new QuestionDto
							{
                                QuestionText = o.QuestionText,
                                QuestionType = o.QuestionType,
                                Id = o.Id
							},
                         	SurveyTitle = s1 == null ? "" : s1.Title.ToString()
						 });


            var questionListDtos = await query.ToListAsync();

            return _questionsExcelExporter.ExportToFile(questionListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_Administration_Questions)]
         public async Task<PagedResultDto<QuestionSurveyLookupTableDto>> GetAllSurveyForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _lookup_surveyRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Title.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var surveyList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<QuestionSurveyLookupTableDto>();
			foreach(var survey in surveyList){
				lookupTableDtoList.Add(new QuestionSurveyLookupTableDto
				{
					Id = survey.Id,
					DisplayName = survey.Title?.ToString()
				});
			}

            return new PagedResultDto<QuestionSurveyLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}