using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using RASP.AbpTemplate.DataExporting.Excel.EpPlus;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;
using RASP.AbpTemplate.Storage;

namespace RASP.AbpTemplate.RSurvey.Exporting
{
    public class QuestionsExcelExporter : EpPlusExcelExporterBase, IQuestionsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public QuestionsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetQuestionForViewDto> questions)
        {
            return CreateExcelPackage(
                "Questions.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Questions"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("QuestionText"),
                        L("QuestionType"),
                        (L("Survey")) + L("Title")
                        );

                    AddObjects(
                        sheet, 2, questions,
                        _ => _.Question.QuestionText,
                        _ => _.Question.QuestionType,
                        _ => _.SurveyTitle
                        );

					

                });
        }
    }
}
