using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using RASP.AbpTemplate.DataExporting.Excel.EpPlus;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;
using RASP.AbpTemplate.Storage;

namespace RASP.AbpTemplate.RSurvey.Exporting
{
    public class SurveyResultsExcelExporter : EpPlusExcelExporterBase, ISurveyResultsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public SurveyResultsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetSurveyResultForViewDto> surveyResults)
        {
            return CreateExcelPackage(
                "SurveyResults.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("SurveyResults"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Text"),
                        L("Json"),
                        (L("Survey")) + L("Title"),
                        (L("User")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, surveyResults,
                        _ => _.SurveyResult.Text,
                        _ => _.SurveyResult.Json,
                        _ => _.SurveyTitle,
                        _ => _.UserName
                        );

					

                });
        }
    }
}
