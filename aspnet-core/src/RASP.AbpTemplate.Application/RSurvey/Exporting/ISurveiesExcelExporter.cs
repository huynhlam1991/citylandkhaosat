using System.Collections.Generic;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RSurvey.Exporting
{
    public interface ISurveiesExcelExporter
    {
        FileDto ExportToFile(List<GetSurveyForViewDto> surveies, long? surveyId);
        FileDto ExportToFileDetail(List<GetSurveyForViewDto> surveies, long? surveyId);
        FileDto ExportSurveyResultToFile(ExportSurvey surveies);
    }
}