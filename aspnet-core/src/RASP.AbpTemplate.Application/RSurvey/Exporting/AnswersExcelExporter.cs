using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using RASP.AbpTemplate.DataExporting.Excel.EpPlus;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;
using RASP.AbpTemplate.Storage;

namespace RASP.AbpTemplate.RSurvey.Exporting
{
    public class AnswersExcelExporter : EpPlusExcelExporterBase, IAnswersExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public AnswersExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetAnswerForViewDto> answers)
        {
            return CreateExcelPackage(
                "Answers.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Answers"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("AnswerText"),
                        (L("Question")) + L("QuestionText")
                        );

                    AddObjects(
                        sheet, 2, answers,
                        _ => _.Answer.AnswerText,
                        _ => _.QuestionQuestionText
                        );

					

                });
        }
    }
}
