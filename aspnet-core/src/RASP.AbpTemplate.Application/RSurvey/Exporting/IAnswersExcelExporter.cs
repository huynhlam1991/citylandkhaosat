using System.Collections.Generic;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RSurvey.Exporting
{
    public interface IAnswersExcelExporter
    {
        FileDto ExportToFile(List<GetAnswerForViewDto> answers);
    }
}