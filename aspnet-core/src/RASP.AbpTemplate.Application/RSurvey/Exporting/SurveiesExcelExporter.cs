﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using RASP.AbpTemplate.DataExporting.Excel.EpPlus;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;
using RASP.AbpTemplate.Storage;
using Abp.Domain.Repositories;
using Abp.Domain.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace RASP.AbpTemplate.RSurvey.Exporting
{
    public class SurveiesExcelExporter : EpPlusExcelExporterBase, ISurveiesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly IRepository<Survey, long> _surveyRepository;
        private readonly IRepository<SurveyResult, long> _surveyResultRepository;
        public SurveiesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager,
            IRepository<Survey, long> surveyRepository,
            IRepository<SurveyResult, long> surveyResultRepository) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _surveyRepository = surveyRepository;
            _surveyResultRepository = surveyResultRepository;
        }

        public FileDto ExportToFile(List<GetSurveyForViewDto> surveies, long? surveyId)
        {
            if (surveyId.HasValue)
            {
                int startRow = 1;
                var survey = _surveyRepository.FirstOrDefault(x => x.Id == surveyId.Value);
                var jsonSurvey = survey.GetData<string>("SurveyStorage");
                var SurveyData = JsonConvert.DeserializeObject<CustomSurveyResultDto>(jsonSurvey);
                
                //Phòng ban
                var phongban = SurveyData.pages[0].elements[0].choices.Select(x => x).ToList();
               
                //Câu trả lời
                var surveyResult = _surveyResultRepository.GetAll().Where(x => x.SurveyId == surveyId.Value).ToList();
                //Get Đáp án 
                List<CustomSurveyAnswerDto> ResultAnswer = new List<CustomSurveyAnswerDto>();
                foreach (var res in surveyResult)
                {
                    var jsonsurveyResult = res.GetData<string>("ResultsStorage");
                    var answerData = (JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(jsonsurveyResult)).ToList();
                   

                    string objPhongBan = JsonConvert.SerializeObject(answerData[0].Value, Formatting.Indented);
                    List<string> arrPhongBan = (JsonConvert.DeserializeObject<List<string>>(objPhongBan));
                    var listSelectPhongBan = phongban.Where(x => arrPhongBan.Contains(x.value)).Select(x => x.text).ToList();
                   
                    foreach (var ans in answerData.ToList().Skip(0))
                    {
                       
                        CustomSurveyAnswerDto tempAns = new CustomSurveyAnswerDto
                        {
                            QuestionTitle = ans.Key,
                            CreatedDate = res.CreationTime.ToString("dd/MM/yyyy HH:mm"),
                            Email = res.Text
                        };
                        try
                        {
                            string objMatrix = JsonConvert.SerializeObject(ans.Value, Formatting.Indented);
                            var arrMatrix = (JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(objMatrix));
                            if (arrMatrix != null)
                            {
                                var max = arrMatrix.Select(x => new MatrixSurvey { Column = x.Key, Value = x.Value, Row = x.Value }).ToList();
                                tempAns.Matrixs.AddRange(max);
                            }
                        }
                        catch
                        {

                        }
                        try
                        {
                            string objOption = JsonConvert.SerializeObject(ans.Value, Formatting.Indented);
                            var arrOption = (JsonConvert.DeserializeObject<List<string>>(objOption));
                            if (arrOption != null && arrOption.Any())
                            {

                                tempAns.Options.AddRange(arrOption);
                            }
                        }
                        catch
                        {
                            //Trường hợp đáp án không phải matrix chỉ là radiogroup 
                            try
                            {
                                string objRadiogroup = JsonConvert.SerializeObject(ans.Value, Formatting.Indented);
                                var arrRadiogroup = (JsonConvert.DeserializeObject<string>(objRadiogroup));
                                if (arrRadiogroup != null && arrRadiogroup.Any())
                                {

                                    tempAns.Text = arrRadiogroup;
                                }
                            }
                            catch
                            {

                            }
                        }
                        ResultAnswer.Add(tempAns);
                    }
                }
                var file = CreateExcelPackage(
                "Surveies.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Surveies"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                       phongban.Select(x => x.text).ToArray()
                        );
                   
                    //AddQuestion

                    foreach (var item in SurveyData.pages.Skip(1))
                    {
                        #region In ra câu hỏi
                        //Draw question
                        foreach (var ele in item.elements)
                        {

                            //Xuống dòng
                            startRow = startRow + 1;

                            AddObjects(
                           sheet, startRow, new List<string> { "" },
                           _ => ele.title
                           );
                            sheet.Cells[startRow, 1, startRow, phongban.Count + 1].Merge = true;
                            sheet.Cells[startRow, 1, startRow, phongban.Count + 1].Style.Font.Italic = true;
                            #region In ra lựa chọn của câu hỏi
                            //Draw option
                            //If maxtrix
                            if (ele.type == "matrix" && ele.columns.Any())
                            {
                                foreach (var col in ele.columns)
                                {
                                    //Xuống dòng
                                    startRow = startRow + 1;
                                    AddObjects(
                                   sheet, startRow, new List<string> { "" },
                                   _ => col
                                   );
                                    sheet.Cells[startRow, 1].Style.Font.Bold = true;
                                    int column = 2;
                                    var selectQues = ResultAnswer.Where(x => x.QuestionTitle == ele.name).ToList();
                                    for (int i = 0;i< phongban.Count;i++)
                                    {
                                       
                                        
                                        var countPointPhongBan = selectQues.Count(x => 
                                        x.Matrixs.Any(y => y.Column == phongban[i].text && y.Row == col)
                                        
                                        );
                                        sheet.Cells[startRow, column].Value = countPointPhongBan;
                                        sheet.Cells[startRow, column].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        sheet.Cells[startRow, column].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                        column++;
                                    }
                                    
                                }
                            }
                            //If radiogroup
                            if (ele.type == "radiogroup" || ele.type == "checkbox" && ele.choices.Any())
                            {
                                foreach (var choi in ele.choices)
                                {
                                    //Xuống dòng
                                    startRow = startRow + 1;
                                    AddObjects(
                                   sheet, startRow, new List<string> { "" },
                                   _ => choi.text
                                   );
                                    sheet.Cells[startRow, 1].Style.Font.Bold = true;
                                    int column = 2;
                                    for (int i = 0; i < phongban.Count; i++)
                                    {
                                        var selectQues = ResultAnswer.Where(x => x.QuestionTitle == ele.name).ToList();
                                        
                                        var countPointPhongBan = selectQues.Count(x =>
                                       x.Options.Any(y => y == choi.value) || (!string.IsNullOrEmpty(x.Text) && x.Text == choi.value)
                                       );
                                        //Get phongban from visibleIf
                                        var questionCurrent = selectQues.FirstOrDefault();

                                        var pbvisible = phongban.FirstOrDefault(x => questionCurrent != null && x.value == ele.visibleIf);
                                        //Get phongban đã chọn ở câu 1
                                        var selectcau1 = ResultAnswer[0].Options;
                                        if ((pbvisible!= null && pbvisible.value != phongban[i].value) || (selectcau1.Any() && !selectcau1.Contains(phongban[i].value)))
                                        {
                                            countPointPhongBan = 0;
                                        }

                                        sheet.Cells[startRow, column].Value = countPointPhongBan;
                                        sheet.Cells[startRow, column].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        sheet.Cells[startRow, column].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                        //if(countPointPhongBan > 0)
                                        //{
                                        //    //sheet.Cells[startRow, column].Style.Fill.PatternColor.SetColor(System.Drawing.Color.Brown);
                                        //    sheet.Cells[startRow, column].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                        //    sheet.Cells[startRow, column].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightYellow);
                                        //}
                                        column++;
                                    }
                                }
                            }
                            #endregion


                        }
                        #endregion
                    }

                    

                });
                return file;
            }


            return CreateExcelPackage(
                "Surveies.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Surveies"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Title"),
                        L("Description"),
                        L("StartDate"),
                        L("EndDate"),
                        L("IsAvailable"),
                        L("Text")
                        );

                    AddObjects(
                        sheet, 2, surveies,
                        _ => _.Survey.Title,
                        _ => _.Survey.Description,
                        _ => _timeZoneConverter.Convert(_.Survey.StartDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _timeZoneConverter.Convert(_.Survey.EndDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.Survey.IsAvailable,
                        _ => _.Survey.Text
                        );

                    //var startDateColumn = sheet.Column(3);
                    //               startDateColumn.Style.Numberformat.Format = "yyyy-mm-dd";
                    //startDateColumn.AutoFit();
                    //var endDateColumn = sheet.Column(4);
                    //               endDateColumn.Style.Numberformat.Format = "yyyy-mm-dd";
                    //endDateColumn.AutoFit();


                });
        }
        public FileDto ExportToFileDetail(List<GetSurveyForViewDto> surveies, long? surveyId)
        {
            if (surveyId.HasValue)
            {
                int startRow = 1;
                int startColumn = 3;
                var survey = _surveyRepository.FirstOrDefault(x => x.Id == surveyId.Value);
                var jsonSurvey = survey.GetData<string>("SurveyStorage");
                var SurveyData = JsonConvert.DeserializeObject<CustomSurveyResultDto>(jsonSurvey);

                //Phòng ban
                var phongban = SurveyData.pages[0].elements[0].choices.Select(x => x).ToList();
               
                //Câu trả lời
                var surveyResult = _surveyResultRepository.GetAll().Where(x => x.SurveyId == surveyId.Value).ToList();
                //Get Đáp án 
                List<CustomSurveyAnswerDto> ResultAnswer = new List<CustomSurveyAnswerDto>();
                foreach (var res in surveyResult)
                {
                    var jsonsurveyResult = res.GetData<string>("ResultsStorage");
                    var answerData = (JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(jsonsurveyResult)).ToList();


                    string objPhongBan = JsonConvert.SerializeObject(answerData[0].Value, Formatting.Indented);
                    List<string> arrPhongBan = (JsonConvert.DeserializeObject<List<string>>(objPhongBan));
                    var listSelectPhongBan = phongban.Where(x => arrPhongBan.Contains(x.value)).Select(x => x.text).ToList();

                    foreach (var ans in answerData.ToList().Skip(1))
                    {

                        CustomSurveyAnswerDto tempAns = new CustomSurveyAnswerDto
                        {
                            QuestionTitle = ans.Key,
                            CreatedDate = res.CreationTime.ToString("dd/MM/yyyy HH:mm"),
                            Email = res.Text
                        };
                        try
                        {
                            string objMatrix = JsonConvert.SerializeObject(ans.Value, Formatting.Indented);
                            var arrMatrix = (JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(objMatrix));
                            if (arrMatrix != null)
                            {
                                var max = arrMatrix.Select(x => new MatrixSurvey { Column = x.Key, Value = x.Value, Row = x.Value }).ToList();
                                tempAns.Matrixs.AddRange(max);
                            }
                        }
                        catch
                        {

                        }
                        try
                        {
                            string objOption = JsonConvert.SerializeObject(ans.Value, Formatting.Indented);
                            var arrOption = (JsonConvert.DeserializeObject<List<string>>(objOption));
                            if (arrOption != null && arrOption.Any())
                            {

                                tempAns.Options.AddRange(arrOption);
                            }
                        }
                        catch
                        {

                        }
                        ResultAnswer.Add(tempAns);
                    }
                }
                var file = CreateExcelPackage(
                "SurveiesDetail.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("SurveiesDetail"));
                    sheet.OutLineApplyStyle = true;
                    var newArrPb = new List<string>() { "" };
                    newArrPb.AddRange(phongban.Select(x => x.text));
                    AddHeader(
                        sheet,
                       newArrPb.ToArray()
                        );

                    //AddQuestion

                    foreach (var item in SurveyData.pages.Skip(1))
                    {
                        #region In ra câu hỏi
                        //Draw question
                        foreach (var ele in item.elements)
                        {

                            //Xuống dòng
                            startRow = startRow + 1;

                            AddObjects(
                           sheet, startRow, new List<string> { "" },
                           _ => ele.title
                           );
                            sheet.Cells[startRow, 1, startRow, phongban.Count + 1].Merge = false;
                            sheet.Cells[startRow, 1, startRow, phongban.Count + 1].Style.Font.Italic = true;
                            #region In ra lựa chọn của câu hỏi
                            //Draw option
                            //If maxtrix
                            if (ele.type == "matrix" && ele.columns.Any())
                            {
                                foreach (var col in ele.columns)
                                {
                                   // //Xuống dòng
                                   // startRow = startRow + 1;
                                   // AddObjects(
                                   //sheet, startRow, new List<string> { "" },
                                   //_ => col
                                   //);
                                   // sheet.Cells[startRow, 1].Style.Font.Bold = true;
                                    int column = startColumn;
                                    var selectQues = ResultAnswer.Where(x => x.QuestionTitle == ele.name).ToList();
                                    //for (int i = 0; i < phongban.Count; i++)
                                    //{


                                    //    var countPointPhongBan = selectQues.Count(x =>
                                    //    x.Matrixs.Any(y => y.Column == phongban[i].text && y.Row == col)

                                    //    );
                                    //    sheet.Cells[startRow, column].Value = countPointPhongBan;
                                    //    sheet.Cells[startRow, column].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                    //    sheet.Cells[startRow, column].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    //    column++;
                                    //}
                                    foreach (var detailAnswer in selectQues)
                                    {
                                        var checkAnswer = selectQues.FirstOrDefault(x =>
                                            x.Matrixs.Any(y => y.Row == col && x.Email == detailAnswer.Email)

                                            );
                                        if (checkAnswer != null)
                                        {
                                            //Xuống dòng
                                            startRow = startRow + 1;
                                            AddObjects(
                                           sheet, startRow, new List<string> { "" },
                                           _ => detailAnswer.CreatedDate,
                                           _ => detailAnswer.Email
                                           );
                                        }
                                        
                                        column = startColumn;
                                        for (int i = 0; i < phongban.Count; i++)
                                        {


                                            var countPointPhongBan = selectQues.FirstOrDefault(x =>
                                            x.Matrixs.Any(y => y.Column == phongban[i].text && y.Row == col && x.Email == detailAnswer.Email)

                                            );
                                            if (countPointPhongBan != null)
                                            {
                                                sheet.Cells[startRow, column].Value = col;
                                                sheet.Cells[startRow, column].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                                sheet.Cells[startRow, column].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                            }

                                            column++;
                                        }
                                    }
                                }
                            }
                            //If radiogroup
                            if (ele.type == "radiogroup" || ele.type == "checkbox" && ele.choices.Any())
                            {
                                foreach (var choi in ele.choices)
                                {
                                   // //Xuống dòng
                                   // startRow = startRow + 1;
                                   // AddObjects(
                                   //sheet, startRow, new List<string> { "" },
                                   //_ => choi.text
                                   //);
                                   // sheet.Cells[startRow, 1].Style.Font.Bold = true;
                                    int column = startColumn;
                                    var selectQues = ResultAnswer.Where(x => x.QuestionTitle == ele.name).ToList();
                                    //for (int i = 0; i < phongban.Count; i++)
                                    //{
                                       

                                    //    var countPointPhongBan = selectQues.Count(x =>
                                    //    x.Options.Any(y => y == choi.value)
                                    //    );
                                    //    sheet.Cells[startRow, column].Value = countPointPhongBan;
                                    //    sheet.Cells[startRow, column].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    //    sheet.Cells[startRow, column].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                    //    //if(countPointPhongBan > 0)
                                    //    //{
                                    //    //    //sheet.Cells[startRow, column].Style.Fill.PatternColor.SetColor(System.Drawing.Color.Brown);
                                    //    //    sheet.Cells[startRow, column].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                    //    //    sheet.Cells[startRow, column].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightYellow);
                                    //    //}
                                    //    column++;
                                    //}
                                    foreach (var detailAnswer in selectQues)
                                    {
                                        var countPointPhongBan = selectQues.FirstOrDefault(x =>
                                       x.Options.Any(y => y == choi.value) && x.Email == detailAnswer.Email);
                                        if (countPointPhongBan != null)
                                        {
                                            //Xuống dòng
                                            startRow = startRow + 1;
                                            AddObjects(
                                           sheet, startRow, new List<string> { "" },
                                           _ => detailAnswer.CreatedDate,
                                           _ => detailAnswer.Email
                                           );
                                            column = startColumn;
                                            for (int i = 0; i < phongban.Count; i++)
                                            {



                                                if (countPointPhongBan != null)
                                                {
                                                    sheet.Cells[startRow, column].Value = !string.IsNullOrEmpty(choi.text) ? choi.text : choi.value;
                                                    sheet.Cells[startRow, column].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                                    sheet.Cells[startRow, column].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                                }

                                                column++;
                                            }
                                        }
                                       
                                    }
                                }
                            }
                            #endregion


                        }
                        #endregion
                    }



                });
                return file;
            }


            return CreateExcelPackage(
                "Surveies.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Surveies"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Title"),
                        L("Description"),
                        L("StartDate"),
                        L("EndDate"),
                        L("IsAvailable"),
                        L("Text")
                        );

                    AddObjects(
                        sheet, 2, surveies,
                        _ => _.Survey.Title,
                        _ => _.Survey.Description,
                        _ => _timeZoneConverter.Convert(_.Survey.StartDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _timeZoneConverter.Convert(_.Survey.EndDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.Survey.IsAvailable,
                        _ => _.Survey.Text
                        );

                    //var startDateColumn = sheet.Column(3);
                    //               startDateColumn.Style.Numberformat.Format = "yyyy-mm-dd";
                    //startDateColumn.AutoFit();
                    //var endDateColumn = sheet.Column(4);
                    //               endDateColumn.Style.Numberformat.Format = "yyyy-mm-dd";
                    //endDateColumn.AutoFit();


                });
        }
        private static bool IsValidJson(string strInput)
        {
            if (string.IsNullOrWhiteSpace(strInput)) { return false; }
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    //Exception in parsing json

                    return false;
                }
                catch (Exception ex) //some other exception
                {

                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public FileDto ExportSurveyResultToFile(ExportSurvey surveies)
        {
            DataTable dt = new DataTable();
            dt.Clear();
            foreach (var title in surveies.Titles)
            {
                dt.Columns.Add(title.Name);

            }

            var listEmail = surveies.answer.Select(x => x.Email).Distinct();
            foreach (var email in listEmail)
            {
                foreach (var title in surveies.Titles)
                {

                    var answerQuestion = surveies.answer.Where(x => x.Email == email)
                        .ToList();
                    var rowCount = 0;
                    foreach (var answer in answerQuestion)
                    {
                        DataRow _ravi = dt.NewRow();
                        foreach (var title1 in surveies.Titles)
                        {
                            //_ravi[title1.Name] = 
                        }
                    }
                    //var row = 0;
                    //foreach (var title1 in surveies.Titles)
                    //{
                    //    DataRow _ravi = dt.NewRow();
                    //    foreach (var answer in answerQuestion.Where(x=>x.questionKey == title1.Name))
                    //    {
                    //        _ravi[answer.questionKey] = answer.AnswerText;
                    //        break;
                    //    }
                    //}


                }
                //foreach (var answerQuestion in surveies.answer.Where(x=>x.Email == email))
                //{
                //    DataRow _ravi = dt.NewRow();
                //    _ravi[answerQuestion.questionText.Trim().Replace("\\n", " ")] = answerQuestion.AnswerText;
                //    dt.Rows.Add(_ravi);
                //}
            }




            return CreateExcelPackage(
                "Surveies.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Surveies"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        surveies.Titles.Select(x => x.Title).ToArray()
                        );

                    //AddObjects(
                    //    sheet, 2, surveies.answer,
                    //    _ => _..Title,
                    //    _ => _.Survey.Description,
                    //    _ => _timeZoneConverter.Convert(_.Survey.StartDate, _abpSession.TenantId, _abpSession.GetUserId()),
                    //    _ => _timeZoneConverter.Convert(_.Survey.EndDate, _abpSession.TenantId, _abpSession.GetUserId()),
                    //    _ => _.Survey.IsAvailable,
                    //    _ => _.Survey.Text
                    //    );

                    //var startDateColumn = sheet.Column(3);
                    //startDateColumn.Style.Numberformat.Format = "yyyy-mm-dd";
                    //startDateColumn.AutoFit();
                    //var endDateColumn = sheet.Column(4);
                    //endDateColumn.Style.Numberformat.Format = "yyyy-mm-dd";
                    //endDateColumn.AutoFit();


                });
        }
    }
}
