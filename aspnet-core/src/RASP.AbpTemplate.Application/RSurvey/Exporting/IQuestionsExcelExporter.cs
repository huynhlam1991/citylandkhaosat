using System.Collections.Generic;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RSurvey.Exporting
{
    public interface IQuestionsExcelExporter
    {
        FileDto ExportToFile(List<GetQuestionForViewDto> questions);
    }
}