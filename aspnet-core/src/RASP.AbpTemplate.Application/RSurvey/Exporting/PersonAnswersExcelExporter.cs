using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using RASP.AbpTemplate.DataExporting.Excel.EpPlus;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.Dto;
using RASP.AbpTemplate.Storage;

namespace RASP.AbpTemplate.RSurvey.Exporting
{
    public class PersonAnswersExcelExporter : EpPlusExcelExporterBase, IPersonAnswersExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PersonAnswersExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetPersonAnswerForViewDto> personAnswers)
        {
            return CreateExcelPackage(
                "PersonAnswers.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("PersonAnswers"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("OtherText"),
                        (L("Survey")) + L("Title"),
                        (L("Question")) + L("QuestionText"),
                        (L("Answer")) + L("AnswerText")
                        );

                    AddObjects(
                        sheet, 2, personAnswers,
                        _ => _.PersonAnswer.OtherText,
                        _ => _.SurveyTitle,
                        _ => _.QuestionQuestionText,
                        _ => _.AnswerAnswerText
                        );

					

                });
        }
    }
}
