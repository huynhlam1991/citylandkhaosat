﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using RASP.AbpTemplate.Authorization;

namespace RASP.AbpTemplate
{
    /// <summary>
    /// Application layer module of the application.
    /// </summary>
    [DependsOn(
        typeof(AbpTemplateCoreModule)
        )]
    public class AbpTemplateApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Adding authorization providers
            Configuration.Authorization.Providers.Add<AppAuthorizationProvider>();

            //Adding custom AutoMapper configuration
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.CreateMappings);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(AbpTemplateApplicationModule).GetAssembly());
        }
    }
}