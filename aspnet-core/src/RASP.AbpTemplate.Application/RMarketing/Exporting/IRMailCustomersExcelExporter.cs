using System.Collections.Generic;
using RASP.AbpTemplate.RMarketing.Dtos;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.RMarketing.Exporting
{
    public interface IRMailCustomersExcelExporter
    {
        FileDto ExportToFile(List<GetRMailCustomerForViewDto> rMailCustomers);
    }
}