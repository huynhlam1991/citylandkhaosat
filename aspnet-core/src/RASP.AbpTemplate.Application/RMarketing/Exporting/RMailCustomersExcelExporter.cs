using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using RASP.AbpTemplate.DataExporting.Excel.EpPlus;
using RASP.AbpTemplate.RMarketing.Dtos;
using RASP.AbpTemplate.Dto;
using RASP.AbpTemplate.Storage;

namespace RASP.AbpTemplate.RMarketing.Exporting
{
    public class RMailCustomersExcelExporter : EpPlusExcelExporterBase, IRMailCustomersExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RMailCustomersExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRMailCustomerForViewDto> rMailCustomers)
        {
            return CreateExcelPackage(
                "RMailCustomers.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("RMailCustomers"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Email"),
                        L("PrivateCode"),
                        L("PhoneNumber")
                        );

                    AddObjects(
                        sheet, 2, rMailCustomers,
                        _ => _.RMailCustomer.Name,
                        _ => _.RMailCustomer.Email,
                        _ => _.RMailCustomer.PrivateCode,
                        _ => _.RMailCustomer.PhoneNumber
                        );

					

                });
        }
    }
}
