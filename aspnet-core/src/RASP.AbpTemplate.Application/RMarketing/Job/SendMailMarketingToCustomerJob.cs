﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Net.Mail;
using RASP.AbpTemplate.Authorization.Users.Dto;
using RASP.AbpTemplate.RMarketing.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RASP.AbpTemplate.RMarketing.Job
{
    public class SendMailMarketingToCustomerJob : BackgroundJob<MailSendInputArgs>, ITransientDependency
    {
        private readonly IRepository<RMailCustomer, Guid> _rMailCustomerRepository;
        private readonly IEmailSender _emailSender;
        public SendMailMarketingToCustomerJob(IRepository<RMailCustomer, Guid> rMailCustomerRepository, IEmailSender emailSender)
        {
            _rMailCustomerRepository = rMailCustomerRepository;
            _emailSender = emailSender;
        }
        [UnitOfWork]
        public override void Execute(MailSendInputArgs args)
        {
            using (var unitOfWork = UnitOfWorkManager.Begin())
            {
                _emailSender.Send(args.To, args.Subject, args.Body, args.IsBodyHtml);
                unitOfWork.Complete();
            }
        }
    }
}
