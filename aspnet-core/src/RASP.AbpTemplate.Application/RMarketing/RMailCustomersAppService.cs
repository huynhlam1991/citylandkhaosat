﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using RASP.AbpTemplate.RMarketing.Exporting;
using RASP.AbpTemplate.RMarketing.Dtos;
using RASP.AbpTemplate.Dto;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.Net.Mail;
using Abp.BackgroundJobs;
using RASP.AbpTemplate.RMarketing.Dto;
using RASP.AbpTemplate.RMarketing.Job;
using System.Web;

namespace RASP.AbpTemplate.RMarketing
{
	[AbpAuthorize(AppPermissions.Pages_Administration_RMailCustomers)]
    public class RMailCustomersAppService : AbpTemplateAppServiceBase, IRMailCustomersAppService
    {
		 private readonly IRepository<RMailCustomer, Guid> _rMailCustomerRepository;
		 private readonly IRMailCustomersExcelExporter _rMailCustomersExcelExporter;
        protected readonly IBackgroundJobManager BackgroundJobManager;

		  public RMailCustomersAppService(IRepository<RMailCustomer, Guid> rMailCustomerRepository, IRMailCustomersExcelExporter rMailCustomersExcelExporter, IBackgroundJobManager backgroundJobManager) 
		  {
			_rMailCustomerRepository = rMailCustomerRepository;
			_rMailCustomersExcelExporter = rMailCustomersExcelExporter;
            BackgroundJobManager = backgroundJobManager;
          }

		 public async Task<PagedResultDto<GetRMailCustomerForViewDto>> GetAll(GetAllRMailCustomersInput input)
         {
			
			var filteredRMailCustomers = _rMailCustomerRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.PrivateCode.Contains(input.Filter) || e.ContentMail.Contains(input.Filter) || e.PhoneNumber.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name.ToLower() == input.NameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter),  e => e.Email.ToLower() == input.EmailFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.PrivateCodeFilter),  e => e.PrivateCode.ToLower() == input.PrivateCodeFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ContentMailFilter),  e => e.ContentMail.ToLower() == input.ContentMailFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.PhoneNumberFilter),  e => e.PhoneNumber.ToLower() == input.PhoneNumberFilter.ToLower().Trim());

            var pagedAndFilteredRMailCustomers = filteredRMailCustomers
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

			var rMailCustomers = from o in pagedAndFilteredRMailCustomers
                         select new GetRMailCustomerForViewDto() {
							RMailCustomer = new RMailCustomerDto
							{
                                Name = o.Name,
                                Email = o.Email,
                                PrivateCode = o.PrivateCode,
                                PhoneNumber = o.PhoneNumber,
                                Id = o.Id
							}
						};

            var totalCount = await filteredRMailCustomers.CountAsync();

            return new PagedResultDto<GetRMailCustomerForViewDto>(
                totalCount,
                await rMailCustomers.ToListAsync()
            );
         }
        private async Task<PagedResultDto<GetRMailCustomerForViewDto>> GetAllToSendMail(GetAllRMailCustomersInput input)
        {

            var filteredRMailCustomers = _rMailCustomerRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.PrivateCode.Contains(input.Filter) || e.ContentMail.Contains(input.Filter) || e.PhoneNumber.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name.ToLower() == input.NameFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email.ToLower() == input.EmailFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PrivateCodeFilter), e => e.PrivateCode.ToLower() == input.PrivateCodeFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ContentMailFilter), e => e.ContentMail.ToLower() == input.ContentMailFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneNumberFilter), e => e.PhoneNumber.ToLower() == input.PhoneNumberFilter.ToLower().Trim());

            var pagedAndFilteredRMailCustomers = filteredRMailCustomers
                .OrderBy(input.Sorting ?? "id asc");
            //.PageBy(input);

            var rMailCustomers = from o in pagedAndFilteredRMailCustomers
                                 select new GetRMailCustomerForViewDto()
                                 {
                                     RMailCustomer = new RMailCustomerDto
                                     {
                                         Name = o.Name,
                                         Email = o.Email,
                                         PrivateCode = o.PrivateCode,
                                         PhoneNumber = o.PhoneNumber,
                                         Id = o.Id
                                     }
                                 };

            var totalCount = await filteredRMailCustomers.CountAsync();

            return new PagedResultDto<GetRMailCustomerForViewDto>(
                totalCount,
                await rMailCustomers.ToListAsync()
            );
        }
        public async Task<GetRMailCustomerForViewDto> GetRMailCustomerForView(Guid id)
         {
            var rMailCustomer = await _rMailCustomerRepository.GetAsync(id);

            var output = new GetRMailCustomerForViewDto { RMailCustomer = ObjectMapper.Map<RMailCustomerDto>(rMailCustomer) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Administration_RMailCustomers_Edit)]
		 public async Task<GetRMailCustomerForEditOutput> GetRMailCustomerForEdit(EntityDto<Guid> input)
         {
            var rMailCustomer = await _rMailCustomerRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetRMailCustomerForEditOutput {RMailCustomer = ObjectMapper.Map<CreateOrEditRMailCustomerDto>(rMailCustomer)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditRMailCustomerDto input)
         {
            input.Email = input.Email.Trim();
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_RMailCustomers_Create)]
		 protected virtual async Task Create(CreateOrEditRMailCustomerDto input)
         {
            var rMailCustomer = ObjectMapper.Map<RMailCustomer>(input);

			

            await _rMailCustomerRepository.InsertAsync(rMailCustomer);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_RMailCustomers_Edit)]
		 protected virtual async Task Update(CreateOrEditRMailCustomerDto input)
         {
            var rMailCustomer = await _rMailCustomerRepository.FirstOrDefaultAsync((Guid)input.Id);
             ObjectMapper.Map(input, rMailCustomer);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_RMailCustomers_Delete)]
         public async Task Delete(EntityDto<Guid> input)
         {
            await _rMailCustomerRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetRMailCustomersToExcel(GetAllRMailCustomersForExcelInput input)
         {
			
			var filteredRMailCustomers = _rMailCustomerRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.PrivateCode.Contains(input.Filter) || e.ContentMail.Contains(input.Filter) || e.PhoneNumber.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name.ToLower() == input.NameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter),  e => e.Email.ToLower() == input.EmailFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.PrivateCodeFilter),  e => e.PrivateCode.ToLower() == input.PrivateCodeFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ContentMailFilter),  e => e.ContentMail.ToLower() == input.ContentMailFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.PhoneNumberFilter),  e => e.PhoneNumber.ToLower() == input.PhoneNumberFilter.ToLower().Trim());

			var query = (from o in filteredRMailCustomers
                         select new GetRMailCustomerForViewDto() { 
							RMailCustomer = new RMailCustomerDto
							{
                                Name = o.Name,
                                Email = o.Email,
                                PrivateCode = o.PrivateCode,
                                PhoneNumber = o.PhoneNumber,
                                Id = o.Id
							}
						 });


            var rMailCustomerListDtos = await query.ToListAsync();

            return _rMailCustomersExcelExporter.ExportToFile(rMailCustomerListDtos);
         }
        public async Task SendMailMarketing(GetAllRMailCustomersInput input)
        {
            var customers = await GetAllToSendMail(input);
            if (customers.Items.Any())
            {
                if (string.IsNullOrEmpty(input.Subject))
                {
                    input.Subject = "Phiếu lấy ý kiến đánh giá";
                }
                foreach (var item in customers.Items)
                {
                    var linkStyle = "<p style='text-align:center;margin-top:40px'><a href='" + input.Domain + "&cpo=" + item.RMailCustomer.Id.ToString().ToLower() + "' style='text-decoration:none;color:black;border-radius:3px;padding:14px 22px 9px;background-color:#71b7e6;font-size:16px;border-bottom:4px solid #4292dd;text-transform:uppercase;color:#ffffff' target='_blank'>Thực hiện khảo sát</a></p>";
                    await BackgroundJobManager.EnqueueAsync<SendMailMarketingToCustomerJob, MailSendInputArgs>(new MailSendInputArgs
                    {
                        To = item.RMailCustomer.Email,
                        Subject = input.Subject,
                        Body = input.ContentMail + "<br>" + linkStyle,
                        IsBodyHtml = true
                    });
                }
            }
        }
    }
}