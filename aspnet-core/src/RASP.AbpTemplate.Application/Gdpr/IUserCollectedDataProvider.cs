﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.Gdpr
{
    public interface IUserCollectedDataProvider
    {
        Task<List<FileDto>> GetFiles(UserIdentifier user);
    }
}
