﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RASP.AbpTemplate.MultiTenancy.HostDashboard.Dto;

namespace RASP.AbpTemplate.MultiTenancy.HostDashboard
{
    public interface IIncomeStatisticsService
    {
        Task<List<IncomeStastistic>> GetIncomeStatisticsData(DateTime startDate, DateTime endDate,
            ChartDateInterval dateInterval);
    }
}