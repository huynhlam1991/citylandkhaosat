﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using RASP.AbpTemplate.MultiTenancy.Accounting.Dto;

namespace RASP.AbpTemplate.MultiTenancy.Accounting
{
    public interface IInvoiceAppService
    {
        Task<InvoiceDto> GetInvoiceInfo(EntityDto<long> input);

        Task CreateInvoice(CreateInvoiceDto input);
    }
}
