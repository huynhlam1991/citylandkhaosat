using RASP.AbpTemplate.RMarketing.Dtos;
using RASP.AbpTemplate.RMarketing;
using RASP.AbpTemplate.RSurvey.Dtos;
using RASP.AbpTemplate.RSurvey;
using RASP.AbpTemplate.RModel.Dtos;
using RASP.AbpTemplate.RModel;
using Abp.Application.Editions;
using Abp.Application.Features;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.EntityHistory;
using Abp.Localization;
using Abp.Notifications;
using Abp.Organizations;
using Abp.UI.Inputs;
using AutoMapper;
using RASP.AbpTemplate.Auditing.Dto;
using RASP.AbpTemplate.Authorization.Accounts.Dto;
using RASP.AbpTemplate.Authorization.Permissions.Dto;
using RASP.AbpTemplate.Authorization.Roles;
using RASP.AbpTemplate.Authorization.Roles.Dto;
using RASP.AbpTemplate.Authorization.Users;
using RASP.AbpTemplate.Authorization.Users.Dto;
using RASP.AbpTemplate.Authorization.Users.Importing.Dto;
using RASP.AbpTemplate.Authorization.Users.Profile.Dto;
using RASP.AbpTemplate.Chat;
using RASP.AbpTemplate.Chat.Dto;
using RASP.AbpTemplate.Editions;
using RASP.AbpTemplate.Editions.Dto;
using RASP.AbpTemplate.Friendships;
using RASP.AbpTemplate.Friendships.Cache;
using RASP.AbpTemplate.Friendships.Dto;
using RASP.AbpTemplate.Localization.Dto;
using RASP.AbpTemplate.MultiTenancy;
using RASP.AbpTemplate.MultiTenancy.Dto;
using RASP.AbpTemplate.MultiTenancy.HostDashboard.Dto;
using RASP.AbpTemplate.MultiTenancy.Payments;
using RASP.AbpTemplate.MultiTenancy.Payments.Dto;
using RASP.AbpTemplate.Notifications.Dto;
using RASP.AbpTemplate.Organizations.Dto;
using RASP.AbpTemplate.Sessions.Dto;

namespace RASP.AbpTemplate
{
    internal static class CustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
           configuration.CreateMap<CreateOrEditRMailCustomerDto, RMailCustomer>().ReverseMap();
           configuration.CreateMap<RMailCustomerDto, RMailCustomer>().ReverseMap();
           configuration.CreateMap<CreateOrEditSurveyResultDto, SurveyResult>().ReverseMap();
           configuration.CreateMap<SurveyResultDto, SurveyResult>().ReverseMap();
           configuration.CreateMap<CreateOrEditPersonAnswerDto, PersonAnswer>().ReverseMap();
           configuration.CreateMap<PersonAnswerDto, PersonAnswer>().ReverseMap();
           configuration.CreateMap<CreateOrEditAnswerDto, Answer>().ReverseMap();
           configuration.CreateMap<AnswerDto, Answer>().ReverseMap();
           configuration.CreateMap<CreateOrEditQuestionDto, Question>().ReverseMap();
           configuration.CreateMap<QuestionDto, Question>().ReverseMap();
           configuration.CreateMap<CreateOrEditSurveyDto, Survey>().ReverseMap();
           configuration.CreateMap<SurveyDto, Survey>().ReverseMap();
           configuration.CreateMap<CreateOrEditRProductCategoryDto, RProductCategory>().ReverseMap();
           configuration.CreateMap<RProductCategoryDto, RProductCategory>().ReverseMap();
           configuration.CreateMap<CreateOrEditProductDto, Product>().ReverseMap();
           configuration.CreateMap<ProductDto, Product>().ReverseMap();
           configuration.CreateMap<CreateOrEditCouponDto, Coupon>().ReverseMap();
           configuration.CreateMap<CouponDto, Coupon>().ReverseMap();
           configuration.CreateMap<CreateOrEditRSliderDto, RSlider>().ReverseMap();
           configuration.CreateMap<RSliderDto, RSlider>().ReverseMap();
           configuration.CreateMap<CreateOrEditArticleDto, Article>().ReverseMap();
           configuration.CreateMap<ArticleDto, Article>().ReverseMap();
           configuration.CreateMap<CreateOrEditRCategoryDto, RCategory>().ReverseMap();
           configuration.CreateMap<RCategoryDto, RCategory>().ReverseMap();
            //Inputs
            configuration.CreateMap<CheckboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<SingleLineStringInputType, FeatureInputTypeDto>();
            configuration.CreateMap<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<IInputType, FeatureInputTypeDto>()
                .Include<CheckboxInputType, FeatureInputTypeDto>()
                .Include<SingleLineStringInputType, FeatureInputTypeDto>()
                .Include<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<ILocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>()
                .Include<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<LocalizableComboboxItem, LocalizableComboboxItemDto>();
            configuration.CreateMap<ILocalizableComboboxItem, LocalizableComboboxItemDto>()
                .Include<LocalizableComboboxItem, LocalizableComboboxItemDto>();

            //Chat
            configuration.CreateMap<ChatMessage, ChatMessageDto>();
            configuration.CreateMap<ChatMessage, ChatMessageExportDto>();

            //Feature
            configuration.CreateMap<FlatFeatureSelectDto, Feature>().ReverseMap();
            configuration.CreateMap<Feature, FlatFeatureDto>();

            //Role
            configuration.CreateMap<RoleEditDto, Role>().ReverseMap();
            configuration.CreateMap<Role, RoleListDto>();
            configuration.CreateMap<UserRole, UserListRoleDto>();

            //Edition
            configuration.CreateMap<EditionEditDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<EditionCreateDto, SubscribableEdition>();
            configuration.CreateMap<EditionSelectDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<Edition, EditionInfoDto>().Include<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<SubscribableEdition, EditionListDto>();
            configuration.CreateMap<Edition, EditionEditDto>();
            configuration.CreateMap<Edition, SubscribableEdition>();
            configuration.CreateMap<Edition, EditionSelectDto>();


            //Payment
            configuration.CreateMap<SubscriptionPaymentDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPaymentListDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPayment, SubscriptionPaymentInfoDto>();

            //Permission
            configuration.CreateMap<Permission, FlatPermissionDto>();
            configuration.CreateMap<Permission, FlatPermissionWithLevelDto>();

            //Language
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageListDto>();
            configuration.CreateMap<NotificationDefinition, NotificationSubscriptionWithDisplayNameDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>()
                .ForMember(ldto => ldto.IsEnabled, options => options.MapFrom(l => !l.IsDisabled));

            //Tenant
            configuration.CreateMap<Tenant, RecentTenant>();
            configuration.CreateMap<Tenant, TenantLoginInfoDto>();
            configuration.CreateMap<Tenant, TenantListDto>();
            configuration.CreateMap<TenantEditDto, Tenant>().ReverseMap();
            configuration.CreateMap<CurrentTenantInfoDto, Tenant>().ReverseMap();

            //User
            configuration.CreateMap<User, UserEditDto>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());
            configuration.CreateMap<User, UserLoginInfoDto>();
            configuration.CreateMap<User, UserListDto>();
            configuration.CreateMap<User, ChatUserDto>();
            configuration.CreateMap<User, OrganizationUnitUserListDto>();
            configuration.CreateMap<Role, OrganizationUnitRoleListDto>();
            configuration.CreateMap<CurrentUserProfileEditDto, User>().ReverseMap();
            configuration.CreateMap<UserLoginAttemptDto, UserLoginAttempt>().ReverseMap();
            configuration.CreateMap<ImportUserDto, User>();

            //AuditLog
            configuration.CreateMap<AuditLog, AuditLogListDto>();
            configuration.CreateMap<EntityChange, EntityChangeListDto>();
            configuration.CreateMap<EntityPropertyChange, EntityPropertyChangeDto>();

            //Friendship
            configuration.CreateMap<Friendship, FriendDto>();
            configuration.CreateMap<FriendCacheItem, FriendDto>();

            //OrganizationUnit
            configuration.CreateMap<OrganizationUnit, OrganizationUnitDto>();

            /* ADD YOUR OWN CUSTOM AUTOMAPPER MAPPINGS HERE */
        }
    }
}
