﻿using System.Collections.Generic;
using RASP.AbpTemplate.Auditing.Dto;
using RASP.AbpTemplate.Dto;

namespace RASP.AbpTemplate.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);

        FileDto ExportToFile(List<EntityChangeListDto> entityChangeListDtos);
    }
}
