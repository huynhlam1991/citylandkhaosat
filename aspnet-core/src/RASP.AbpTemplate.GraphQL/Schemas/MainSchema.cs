﻿using Abp.Dependency;
using GraphQL;
using GraphQL.Types;
using RASP.AbpTemplate.Queries.Container;

namespace RASP.AbpTemplate.Schemas
{
    public class MainSchema : Schema, ITransientDependency
    {
        public MainSchema(IDependencyResolver resolver) :
            base(resolver)
        {
            Query = resolver.Resolve<QueryContainer>();
        }
    }
}