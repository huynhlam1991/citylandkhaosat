﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RASP.AbpTemplate.RSurvey
{
    public enum QuestionTypeEnum
    {
        YesNo = 1,
        AnswerText = 2,
        SingleChoice = 3,
        MultiChoice = 4
    }
}
