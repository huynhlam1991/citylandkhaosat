namespace RASP.AbpTemplate.RSurvey
{
    public class SurveyConsts
    {

		public const int MinTitleLength = 0;
		public const int MaxTitleLength = 1000;
						
		public const int MinDescriptionLength = 0;
		public const int MaxDescriptionLength = 2000;
						
						
    }
}