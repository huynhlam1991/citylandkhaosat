namespace RASP.AbpTemplate.RModel
{
    public class ArticleConsts
    {

		public const int MinMetaTitleLength = 0;
		public const int MaxMetaTitleLength = 500;
						
						
		public const int MinImageNameLength = 0;
		public const int MaxImageNameLength = 500;
						
						
		public const int MinTitleLength = 0;
		public const int MaxTitleLength = 2000;
						
		public const int MinDescriptionLength = 0;
		public const int MaxDescriptionLength = 2000;
						
						
		public const int MinAuthorLength = 0;
		public const int MaxAuthorLength = 200;
						
						
						
    }
}