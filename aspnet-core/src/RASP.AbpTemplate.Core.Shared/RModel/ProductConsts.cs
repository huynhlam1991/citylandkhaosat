namespace RASP.AbpTemplate.RModel
{
    public class ProductConsts
    {

		public const int MinMetaTitleLength = 0;
		public const int MaxMetaTitleLength = 500;
						
		public const int MinMetaDescriptionLength = 0;
		public const int MaxMetaDescriptionLength = 2000;
						
		public const int MinTitleLength = 0;
		public const int MaxTitleLength = 2000;
						
		public const int MinDescriptionLength = 0;
		public const int MaxDescriptionLength = 2000;
						
						
						
		public const int MinSaleManLength = 0;
		public const int MaxSaleManLength = 500;
						
						
						
						
		public const int MinDisplayIsContactLength = 0;
		public const int MaxDisplayIsContactLength = 500;
						
    }
}