namespace RASP.AbpTemplate.RModel
{
    public class RSliderConsts
    {

		public const int MinTitleLength = 0;
		public const int MaxTitleLength = 1000;
						
		public const int MinFileNameLength = 0;
		public const int MaxFileNameLength = 500;
						
						
		public const int MinDescriptionLength = 0;
		public const int MaxDescriptionLength = 1000;
						
		public const int MinPositionCodeLength = 0;
		public const int MaxPositionCodeLength = 250;
						
    }
}