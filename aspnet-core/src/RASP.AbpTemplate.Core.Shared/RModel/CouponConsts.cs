namespace RASP.AbpTemplate.RModel
{
    public class CouponConsts
    {

		public const int MinTitleLength = 0;
		public const int MaxTitleLength = 500;
						
		public const int MinDescriptionLength = 0;
		public const int MaxDescriptionLength = 2000;
						
						
						
		public const int MinAuthorLength = 0;
		public const int MaxAuthorLength = 250;
						
		public const int MinImageNameLength = 0;
		public const int MaxImageNameLength = 500;
						
    }
}