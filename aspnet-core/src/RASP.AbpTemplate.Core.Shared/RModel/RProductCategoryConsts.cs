namespace RASP.AbpTemplate.RModel
{
    public class RProductCategoryConsts
    {

		public const int MinMetaTitleLength = 0;
		public const int MaxMetaTitleLength = 500;
						
		public const int MinMetaDescriptionLength = 0;
		public const int MaxMetaDescriptionLength = 2000;
						
		public const int MinTitleLength = 0;
		public const int MaxTitleLength = 1000;
						
		public const int MinDescriptionLength = 0;
		public const int MaxDescriptionLength = 2000;
						
						
						
    }
}