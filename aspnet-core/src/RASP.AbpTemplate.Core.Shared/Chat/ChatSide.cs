﻿namespace RASP.AbpTemplate.Chat
{
    public enum ChatSide
    {
        Sender = 1,

        Receiver = 2
    }
}