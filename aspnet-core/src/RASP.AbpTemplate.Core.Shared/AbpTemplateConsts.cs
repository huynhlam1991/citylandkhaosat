﻿namespace RASP.AbpTemplate
{
    public class AbpTemplateConsts
    {
        public const string LocalizationSourceName = "AbpTemplate";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = false;

        public const bool AllowTenantsToChangeEmailSettings = false;

        public const string Currency = "USD";

        public const string CurrencySign = "$";

        public const string AbpApiClientUserAgent = "AbpApiClient";
    }
}