namespace RASP.AbpTemplate.RMarketing
{
    public class RMailCustomerConsts
    {

		public const int MinNameLength = 0;
		public const int MaxNameLength = 500;
						
		public const int MinEmailLength = 0;
		public const int MaxEmailLength = 500;
						
		public const int MinPrivateCodeLength = 0;
		public const int MaxPrivateCodeLength = 1000;
						
						
		public const int MinPhoneNumberLength = 0;
		public const int MaxPhoneNumberLength = 100;
						
    }
}