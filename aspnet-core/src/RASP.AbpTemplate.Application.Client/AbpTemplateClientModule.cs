﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace RASP.AbpTemplate
{
    public class AbpTemplateClientModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(AbpTemplateClientModule).GetAssembly());
        }
    }
}
