﻿using Abp.AspNetCore.Mvc.Views;

namespace RASP.AbpTemplate.Web.Views
{
    public abstract class AbpTemplateRazorPage<TModel> : AbpRazorPage<TModel>
    {
        protected AbpTemplateRazorPage()
        {
            LocalizationSourceName = AbpTemplateConsts.LocalizationSourceName;
        }
    }
}
