﻿using Microsoft.AspNetCore.Antiforgery;

namespace RASP.AbpTemplate.Web.Controllers
{
    public class AntiForgeryController : AbpTemplateControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
