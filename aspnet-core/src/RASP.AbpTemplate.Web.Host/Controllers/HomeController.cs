﻿using Abp.Auditing;
using Microsoft.AspNetCore.Mvc;

namespace RASP.AbpTemplate.Web.Controllers
{
    public class HomeController : AbpTemplateControllerBase
    {
        [DisableAuditing]
        public IActionResult Index()
        {
            return RedirectToAction("Index", "Ui");
        }
    }
}
