﻿using Abp.AspNetCore.Mvc.Authorization;
using RASP.AbpTemplate.Storage;

namespace RASP.AbpTemplate.Web.Controllers
{
    [AbpMvcAuthorize]
    public class ProfileController : ProfileControllerBase
    {
        public ProfileController(ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
        }
    }
}